/**
 * Utility class for injecting hidden default values if explicitly set in NSO.
 * Uses the meta-data tag "default-value".
 * Example:
 *   tailf:meta-data "default-value";
 *
 * @author lbang
 * @version 20180510
 */

package com.tailf.packages.ned.ios;

import static com.tailf.packages.ned.nedcom.NedString.getMatch;
import static com.tailf.packages.ned.nedcom.NedString.stringQuote;
import static com.tailf.packages.ned.nedcom.NedString.calculateMd5Sum;

import java.util.Iterator;
import java.util.ArrayList;

import com.tailf.ned.NedWorker;
import com.tailf.ned.NedException;

import com.tailf.conf.ConfBuf;
import com.tailf.conf.ConfPath;


//
// NedDefaults
//
@SuppressWarnings("deprecation")
public class NedDefaults {

    private static final String[][] defaultMaps = {
        {
            "wrr-queue/cos-map{1 1} :: wrr-queue cos-map 1 1 0 1",
            "wrr-queue/cos-map{1 2} :: wrr-queue cos-map 1 2 2 3"
        },
        {
            "wrr-queue/cos-map{1 1} :: wrr-queue cos-map 1 1 0",
            "wrr-queue/cos-map{1 2} :: wrr-queue cos-map 1 2 1",
            "wrr-queue/cos-map{3 1} :: wrr-queue cos-map 3 1 6"
        }
    };

    // Constructor data:
    private IOSNedCli owner;

    // Local data:
    private String operRoot;
    private String operList;
    private boolean showQueueing = true;

    /*
     * Constructor
     */
    NedDefaults(IOSNedCli owner) throws NedException {
        this.owner = owner;
        this.operRoot = "/ncs:devices/ncs:device{"
            +owner.device_id
            +"}/ncs:ned-settings/ios-op:cisco-ios-oper/defaults";
        this.operList = this.operRoot + "{%s}";
    }


    /**
     * Cache all default entries
     * @param
     * @throws NedException
     */
    public void cache(NedWorker worker, String[] lines) throws NedException {
        for (int i = 0 ; i < lines.length - 1; i++) {
            if (!isMetaDataDefault(lines[i])) {
                continue; // not a meta-data default
            }
            int c;
            for (c = i + 1; c < lines.length; c++) {
                if (!lines[c].trim().startsWith("! meta-data :: ")) {
                    break; // found command line
                }
            }
            cacheLine(worker, lines[c].trim(), lines[i].trim());
        }
    }


    /**
     * Cache a default value entry
     * @param
     * @throws NedException
     */
    private void cacheLine(NedWorker worker, String trimmed, String meta) throws NedException {

        // metas[0] = ! meta-data
        // metas[1] = <path>
        // metas[2] = "default-xxx"
        final String[] metas = meta.split(" :: ");
        String id = null;
        try {
            id = calculateMd5Sum(metas[1]);
        } catch (Exception e) {
            throw new NedException("Internal ERROR: failed to create DEFAULTS id for "+metas[1]);
        }

        // Delete or create default entry for this path
        if (trimmed.startsWith("no ")) {
            operDelete(worker, meta, id);
        } else {
            operCreate(worker, meta, id);
        }
    }


    /**
     * Create default entry in oper data defaults list
     * @param
     */
    private void operCreate(NedWorker worker, String meta, String id) {
        try {
            ConfPath cp = new ConfPath(String.format(this.operList, id));
            if (!owner.cdbOper.exists(cp)) {
                owner.traceInfo(worker, "DEFAULTS - Creating : " + meta);
                owner.cdbOper.create(cp);
            }
            owner.cdbOper.setElem(new ConfBuf(meta), cp.append("/meta"));
        } catch (Exception e) {
            owner.logError(worker, "DEFAULTS - ERROR : failed to create "+id, e);
        }
    }


    /**
     * Delete default entry in oper data defaults list
     * @param
     */
    private void operDelete(NedWorker worker, String meta, String id) {
        try {
            ConfPath cp = new ConfPath(String.format(this.operList, id));
            if (owner.cdbOper.exists(cp)) {
                if (meta != null) {
                    owner.traceInfo(worker, "DEFAULTS - Deleting : " + meta);
                }
                owner.cdbOper.delete(cp);
            }
        } catch (Exception e) {
            owner.logError(worker, "DEFAULTS - ERROR : failed to delete "+id, e);
        }
    }


    /**
     * Check if line is a default meta-data
     * @param
     * @return
     */
    private boolean isMetaDataDefault(String line) {
        if (!line.trim().startsWith("! meta-data :: ")) {
            return false;
        }
        return line.contains(" :: default-");
    }


    /**
     * Inject default values
     * @param
     * @return
     */
    public String inject(NedWorker worker, String dump, int th) throws NedException {
        StringBuilder sb = new StringBuilder();
        ArrayList<String> deleteList = new ArrayList<>();

        // Read list using Maapi.getObjects
        ArrayList<String[]> list = owner.maapiGetObjects(worker, th, this.operRoot, 2);
        for (String[] obj : list) {
            final String id = obj[0];
            final String meta = obj[1];

            // Verify that new 'cisco-ios-oper defaults' list API
            if (obj[0] == null || obj[1] == null || !obj[1].startsWith("! meta-data :: ")) {
                owner.traceInfo(worker, "DEFAULTS - Deleting (old CDB oper syntax) : " + id);
                deleteList.add(id);
                continue;
            }

            // Config is deleted in CDB, don't need to inject default
            final String[] metas = meta.split(" :: ");
            final String path = metas[1];
            if (!owner.maapiExists(worker, th, path)) {
                owner.traceInfo(worker, "DEFAULTS - Deleting (missing in CDB) : " + path);
                deleteList.add(id);
                continue;
            }

            // Verify that the interfaces still exists on device
            String ifname = getMatch(path, "interface/(\\S+?[{]\\S+?[}])");
            ifname = "interface " + ifname.replace("{", "").replace("}", "");
            if (!dump.contains("\n"+ifname)) {
                owner.traceInfo(worker, "DEFAULTS - Deleting (missing interface) : " + path);
                deleteList.add(id);
                continue;
            }

            // Re-inject default values
            String inject = null;

            //
            // interface * / priority-queue cos-map *
            //
            if ("default-if-priority-queue-cos-map".equals(metas[2])) {
                inject = " priority-queue cos-map 1 5\n";
            }

            //
            // interface * / wrr-queue cos-map *
            //
            else if ("default-if-wrr-queue-cos-map".equals(metas[2])) {
                int map = getWrrQueueCosMapDefaultMap(worker, ifname);
                for (int n = 0; n < defaultMaps[map].length; n++) {
                    String[] tokens = defaultMaps[map][n].split(" :: ");
                    if (path.contains(tokens[0])) {
                        inject = " " + tokens[1] + "\n";
                        break;
                    }
                }
            }

            if (inject != null) {
                owner.traceInfo(worker, "transformed <= injected DEFAULTS: "
                                +stringQuote(inject)+" in "+stringQuote(ifname));
                sb.append(ifname+"\n"+inject+"exit\n");
            }
        }

        // Delete unfound/bad entries
        Iterator it = deleteList.iterator();
        while (it.hasNext()) {
            String id = (String)it.next();
            operDelete(worker, null, id);
        }

        return sb.toString() + dump;
    }


    /**
     * Look up which queue map defaults are used on interface
     * @param
     * @return
     */
    private int getWrrQueueCosMapDefaultMap(NedWorker worker, String ifname) {
        int map = 0;

        // Show queuing to determine what type of default map
        try {
            // WS-C6504-E show queueing syntax
            String res = "Invalid input detected at";
            if (this.showQueueing) {
                res = owner.print_line_exec(worker, "show queueing "+ifname+" | i WRR");
            }

            // 7604 show queueing syntax
            if (res.contains("Invalid input detected at")) {
                this.showQueueing = false;
                res = owner.print_line_exec(worker, "show mls qos queuing "+ifname+" | i WRR");
            }
            if (res.contains("Invalid input detected at")) {
                this.showQueueing = true;
                owner.traceInfo(worker, "DEFAULTS - cache() ERROR :: failed to show queuing for "+ifname);
            } else if (res.contains("[queue 3]")) {
                map = 1;
            }
        } catch (Exception e) {
            owner.logError(worker, "DEFAULTS - ERROR : show queuing Exception", e);
        }

        owner.traceVerbose(worker, "DEFAULTS - "+stringQuote(ifname)+" map = "+map);
        return map;
    }

}
