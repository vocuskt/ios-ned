package com.tailf.packages.ned.ios;

import com.tailf.packages.ned.nedcom.Schema;
import com.tailf.packages.ned.nedcom.NedComCliBase;
import com.tailf.packages.ned.nedcom.NedCommonLib.PlatformInfo;
import com.tailf.packages.ned.nedcom.NedDiff;
import com.tailf.packages.ned.nedcom.NedSecrets;
import com.tailf.packages.ned.nedcom.livestats.NedLiveStats;
import com.tailf.packages.ned.nedcom.livestats.NedLiveStatsException;
import com.tailf.packages.ned.nedcom.livestats.NedLiveStatsShowHandler;
import static com.tailf.packages.ned.nedcom.NedString.getMatch;
import static com.tailf.packages.ned.nedcom.NedString.getMatches;
import static com.tailf.packages.ned.nedcom.NedString.fillGroups;
import static com.tailf.packages.ned.nedcom.NedString.stringQuote;
import static com.tailf.packages.ned.nedcom.NedString.stringDequote;
import static com.tailf.packages.ned.nedcom.NedString.passwordQuote;
import static com.tailf.packages.ned.nedcom.NedString.passwordDequote;
import static com.tailf.packages.ned.nedcom.NedString.matcherToString;
import static com.tailf.packages.ned.nedcom.NedString.hasString;
import static com.tailf.packages.ned.nedcom.NedString.findString;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.text.StringCharacterIterator;
import java.text.CharacterIterator;

import java.net.InetAddress;
import java.net.NetworkInterface;

import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.SCPClient;
import ch.ethz.ssh2.SCPInputStream;

import com.tailf.conf.Conf;
import com.tailf.conf.ConfBuf;
import com.tailf.conf.ConfPath;
import com.tailf.conf.ConfValue;
import com.tailf.conf.ConfObject;
import com.tailf.conf.ConfException;
import com.tailf.conf.ConfXMLParam;
import com.tailf.conf.ConfXMLParamValue;

import com.tailf.maapi.Maapi;
import com.tailf.maapi.MaapiCrypto;
import com.tailf.maapi.MaapiCursor;
import com.tailf.maapi.MaapiException;
import com.tailf.maapi.MaapiConfigFlag;
import com.tailf.maapi.MaapiInputStream;

import com.tailf.ned.NedCmd;
import com.tailf.ned.NedExpectResult;
import com.tailf.ned.NedException;
import com.tailf.ned.NedMux;
import com.tailf.ned.NedWorker;
import com.tailf.ned.CliSession;
import com.tailf.ned.SSHSessionException;



/**
 * Implements the cisco-ios CLI NED
 * @author lbang
 *
 */
@SuppressWarnings("deprecation")
public class IOSNedCli extends NedComCliBase {

    // Constants
    protected static final int TRACE_INFO = 6;
    protected static final int TRACE_DEBUG = 7;
    protected static final int TRACE_DEBUG2 = 8;
    protected static final int TRACE_DEBUG3 = 9;
    protected static final int TRACE_DEBUG4 = 10;

    private static final String PREFIX = "ios:";
    private static final String EXTENDED_PARSER = "extended-parser";
    private static final String META_DATA = "! meta-data :: ";
    private enum Echo { WAIT, DONTWAIT, TEXT }

    private static final String UNKNOWN = "unknown";
    private static final String NSKEY = "__key__";

    // Prompts

    // start of input, > 0 non-# and ' ', one #, >= 0 ' ', eol
    private static final String PRIVEXEC_PROMPT = "\\A[^\\# ]+#[ ]?$";
    private static final String PROMPT = "\\A\\S+#";
    private static final String CONFIG_PROMPT = "\\A\\S+\\(\\S+\\)#[ ]?$";
    private static final String CFG_PROMPT = "\\A.*\\(.*\\)#";
    private static final String ANY_PROMPT = "\\A\\S*#";

    // print_line_wait() pattern
    private static final Pattern[] PLW0 = new Pattern[] {
        // 0 prompts:
        Pattern.compile("\\A.*\\(cfg\\)#"),
        Pattern.compile("\\A.*\\(config\\)#"),
        Pattern.compile(CFG_PROMPT),
        Pattern.compile(ANY_PROMPT),
        // 4 standard questions:
        Pattern.compile("\\?[ ]{0,2}\\(yes/\\[no\\]\\)"),  // ? (yes/[no])
        Pattern.compile("\\?[ ]{0,2}\\[[Yy]es/[Nn]o\\]"),  // ? [yes/no]
        Pattern.compile("\\?[ ]{0,2}\\[[Yy]es\\]"),        // ? [yes]
        Pattern.compile("\\?[ ]{0,2}\\[[Nn]o\\]"),         // ? [no]
        Pattern.compile("\\?[ ]{0,2}\\[confirm\\]")        // ? [confirm]
        // 9 additional patterns from inject-answer
    };

    private static final Pattern[] EC = new Pattern[] {
        Pattern.compile("Do you want to kill that session and continue"),
        Pattern.compile("Configuration mode is locked by process.+Please try later"),
        Pattern.compile("\\A\\S*\\(config\\)#"),
        Pattern.compile(CFG_PROMPT),
        Pattern.compile("Aborted.*\n"),
        Pattern.compile("Error.*\n"),
        Pattern.compile("syntax error.*\n"),
        Pattern.compile("error:.*\n")
    };

    private static final Pattern[] EC2 = new Pattern[] {
        Pattern.compile("\\A.*\\(cfg\\)#"),
        Pattern.compile("\\A.*\\(config\\)#"),
        Pattern.compile(CFG_PROMPT),
        Pattern.compile("Aborted.*\n"),
        Pattern.compile("Error.*\n"),
        Pattern.compile("syntax error.*\n"),
        Pattern.compile("error:.*\n")
    };

    // NEDLIVESTATS prompts
    private static final Pattern[] NEDLIVESTATS_PROMPT = new Pattern[] {
        Pattern.compile(CFG_PROMPT),
        Pattern.compile("\\A[^\\# ]+#[ ]?$")
    };

    /**
     * Warnings, regular expressions. NOTE: Lowercase!
     */
    private static final String[] staticWarning = {
        // general
        "warning[:,] \\S+.*",
        "warning:",
        ".?note:",
        "info:",
        "aaa: warning",
        "success",  //  " added successfully",
        "enter text message",
        "enter macro commands one per line",
        "this commmand is deprecated",
        "this command requires a reload to take effect",
        "will take effect after reload",
        "this command is an unreleased and unsupported feature",
        "cli will be deprecated soon",
        "command accepted but obsolete, unreleased or unsupported",
        "redundant .* statement",
        "elapsed time was \\d+ seconds",
        "configuring anyway",

        // remove || delete
        "hqm_tablemap_inform: class_remove error",
        "all rsa keys will be removed",
        "all router certs issued using these keys will also be removed",
        "not all config may be removed and may reappear after",
        "removed .* policy from .* interface",
        "this will remove previously",
        "removing ssp group",
        "remote  deleted",
        "tunnel interface was deleted",
        "mac address.*has been deleted from the bridge table",
        "non-fr-specific configuration, if not yet explicitly deconfigured",
        "entry does not exist", // no ip multicast vrf *
        "please remove .+ from all .+ for complete cleanup", // no platform mpls mtu-enable
        "deleting ac member of xconnect", // interface * / no service instance *

        // in case of some device flavors the entry is not deleted
        // "bridge-domain \\d+ cannot be deleted because it is not empty",  // no bridge-domain *
        "\\S+ profile is removed",
        "service removed for domain .*",
        "will be removed from .* due to removal of",
        "warning: \\S+ is the default fax protocol, it can not be removed",
        "removed \\d+ (entry|entries)",
        "removing .+ configuration on all interfaces",
        "can't delete view \\S+",
        "can not find view \\S+",
        "entry not configured",

        // change
        "changes to .* will not take effect until the next",
        "please reload the switch for .+ configuration to take effect", // stackwise-virtual
        "please reload the switch to disable .+ functionality", // no stackwise-virtual
        "security level for .* changed to",
        "connection name is changed",
        "changes to the running .* have been stored",
        "you are about to \\S+grade",
        "use 'write' command to make", // license boot level security
        "no change in the configuration",
        "same config is entered which has no effect",
        "a system reload is required before .+ change", // subscriber templating
        "changing media to \\S+", // interface * / media-type
        ".+ set to default configuration", // default

        // VRF
        "removed due to \\S+abling vrf",
        "removed due to vrf change",
        "the static routes in vrf .*with outgoing interface .*will be",
        "ip.* addresses from all interfaces in vrf .*have been removed",
        "number of vrfs \\S+graded",
        "vrf .*exists but is not enabled",
        "a new tunnel id may be used if the default mdt is reconfigured for this vrf",
        "for vrf .* scheduled for deletion",
        "vrf \\S+ not configured, invalid vrf name",
        "unable to remove extended community", // ip vrf * / no route-target
        "vrf \\S+ is now bound to default vrf parameter map",

        // vlan
        "vlan.* does not exist.* creating vlan",
        "please refer to documentation on configuring ieee 802.1q vlans",
        "vlan mapping is also changed",
        ".*vlan .* does not exist, creating vlan.*",
        "vlan mapping is also changed",
        "vlan  mod/ports",
        "applying vlan changes may take few minutes",
        "access vlan does not exist",
        "the .+ in slot \\S+ is currently offline",
        "pruning switched (off|on)", // no vtp pruning

        // interface
        "if .*interface does.* support baby giant frames",
        "no cef interface information",
        "unrecognized virtual interface .* treat it as loopback stub",
        "ipv4 and ipv6 addresses from all",
        "ip\\S+ addresses from all interfaces",
        "pim configuration for interface",
        "interface .* hsrp [a-f0-9:]* removed due to vrf change",
        "(\\S+): informational: \\S+ is in use on",
        "is reverting to router mode configuration, and remains disabled",
        "ospf will not operate on this interface until ip is configured on it",
        "command will have no effect with this interface",
        "portfast has been configured on ",  // spanning-tree portfast
        "creating a port-channel interface port-channel", // interface * / channel-group 3 mode active
        "speed auto-negotiation also needs to be set for auto-mdix to take effect", // mdix auto
        "the multilink group configuration will be removed from all the member links", // no interface Multilink
        "removal of channelized sonet/sdh interface configuration is not permitted", // no interface Serial
        "xconnect configuration on this circuit is incomplete", // interface * / xconnect
        "not found . using global defaults",
        "configured platform supported protocols",
        "is .+ fragmentation may occur", // interface * / ip mtu
        "loopback is a traffic-affecting operation", // interface * / loopback mac
        "policy attached to main[-]interface may not function as expected", // interface * / encapsulation ppp
        "macsec changed ip mtu on ", // interface * / macsec
        "the combination of icmp redirects and hsrp on the same", // interface * / ip redirects
        "interface may result in lost packets", // interface * / ip redirects

        // controller * /
        "is out of range while validating payload for vcop", // cem-group *
        "has been configured by unframed channel", // no tug-2 * e1 * framing unframed

        // router
        "peer-group \\S+ is not present, but will go ahead and delete",
        "peergroups are automatically activated when parameters are configured",
        // router lisp / service * / encapsulation
        "setting the encapsulation of ipv(4|6) to \\S+. encapsulation cannot be different",
        "all bgp sessions must be reset to take the new", // bgp graceful-restart restart-time
        "only classful networks will be redistributed", // router ospf * / redistribute static
        "removing wide metrics also removes mpls te on", // router isis / no metric-style wide
        "reference bandwidth is changed", // router ospf / auto-cost reference-bandwidth
        ".* set use own .* address for the nexthop not supported", // MPLS-OUT
        "ospf supports only classless redistribution", // router ospf * / no redistribute isis
        // router bgp * / neighbor * send-label
        "for distributing mpls labels to ibgp peers, the update source should be set to",

        // tunnel
        "tunnel mpls traffic-eng fast-reroute",
        "attach member tunnel to a master",
        "\\S+ tunnels are not enabled on this router", // interface * / mpls traffic-eng tunnels

        // mpls
        "pce disjoint-path source \\S+ type \\S+ group-id", // mpls traffic-eng lsp attributes * / pce
        "^record-route$", // mpls traffic-eng lsp attributes * / record-route

        // AppNav / virtual-service
        "ac with local ip is being removed\\. service context cannot be enabled",
        "service context must have a appnav controller group attached to it before it can be enabled",
        "last vrf removed\\. disable this service-context",
        "activating virtual-service .* this might take a few minutes", //  virtual-service * / activate
        "virtual service .* install has not completed",
        "virtual service \\S+ was not activated",
        "acg must contain ip that is local to the device and interface should be up",

        // utd
        "please ensure .* is configured to use",
        "filtering will now be disabled on exiting the submode",
        "source db config has now been removed",
        "utd deregistered with appnav", // no utd
        "utd successfully registered with appnav", // utd engine standard multi-tenancy
        "utd redirect interface set to \\S+ internally", // utd engine standard multi-tenancy
        "utd appnav.*registration",  // utd engine standard multi-tenancy

        // SSH & certificate
        "enter the certificate",
        "certificate accepted",
        "certificate request sent",
        "ssh:publickey disabled.overriding rfc",
        "ssh:no auth method configured.incoming connection will be dropped",
        "please create rsa keys to enable ssh",
        "generating \\d+ bit rsa keys",   // ip http secure-server, crypto pki server * / no shutdown etc.
        "the certificate has been deleted", // no certificate self-signed X
        "cannot delete certificate server certificates", // no crypto pki certificate chain

        // crypto
        "ikev2 \\S+ must have",
        "crypto-6-isakmp_on_off: isakmp is",
        "be sure to ask the ca administrator to revoke your certificates",
        "overriding already existing source with priority",
        "updated group cp to ", // crypto gkm group * / server address ipv4
        "this will remove all existing \\S+ on this map", // crypto map ** ipsec-isakmp / reverse-route static
        // crypto map ** ipsec-isakmp / no reverse-route static
        "removing \\S+ will delete all routes and clear current ipsec",
        "ikev2 proposal must either have a set of an encryption algorithm", // crypto ikev2 proposal *
        "event has been queued for processing", // crypto pki server * / shutdown
        "the .* change will take effect after existing .* expire", // crypto pki server * / lifetime
        "can't find policy \\S+",  // no crypto pki trustpoint
        "re-enter password:", // crypto pki server * / no shutdown
        "certificate server enabled", // crypto pki server * / no shutdown
        "removing rri will delete all routes and", // crypto map * / no reverse-route
        "remove the trustpoint to remove the cert chain", // no crypto pki certificate chain
        "enrollment url must be configured", // crypto pki trustpoint * / vrf
        "remember that, to permamently enforce", // no crypto ipsec optional
        "if cipher currently being used is not configured", // mka policy * / macsec-cipher-suite
        "active mka session on this interface cleared for ", // interface * / no mka pre-shared-key
        "certificate not found", // crypto pki certificate chain * / no certificate

        // nat
        "global .* will be port address translated",
        "outside interface address added",
        "pool nat-pool mask .* too small",
        "the active \\S+ status is not known",  // do clear ip nat translation vrf

        // policy-map & class-map
        "no specific protocol configured in class (.*) for inspection",
        "conform burst size \\S+creased to",
        //class-map .* being used
        //service policy .* not attached

        // routing
        "reload or use .* command, for this to take effect",
        // ip routing table .* does not exist. create first
        //no matching route to delete

        // queue | cos
        ".*propagating cos-map configuration to.*",
        ".*propagating queue-limit configuration to.*",
        "cos mutation map",
        "(cos-map|queue-limit) configured on all.* ports on slot",
        "please change queue-limit setting",
        "remove wrr queue cos map on all.* ports",

        // cable
        "minislot size set to", // no us-channel
        "the minislot size is now changed to", // no cable service class
        "response of applying upstream controller-profile", // no cable service class
        "fiber node \\d+ is valid",
        "port \\S+ admin change to (down|up)",
        "caution[:] .+ may result in .+",
        "\\S+ profile group \\S+ is reset to default",
        "setup depi class automatically", // cable rpd *
        "interface \\S+ ip \\S+ mask \\S+ removed", // cable oob / no virtual-arpd *
        "ptp domain \\d+ changed to default.+ of ", // ptp domain
        "channel.+ may only use sc qams .+ when max-carrier set to ", // no rf-chan 111 121
        " is(?: already)? enabled", // cable dynamic-punt enable
        " is disabled", // no cable dynamic-punt enable

        // call-home
        // call-home / profile * / destination transport-method http
        "profile cannot enable more than one transport method",
        // call-home / profile * / no destination transport-method http
        "call-home profile need to have at least one transport-method",
        //"removal of cisco tac profile is not allowed. it can be disabled by issuing", // call-home / no profile *
        "the email address configured in .* will be used as", // call-home / contact-email-addr
        "please configure .* under call-home mode", // call-home / profile * / no anonymous-reporting-only
        "the specified .* is removed", // call-home / no mail-server *
        "configuration succeed, but fail to parse the address", // call-home / mail-server *

        // snmp
        "user cannot belong to an auto-configured group",

        // line * /
        "autoselect w/o the interface command .+ is useless",

        // misc
        "remove mapping of trigger id",
        "cts device id and password have been inserted in the local keystore", // cts credentials id [EXEC]
        "class set to .+ for redundancy group",  // redundancy / linecard-group * / class
        "restarting lc in slot .+ as it is being added as a secondary to", // redundancy / linecard-group * / member
        "warning\\S+ auto discovery already ", // service-insertion * / node-discovery enable
        "enabling mls qos globally",
        "name length exceeded the recommended length of .* characters",
        "a profile is deemed incomplete until it has .* statements",
        "address aliases with",
        "explicit path name",
        "global ethernet mtu is set to",
        "restarting .* service",
        "the .* command will also show the fingerprint",
        "encapsulation dot1q",
        "icmp redirect",
        "\\S+abling learning on",
        "\\S+abling failover",
        "the threshold option has been accepted",
        "added .*to the bridge table",
        " and mac table will be flushed",
        "arp inspection \\S+abled on",
        "configurations are no longer synchronized",
        "pix-[.]-",
        "secured .* cleared from",
        "current activity time is .* seconds",
        "rm entries aging is turned o",
        "zoning is currently not configured for interface",
        "propagating wred configuration to",
        "selected country",
        "is not a legal lat node name",
        "affinity \\S+ mask \\S+", // mpls traffic-eng lsp attributes * / affinity
        "changing vtp domain name from",
        "setting device to vtp .*",
        "wait for .* license request to succeed", // platform hardware throughput level
        "logging of %snmp-3-authfail is (dis|en)abled", // logging snmp-authfail
        "translating \\S+",  // ntp server
        "previously established ldp sessions may not have graceful restart protection", // mpls ldp graceful-restart
        "user configured would overwrite defaults", // parameter-map * / resolver
        "et-analytics destination .* combination does not exist", // et-analytics / no ip flow-export destination
        "delete policy map", // domain * / vrf * / no class
        "label range change will cause",
        "enabling .+ on sub interfaces will have unpredictable results", // cdp tlv-list * / port-id
        "please make sure \\S+ \\S+ is configured", // l2 vfi * / neighbor *
        "react configured but inactive for monitor rtp", // policy-map type performance-monitor * / class * / react *
        "please reboot to activate this \\S+", // platform resource
        "request to provisioning driver failed", // exit from 'radius server *'
        "cannot attach a card profile to the empty slot", // alarm-profile * attach card
        "port[-]range not supported for \\S+", // monitor session * type rspan-source / exit
        "probes are not configured", // ip sla group schedule
        "probes are already running", // ip sla group schedule * add

        // dial-peer
        // dial-peer voice * / voice-class sip bind control source-interface
        "bind command will take effect after the bound interface is up",

        // telephony-service
        "fac standard (is set|has been disabled)!", // telephony-service / fac
        "reload the system to remove ephone.*", // telephony-service / max-ephones

        // ephone *
        "the ephone template tag has been changed under this ephone.*" // ephone * / ephone-template
    };


    // Error override messages (TRUE case)
    private static final String[] staticError = {
        "Error Message",
        "HARDWARE_NOT_SUPPORTED",
        " Incomplete command.",
        "password/key will be truncated to \\d+ characters",
        "Warning: Current config does not permit HSRP version 1",
        "Cannot modify internally generated "
    };

    // PROTECTED:
    protected String confRoot;
    protected MaapiCrypto mCrypto = null;
    protected NedCommand nedCommand;
    protected long lastTimeout;
    protected boolean inConfig = false;
    protected boolean isDry = false;
    protected int lastTransactionId = 0;
    protected int devTraceLevel;
    protected String outputData;
    protected StringBuilder extInjectFirst;

    // Utility classes
    private MetaDataModify metaData;
    private NedLocks locks;
    private NedSecrets secrets;
    private NedDefaults defaults;
    private NedAcl nedAcl;
    private NedDiff nedDiff;
    private ConfigArchive configArchive;
    private ModeStack modeStack = new ModeStack();

    // devices info
    private String iosname = "ios";
    private String iosversion = UNKNOWN;
    private String iosmodel = UNKNOWN;
    private String iosserial = UNKNOWN;
    private String xeversion = "";
    private String iospolice = UNKNOWN;
    private String licenseLevel = null;
    private String licenseType = null;
    private ArrayList<String[]> cachedShowInventory = new ArrayList<>();
    private String deviceProfile = "null";

    // nso info
    private String rollBackOctal;

    // States
    private long lastReboot = 0;
    private String lastTransformedConfig = null;
    private String lastGetConfig = null;
    private Echo waitForEcho = Echo.WAIT;
    private String lastPrompt = "";
    private String warningsBuf = "";
    private boolean showRaw = false;
    private String syncFile = null;
    private String offlineData = null;
    private boolean ignoreNextWrite = false;
    private String failphase = "";
    private StringBuilder relock = new StringBuilder();

    // have show command:
    private boolean haveShowBoot = true;
    private boolean haveShowVtpStatus = true;
    private boolean haveShowVlan = true;
    private boolean haveShowVlanSwitch = true;
    private boolean haveShowSnmpUser = true;

    // NED-SETTINGS
    private ArrayList<String> dynamicWarning;
    private ArrayList<String[]> interfaceConfig;
    private ArrayList<String[]> injectConfig;
    private ArrayList<String[]> injectCommand;
    private ArrayList<String[]> injectAnswer;
    private ArrayList<String[]> replaceConfig;
    private ArrayList<String[]> replaceCommit;
    private String writeMemory;
    private String writeMemoryMode;
    private boolean writeTransferViaFile;
    private boolean writeIgnoreAbortErrors;
    private int applyRebootTimer;
    private String policeFormat;
    private int deviceOutputDelay;
    private int configOutputMaxRetries;
    private long configOutputRetryInterval;
    private int chunkSize;
    private String transIdMethod;
    private String showRunningConfig;
    private boolean useIpMrouteCacheDistributed;
    private boolean newIpACL;
    private String ipACLunorderedRegex;
    private boolean newSnmpServerHost;
    private boolean expandedLineVtyFormat;
    private boolean resequenceACL;
    private boolean includeCachedShowVersion;
    private boolean includeCachedShowInventory;
    private boolean autoInterfaceSwitchportStatus;
    private boolean autoBgpNbrPasswordPatch;
    private boolean autoStackwiseVirtualPatch;
    private String devPrepareDryModel;
    private Pattern[] plw;


    /*
     **************************************************************************
     * Constructors
     **************************************************************************
     */

    /**
     * NED cisco-ios constructor
     */
    public IOSNedCli() {
        super();
    }


    /**
     * NED cisco-ios constructor
     * @param deviceId
     * @param mux
     * @param trace
     * @param worker
     */
    public IOSNedCli(String deviceId,
                     NedMux mux,
                     boolean trace,
                     NedWorker worker) throws Exception {
        super(deviceId, mux, trace, worker);
        confRoot = "/ncs:devices/device{"+deviceId+"}/config/ios:";
    }


    /*
     **************************************************************************
     * setupParserContext
     **************************************************************************
     */

    /**
     * Override and init Schema.ParserContext with tailfned api info
     * @param
     */
    @Override
    protected void setupParserContext(Schema.ParserContext parserContext) {
        NedWorker worker = (NedWorker)parserContext.externalContext;
        if (parserContext.parserDirection == Schema.ParserDirection.TO_DEVICE) {
            traceVerbose(worker, "   [output parser] adding to-transaction data-provider");
            addTransactionDataProvider(parserContext);
        } else {
            traceVerbose(worker, "Adding /tailfned/police: "+iospolice);
            parserContext.addVirtualLeaf("/tailfned/police", iospolice);

            if (newIpACL) {
                traceVerbose(worker, "Adding /tailfned/api/new-ip-access-list");
                parserContext.addVirtualLeaf("/tailfned/api/new-ip-access-list", "");
            }
            if (resequenceACL) {
                traceVerbose(worker, "Adding /tailfned/api/resequence-access-list");
                parserContext.addVirtualLeaf("/tailfned/api/resequence-access-list", "");
            }
            if (newSnmpServerHost) {
                traceVerbose(worker, "Adding /tailfned/api/new-snmp-server-host");
                parserContext.addVirtualLeaf("/tailfned/api/new-snmp-server-host", "");
            }
            if (expandedLineVtyFormat) {
                traceVerbose(worker, "Adding /tailfned/api/expanded-line-vty-format");
                parserContext.addVirtualLeaf("/tailfned/api/expanded-line-vty-format", "");
            }
        }
    }


    /*
     **************************************************************************
     * nedSettingsDidChange
     **************************************************************************
     */

    /**
     * Called when ned-settings changed
     * @param
     * @throws Exception
     */
    @Override
    public void nedSettingsDidChange(NedWorker worker, Set<String> changedKeys, boolean isConnected) throws Exception {
        final long start = tick(0);
        logInfo(worker, "BEGIN nedSettingsDidChange");
        try {
            // cisco-ios auto interface-switchport-status - FIRST because read by other settings
            autoInterfaceSwitchportStatus = nedSettings.getBoolean("auto/interface-switchport-status");

            //
            // read
            //
            transIdMethod = nedSettings.getString("read/transaction-id-method");
            showRunningConfig = nedSettings.getString("read/show-running-method");
            if (showRunningConfig.startsWith("scp-transfer") && proto != null && !"ssh".equals(proto)) {
                throw new NedException("Must use CLI protocol ssh for read/show-running-method scp-transfer");
            }

            /*
             * read/replace-config
             */
            replaceConfig = new ArrayList<>();
            List<Map<String,String>> entries = nedSettings.getListEntries("read/replace-config");
            for (Map<String,String> entry : entries) {
                String[] newEntry = new String[4];
                newEntry[0] = entry.get(NSKEY); // "id"
                newEntry[1] = entry.get("regexp");
                newEntry[2] = entry.get("replacement");
                newEntry[3] = entry.get("when");
                String buf = "read/replace-config "+newEntry[0];
                buf += " regexp "+stringQuote(newEntry[1]);
                if (newEntry[1] == null) {
                    throw new NedException("ned-settings: read/replace-config "+newEntry[0]+" missing regexp");
                }
                if (newEntry[2] != null) {
                    buf += " to "+stringQuote(newEntry[2]);
                } else {
                    newEntry[2] = "";
                    buf += " filtered";
                }
                if (newEntry[3] != null) {
                    buf += " " + newEntry[3];
                }
                traceVerbose(worker, buf);
                replaceConfig.add(newEntry);
            }

            /*
             * read/inject-config
             */
            injectConfig = new ArrayList<>();
            entries = nedSettings.getListEntries("read/inject-config");
            for (Map<String,String> entry : entries) {
                String[] newEntry = new String[4];
                newEntry[0] = entry.get(NSKEY); // "id"
                newEntry[1] = entry.get("regexp");
                newEntry[2] = entry.get("config");
                newEntry[3] = entry.get("where");
                String buf = "read/inject-config "+newEntry[0];
                if (newEntry[2] == null) {
                    throw new NedException("ned-settings: "+buf+" missing config");
                }
                if (newEntry[1] != null) {
                    buf += " regexp "+stringQuote(newEntry[1]);
                }
                buf += " cfg "+stringQuote(newEntry[2]);
                if (newEntry[3] != null) {
                    buf += " " + newEntry[3];
                }
                traceVerbose(worker, buf);
                injectConfig.add(newEntry);
            }

            /*
             * read/inject-interface-config
             */
            interfaceConfig = new ArrayList<>();

            // Add a static global default 'no switchport' setting
            // Used for devices/interfaces which do not support switchport
            // or for devices which hide 'no switchport' when disabled, eg:
            // WS-C6504-E or CISCO7606-S or CISCO2901/K9
            if (!autoInterfaceSwitchportStatus) {
                String[] staticEntry = new String[3];
                staticEntry[0] = "Ethernet|Port-channel";
                staticEntry[1] = "no switchport";
                staticEntry[2] = "globstat-sp";
                traceVerbose(worker, "read/inject-interface-config "+staticEntry[2]
                             +" if "+stringQuote(staticEntry[0])
                             +" cfg "+stringQuote(staticEntry[1]));
                interfaceConfig.add(staticEntry);
            }
            entries = nedSettings.getListEntries("read/inject-interface-config");
            for (Map<String,String> entry : entries) {
                String[] newEntry = new String[3];
                newEntry[0] = entry.get("interface");
                newEntry[1] = entry.get("config");
                newEntry[2] = entry.get(NSKEY); // "id"
                if (newEntry[0] == null || newEntry[1] == null) {
                    throw new NedException("ned-settings: read/inject-interface-config "+newEntry[2]
                                           +" missing interface or config");
                }
                traceVerbose(worker, "read/inject-interface-config "+newEntry[2]
                             +" if "+stringQuote(newEntry[0])
                             +" cfg "+stringQuote(newEntry[1]));
                interfaceConfig.add(newEntry);
            }

            //
            // write
            //
            writeMemory = nedSettings.getString("write/memory-method");
            writeMemoryMode = nedSettings.getString("write/memory-setting");
            configOutputMaxRetries = nedSettings.getInt("write/config-output-max-retries");
            configOutputRetryInterval = (long)nedSettings.getInt("write/config-output-retry-interval");
            chunkSize = nedSettings.getInt("write/number-of-lines-to-send-in-chunk");
            deviceOutputDelay = nedSettings.getInt("write/device-output-delay");
            writeTransferViaFile = nedSettings.getBoolean("write/transfer-via-file");
            applyRebootTimer = nedSettings.getInt("write/apply-reboot-timer");
            writeIgnoreAbortErrors = nedSettings.getBoolean("write/ignore-abort-errors");

            /*
             * write/config-warning
             */
            dynamicWarning = new ArrayList<>();
            entries = nedSettings.getListEntries("write/config-warning");
            for (Map<String,String> entry : entries) {
                String key = entry.get(NSKEY);
                traceVerbose(worker, "write/config-warning "+key);
                dynamicWarning.add(stringDequote(key));
            }

            /*
             * write/replace-commit
             */
            replaceCommit = new ArrayList<>();
            entries = nedSettings.getListEntries("write/replace-commit");
            for (Map<String,String> entry : entries) {
                String[] newEntry = new String[4];
                newEntry[0] = entry.get(NSKEY); // "id"
                newEntry[1] = entry.get("regexp");
                newEntry[2] = entry.get("replacement");
                String buf = "write/replace-commit "+newEntry[0];
                buf += " regexp "+stringQuote(newEntry[1]);
                if (newEntry[1] == null) {
                    throw new NedException("ned-settings: write/replace-commit "+newEntry[0]+" missing regexp");
                }
                if (newEntry[2] != null) {
                    buf += " to "+stringQuote(newEntry[2]);
                } else {
                    newEntry[2] = "";
                    buf += " filtered";
                }
                traceVerbose(worker, buf);
                replaceCommit.add(newEntry);
            }

            /*
             * write/inject-command
             */
            injectCommand = new ArrayList<>();
            entries = nedSettings.getListEntries("write/inject-command");
            for (Map<String,String> entry : entries) {
                String[] newEntry = new String[4];
                newEntry[0] = entry.get(NSKEY); // "id"
                newEntry[1] = entry.get("config-line");
                newEntry[2] = entry.get("command");
                newEntry[3] = entry.get("where");
                if (newEntry[1] == null || newEntry[3] == null) {
                    throw new NedException("ned-settings: write/inject-command "+newEntry[0]
                                           +" missing config-line or where");
                }
                String buf = "write/inject-command "+newEntry[0]+" cfg "+stringQuote(newEntry[1]);
                if (newEntry[2] != null) {
                    buf += " cmd "+stringQuote(newEntry[2]);
                } else {
                    newEntry[2] = "";
                    buf += " filtered";
                }
                buf += " "+newEntry[3];
                traceVerbose(worker, buf);
                injectCommand.add(newEntry);
            }

            /*
             * write/inject-answer
             */
            injectAnswer = new ArrayList<>();
            entries = nedSettings.getListEntries("write/inject-answer");
            for (Map<String,String> entry : entries) {
                String[] newEntry = new String[4];
                newEntry[0] = entry.get(NSKEY); // "id"
                newEntry[1] = entry.get("question");
                newEntry[2] = entry.get("answer");
                newEntry[3] = entry.get("ml-question");
                if (newEntry[1] == null || newEntry[2] == null) {
                    throw new NedException("ned-settings: write/inject-answer "
                                           +newEntry[0]+" missing question or answer");
                }
                String buf = "write/inject-answer "+newEntry[0]
                    + " q " +stringQuote(newEntry[1])
                    + " a " +stringQuote(newEntry[2]);
                if (newEntry[3] != null) {
                    buf += " ml-q " +stringQuote(newEntry[3]);
                }
                traceVerbose(worker, buf);
                injectAnswer.add(newEntry);
            }

            // Create print_line_wait() pattern 'plw'
            plw = new Pattern[PLW0.length + injectAnswer.size()];
            for (int i = 0; i < PLW0.length; i++) {
                plw[i] = PLW0[i];
            }
            for (int i = 0; i < injectAnswer.size(); i++) {
                String[] entry = injectAnswer.get(i);
                plw[PLW0.length + i] = Pattern.compile(entry[1]);
            }

            //
            // auto
            //
            useIpMrouteCacheDistributed = nedSettings.getBoolean("auto/use-ip-mroute-cache-distributed");
            autoBgpNbrPasswordPatch = nedSettings.getBoolean("auto/bgp-nbr-password-patch");
            autoStackwiseVirtualPatch = nedSettings.getBoolean("auto/stackwise-virtual-if-indent-patch");

            //
            // api
            //
            policeFormat = nedSettings.getString("api/police-format");
            if (policeFormat == null) {
                policeFormat = "auto";  // Note: leaf-list does not support default statement
            }
            newIpACL = nedSettings.getBoolean("api/new-ip-access-list");
            resequenceACL = nedSettings.getBoolean("api/access-list-resequence");
            ipACLunorderedRegex = nedSettings.getString("api/unordered-ip-access-list-regex");
            newSnmpServerHost = nedSettings.getBoolean("api/new-snmp-server-host");
            expandedLineVtyFormat = nedSettings.getBoolean("api/expanded-line-vty-format");

            // developer
            devPrepareDryModel = nedSettings.getString("developer/prepare-dry-model");
            devTraceLevel = nedSettings.getInt("developer/trace-level");
            failphase = nedSettings.getString("developer/failphase");
            if (logVerbose && devTraceLevel < TRACE_DEBUG) {
                traceInfo(worker, "ned-settings log-verbose true => developer trace-level = 7 (DEBUG)");
                devTraceLevel = TRACE_DEBUG;
            }

            //
            // deprecated
            //
            includeCachedShowVersion = nedSettings.getBoolean("deprecated/cached-show-enable/version");
            includeCachedShowInventory = nedSettings.getBoolean("deprecated/cached-show-enable/inventory");

            // write config-archive *
            configArchive = new ConfigArchive(this);
            configArchive.init(worker);

            /*
             * write/config-dependency
             */
            setupNedDiff(worker);

            logInfo(worker, "DONE nedSettingsDidChange "+tickToString(start));

        } catch (Exception e) {
            throw new NedException("Failed to read ned-settings"+e.getMessage(), e);
        }
    }


    /*
     **************************************************************************
     * setupDevice
     **************************************************************************
     */

    /**
     * Setup device
     * @param
     * @return PlatformInfo
     * @throws Exception
     */
    protected PlatformInfo setupDevice(NedWorker worker) throws Exception {
        tracer = trace ? worker : null;
        final long start = tick(0);
        logInfo(worker, "BEGIN PROBE");

        //
        // Logged in, set terminal settings and check device type
        //
        try {
            // Set terminal settings
            print_line_exec(worker, "terminal length 0");
            print_line_exec(worker, "terminal width 0");

            // Show version
            String version = print_line_exec(worker, "show version");
            version = version.replace("\r", "");

            // Verify that this is an IOS device
            traceInfo(worker, "Inspecting version string");
            if (!version.contains("Cisco IOS Software")
                && !version.contains("Cisco Internetwork Operating")
                && !version.contains("Cisco Wide Area Application Services Software")) {
                throw new NedException("Unknown device :: " + version);
            }

            // Found IOS device, init NED
            traceVerbose(worker, "Found IOS device");

            // NETSIM
            if (version.contains("NETSIM")) {
                this.iosmodel = "NETSIM";
                this.iosversion = "cisco-ios-" + nedVersion;
                this.iosserial = device_id;

                // Show CONFD & NED version used by NETSIM in ned trace
                print_line_exec(worker, "show confd-state version");
                print_line_exec(worker, "show confd-state loaded-data-models data-model tailf-ned-cisco-ios");

                // Disable show commands for device only:
                traceInfo(worker, "Disabling all device show checks");
                haveShowBoot = haveShowVtpStatus = haveShowVlan = haveShowVlanSwitch = haveShowSnmpUser = false;
                showRunningConfig = "show running-config"; // Override SCP or other bad global setting
            }

            // REAL DEVICE
            else {

                // Cache show version License Type & Level
                licenseType = findLine(version, "License Type:");
                if (licenseType != null) {
                    licenseType = licenseType.substring(14);
                }
                licenseLevel = findLine(version, "License Level:");
                if (licenseLevel != null) {
                    licenseLevel = licenseLevel.substring(15).trim();
                    int b;
                    if ((b = licenseLevel.indexOf("Type:")) > 0) {
                        licenseType = licenseLevel.substring(b+6).trim();
                        licenseLevel = licenseLevel.substring(0,b).trim();
                    }
                }
                if (licenseType != null && licenseType.contains(" ")) {
                    licenseType = "\"" + licenseType + "\"";
                }

                // cached-show inventory (name and serial numbers)
                if (includeCachedShowInventory) {
                    cacheShowInventory(worker);
                }

                // Show current configuration id for debug purposes
                if ("config-id".equals(transIdMethod)) {
                    print_line_exec(worker, "show configuration id");
                }
            }

            //
            // Get iosname
            //
            if (version.contains("Cisco IOS XE Software")
                || version.contains("IOS-XE Software")
                || version.contains("Cisco IOS-XE software")) {
                this.iosname = "ios-xe";
            }

            //
            // Get iosmodel
            //
            Pattern p = Pattern.compile("\n[Cc]isco (\\S+) .*(?:processor |revision )");
            Matcher m = p.matcher(version);
            if (m.find()) {
                this.iosmodel = m.group(1);
            }

            //
            // Get iosversion (pick IOS version before XE version)
            //
            p = Pattern.compile("Cisco.*IOS Software.*Version ([0-9]+[A-Za-z0-9\\.():-]+[0-9a-zA-Z)]+)");
            m = p.matcher(version);
            if (m.find()) {
                this.iosversion = m.group(1);
            } else {
                // cat3550 and cat6500 version extraction do not trigger on the above regexp
                p = Pattern.compile("(?:Cisco)?.*IOS.*Software.*Version ([0-9]+[A-Za-z0-9\\.():-]+[0-9a-zA-Z)]+)");
                m = p.matcher(version);
                if (m.find()) {
                    this.iosversion = m.group(1);
                }
            }

            //
            // Get xeversion
            //
            p = Pattern.compile("Version(?:[:])? (03\\S+) ");
            m = p.matcher(version);
            if (m.find()) {
                this.xeversion = m.group(1);
            }

            //
            // Get iosserial
            //
            p = Pattern.compile("Processor board ID (\\S+)");
            m = p.matcher(version);
            if (m.find()) {
                this.iosserial = m.group(1);
            }

        } catch (Exception e) {
            logError(worker, "Failed to setup NED :: ", e);
            throw new NedException("Failed to setup NED :: "+e.getMessage(), e);
        }

        logInfo(worker, "DONE PROBE "+tickToString(start));
        return new PlatformInfo(iosname, iosversion, iosmodel, iosserial);
    }


    /**
     *
     * @param
     * @throws Exception
     */
    private void cacheShowInventory(NedWorker worker) throws Exception {
        setReadTimeout(worker);
        String res = print_line_exec(worker, "show inventory");
        String[] lines = res.split("NAME: ");
        for (int i = 0; i < lines.length; i++) {
            Pattern pattern = Pattern.compile("(\\\".*?\\\"), .*,\\s+SN: (.*)", Pattern.DOTALL);
            Matcher matcher = pattern.matcher(lines[i]);
            if (matcher.find()) {
                String[] entry = new String[2];
                entry[0] = matcher.group(1);
                entry[1] = matcher.group(2).trim();
                traceInfo(worker, "Adding cached-show inventory: NAME="+entry[0]+" SN="+entry[1]);
                cachedShowInventory.add(entry);
            }
        }
    }


    /*
     **************************************************************************
     * setupInstance
     **************************************************************************
     */

    /**
     * Setup NED instance
     * @param
     * @throws Exception
     */
    protected void setupInstance(NedWorker worker, PlatformInfo platformInfo) throws Exception {
        final long start = tick(0);
        logInfo(worker, "BEGIN SETUP");

        if (this.writeTimeout < this.readTimeout) {
            traceInfo(worker, "WARNING: write-timeout too low, reset to read-timeout value");
            this.writeTimeout = this.readTimeout; // API CHANGE helper
        }

        this.iosname = platformInfo.name;
        this.iosmodel = platformInfo.model;
        this.iosversion = platformInfo.version;
        this.iosserial = platformInfo.serial;

        setUserSession(worker);
        int th = maapi.startTrans(Conf.DB_RUNNING, Conf.MODE_READ);

        // Get iospolice
        this.iospolice = getIosPolice(worker, th, true);

        // Trace device profile
        try {
            String p = "/ncs:devices/device{"+device_id+"}/device-profile";
            if (maapi.exists(th, p)) {
                this.deviceProfile = ConfValue.getStringByValue(p, maapi.getElem(th, p));
            }
        } catch (MaapiException ignore) {
            // Ignore Exception
        }
        traceInfo(worker, "device-profile = " + this.deviceProfile);

        // Trace NSO features
        rollBackOctal = nsoCapabilityProps.getProperty("rollback-files-octal", "no");
        traceInfo(worker, "nso-features/rollback-files-octal = "+rollBackOctal);

        // Close transaction
        maapi.finishTrans(th);

        // Create utility classes used by IOS NED
        metaData = new MetaDataModify(this);
        locks = new NedLocks(this);
        secrets = new NedSecrets(this);
        defaults = new NedDefaults(this);
        nedAcl = new NedAcl(this);
        mCrypto = new MaapiCrypto(maapi);

        // ned-settings cisco-ios live-status exec-done-pattern
        String execDonePattern = nedSettings.getString("live-status/exec-done-pattern");
        if (execDonePattern == null) {
            // [cisco-ios] 'issu runversion'
            execDonePattern = "(Initiating active RP failover)|(Target RP will now reload)";
        }

        //
        // NedCommand default auto-prompts:
        //
        String[][] defaultAutoPrompts = new String[][] {
            { execDonePattern, "<exit>" },
            { "([!]{20}|[C]{20}|[.]{20})", "<timeout>" },
            { "\\[OK\\]", null },
            { "\\[Done\\]", null },
            { "timeout is \\d+ seconds:", null },  // ping
            { "Key data:", null }, // crypto key export rsa
            { " has the following attributes:", null }, // crypto pki authenticate
            { ":\\s*$", "<prompt>" },
            { "\\][\\?]?\\s*$", "<prompt>" }
        };
        nedCommand = new NedCommand(this, "ios-stats", "ios", PRIVEXEC_PROMPT, CONFIG_PROMPT,
                                    " Invalid input detected at ", defaultAutoPrompts);

        // Only setup liveStats for connected devices
        if (session != null) {

            // Setup custom show handler
            nedLiveStats.setupCustomShowHandler(new ShowHandler(this, session, NEDLIVESTATS_PROMPT));

            // Make NedLiveStats aware of the ietf-interface and ietf-ip modules.
            nedLiveStats.installParserInfo("if:interfaces-state/interface",
                                           "{'show':'show interfaces',"+
                                           "'template':'if:interfaces-state_interface.gili',"+
                                           "'show-entry':{'cmd':'show interfaces %s',"+
                                           "'template':'if:interfaces-state_interface.gili',"+
                                           "'trim-top-node':true,'run-after-show':false}}");

            nedLiveStats.installParserInfo("if:interfaces-state/if:interface/ip:ipv4/ip:address",
                               "{'show':{'cmd':'show run interface %s | include ip address','arg':['../../name']},"+
                               "'template':'if:interfaces-state_interface_ip:ipv4_address.gili'}");

            nedLiveStats.installParserInfo("if:interfaces-state/if:interface/ip:ipv6/ip:address",
                               "{'show':{'cmd':'show run interface %s | include ipv6 address','arg':['../../name']},"+
                               "'template':'if:interfaces-state_interface_ip:ipv6_address.gili'}");
        }

        logInfo(worker, "DONE SETUP "+tickToString(start));
    }


    /**
     * Setup NedDiff
     *    Rule syntax: line to move :: after|before :: line to stay
     * @param
     * @throws NedException
     */
    private void setupNedDiff(NedWorker worker) {

        nedDiff = new NedDiff(this, devTraceLevel > TRACE_DEBUG);

        //
        // Top-mode rules:
        //
        nedDiff.add(">no interface :: after :: ^route-map \\S+ (permit|deny) \\d+<LF> no match interface");
        nedDiff.add("^no ip prefix-list <STAY> .+$ :: before :: ^ip prefix-list <STAY> .+$");
        nedDiff.add("^no ipv6 prefix-list <STAY> .+$ :: before :: ^ipv6 prefix-list <STAY> .+$");
        nedDiff.add("~vtp mode :: before :: ^interface .+<LF>\n switchport private-vlan");
        nedDiff.add("^service-insertion .+ :: last");
        nedDiff.add("^interface AppNav[-].* :: after :: service-insertion .+");
        nedDiff.add(">interface <STAY> :: before :: "
                    +"^interface .+<LF>\n switchport backup interface <STAY> preemption mode forced");
        nedDiff.add(">no interface  :: after :: >router bgp ");
        nedDiff.add(">isdn leased-line :: before :: >interface ");
        nedDiff.add("^interface \\S+.*?\n ip(?:v6)? flow monitor <STAY> :: after :: >flow monitor <STAY>");
        nedDiff.add("^interface \\S+.*?\n no ip(?:v6)? flow monitor <STAY> :: before :: >flow monitor <STAY>");
        nedDiff.add(">>router ospf  :: before :: "+
                    "^router bgp \\d+<LF> address-family .+ vrf \\S+<LF>  redistribute ospf ");
        nedDiff.add(">no bridge-domain  :: after :: >interface ");

        // crypto
        nedDiff.add(">interface <STAY> :: before :: ^crypto pki trustpoint \\S+<LF>\n ip-address\\s+<STAY>");
        nedDiff.add(">no crypto map  :: after :: ^interface .+<LF>\n no(?: ipv6)? crypto map ");
        nedDiff.add(">no crypto gkm  :: after :: >no crypto map ");

        //
        // router bgp * / neighbor *
        //
        final String bgpMode = "router bgp \\d+";
        nedDiff.add(bgpMode, "^neighbor \\S+.*$ :: before :: ^address-family .+<LF>.+ exit-address-family");
        nedDiff.add(bgpMode, "^template peer-session \\S+<LF> no \\S+ :: after :: >no neighbor ");
        nedDiff.add(bgpMode, "=no neighbor <STAY> :: before :: >neighbor <STAY>");
        nedDiff.add(bgpMode, ">no neighbor <STAY> inherit peer-session :: after :: >no neighbor <STAY>");
        nedDiff.add(bgpMode, ">no template  :: after :: >no neighbor");
        nedDiff.add(bgpMode, ">no address-family  :: after :: ^no neighbor \\S+ remote-as \\d+$");

        // router bgp * / address-family * / neighbor *
        nedDiff.add("router bgp (\\d+) :: address-family .+",
                    ">no neighbor <STAY> inherit peer-session :: after :: >no neighbor <STAY>");

        // policy-map * / class * /
        nedDiff.add("policy-map (\\S+) :: class \\S+", ">no priority :: before :: >>police ");

        //
        // ip access-list standard|extended */
        // ipv6 access-list */
        //
        if (newIpACL) {
            // Make sure delete of access-list rules are before create
            nedDiff.add("ip access-list (standard|extended) \\S+", ">no  :: before :: ^[0-9].+$");
            nedDiff.add("ipv6 access-list \\S+", ">no  :: before :: >sequence ");
        }

        //
        // interface * / switchport
        //
        final String[] defaultNo = {
            "ip route-cache",
            "ip proxy-arp",
            "mdix auto",
            "keepalive",
            "logging event link-status",
            "bfd echo",
            "ip redirects"
        };
        final String[] anyLayer = {
            "lldp receive",
            "cdp enable",
            "macro auto processing",
            "port-type"
        };
        StringBuilder ignore = new StringBuilder("ignore");
        for (int d = 0; d < defaultNo.length; d++) {
            ignore.append(":=="+defaultNo[d]);
        }
        for (int d = 0; d < anyLayer.length; d++) {
            ignore.append(":=="+anyLayer[d]);
        }

        //
        // interface * / switchport (mode change to LAYER 2)
        //
        String mode = "interface .+ ,, switchport";
        nedDiff.add(mode, ">switchport :: first :: ignore:=switchport");
        nedDiff.add(mode, ">no  :: before :: >switchport :: "+ignore.toString());
        for (int r = 0; r < defaultNo.length; r++) {
            nedDiff.add(mode, "=" + defaultNo[r] + " :: before :: =switchport");
            nedDiff.add(mode, "=no " + defaultNo[r] + " :: after :: >switchport");
        }

        //
        // interface * / no switchport (mode change to LAYER 3)
        //
        mode = "interface .+ ,, no switchport";
        nedDiff.add(mode, ">no switchport :: last");
        nedDiff.add(mode, "=no switchport :: last");
        nedDiff.add(mode, "+ :: after :: =no switchport :: "+ignore.toString());
        for (int r = 0; r < defaultNo.length; r++) {
            nedDiff.add(mode, "=" + defaultNo[r] + " :: before :: >no switchport");
            nedDiff.add(mode, "=no " + defaultNo[r] + " :: after :: =no switchport");
        }

        //
        // interface * / [no] switchport [LAYER 2 & 3 + reinject]
        //
        String[] spRules = {
            "~ethernet cfm enable :: before :: =switchport mode dot1q-tunnel",
            ">no switchport port-security :: before :: ^no switchport mode (access|trunk)$" // NSO 4.6
        };
        for (int r = 0; r < spRules.length; r++) {
            nedDiff.add("interface .+ ,, (?:no )switchport", spRules[r]);
        }

        //
        // interface * /
        //
        String[] ifRules = {
            // interface * / ip address
            ">ip address  :: after :: ^(ip )?vrf \\S.*$",
            ">no ip address :: before :: >no encapsulation dot1Q ", // RT25447,RT33983
            ">no ip address  :: before :: ^no (ip )?vrf \\S.*$",

            // interface * / ipv6 address
            ">no ipv6 ospf :: before :: >no ipv6 address", // RT33785

            // interface * / media-type
            "^media-type \\S+$ :: after :: =no ip address",
            "^(media-type sfp|no media-type rj45)$ :: after :: ==mdix auto",
            "=media-type rj45 :: before :: ==mdix auto",

            // interface * / speed
            "=duplex auto :: before :: =speed auto",
            "=duplex full :: before :: >speed 1000",
            "=negotiation auto :: after :: >>speed",

            // interface * / no bfd echo
            "=no bfd echo :: after :: >bfd interval ",
            "=bfd echo :: before :: >no bfd interval",

            // interface * / tunnel mode
            ">no tunnel mpls :: before :: >no tunnel mode mpls",

            // interface * / port-type <-> ethernet dot1a
            "=no ethernet dot1ad nni :: before :: ^no port-type [en]ni",
            "=no ethernet dot1ad nni :: before :: =port-type eni",

            //">>ethernet dot1ad ($1) :: before :: ^no port-type (\\S+)",
            //">no ethernet dot1ad  :: before :: >port-type ",

            // interface * / lldp receive <-> port-type
            "^(lldp receive|cdp enable) :: before :: ^no port-type (nni|eni)",
            "^no (lldp receive|cdp enable) :: after :: ^(port-type (nni|eni))|(ethernet dot1ad nni)",

            // interface * / spanning-tree
            ">spanning-tree  :: after :: ^(port-type [en]ni)|(ethernet dot1ad nni)" // if-redeploy 'no spanning-tree'
        };
        for (int r = 0; r < ifRules.length; r++) {
            nedDiff.add("interface .+", ifRules[r]);
        }

        // Add user rules from ned-settings 'write config-dependency' list
        nedDiff.addNedSettings(worker, nedSettings);

        traceDebug2(worker, nedDiff.toString());
    }


    /**
     * NedLiveStatsShowHandler
     * @param
     * @throws Exception
     */
    private class ShowHandler extends NedLiveStatsShowHandler {
        private NedComCliBase owner;
        private CliSession session;
        private Pattern[] prompts;

        public ShowHandler(NedComCliBase owner, CliSession session, Pattern[] prompts)
            throws NedLiveStatsException {
            super(owner, session, prompts);
            this.owner = owner;
            this.session = session;
            this.prompts = prompts;
        }

        public String execute(NedWorker worker, String cmd) throws Exception {

            traceInfo(worker, "ShowHandler: "+stringQuote(cmd));

            // '!noop' used for dummy show-entry
            if (cmd.startsWith("!")) {
                return "";
            }

            // ned-setting cisco-ios developer simulate-show *
            String simulated = simulateShow(worker, cmd);
            if (simulated != null) {
                return simulated;
            }

            // NETSIM show command massage
            if (this.owner != null && this.owner.isNetsim()) {
                // Split interface name
                Pattern p = Pattern.compile("show run interface ([A-Za-z]+)([0-9]+\\S*)");
                Matcher m = p.matcher(cmd);
                if (m.find()) {
                    cmd = cmd.replace(m.group(1)+m.group(2), m.group(1)+" "+m.group(2));
                }

                // Insert "" around the include|exclude <regex>
                String[] args = cmd.split(" [|] (include|exclude) ");
                for (int i = 1; i < args.length; i++) {
                    cmd = cmd.replace(args[i], "\""+args[i]+"\"");
                }
            }

            // General show command massage
            if (cmd.startsWith("show bgp vpnv4 unicast all neighbors ")) {
                if (cmd.endsWith(" -")) {
                    // Strip any vrf, signified by "-" in the code
                    cmd = cmd.substring(0, cmd.length() - 2);
                } else {
                    Pattern p = Pattern.compile("show bgp vpnv4 unicast all neighbors (\\S+) (\\S+)");
                    Matcher m = p.matcher(cmd);
                    if (m.find()) {
                        cmd = "show bgp vpnv4 unicast vrf "+m.group(2)+" neighbors "+m.group(1);
                    }
                }
            }

            session.println(cmd);
            session.expect(Pattern.quote(cmd), worker);
            NedExpectResult res = session.expect(prompts, worker);

            return res.getText();
        }
    }


    /**
     * Look up simulated show command in ned-settings
     * @param
     * @return
     */
    protected String simulateShow(NedWorker worker, String line) {
        // ned-setting cisco-ios developer simulate-show *
        try {
            HashMap<String,String> map = new HashMap<>();
            String path = "developer/simulate-show{\""+line+"\"}/file";
            nedSettings.getMatching(map, path);
            if (map.size() > 0) {
                final String filename = map.get(path);
                if (filename != null) {
                    String output = readFile(filename);
                    if (output != null) {
                        traceInfo(worker, "Simulating '"+line+"' output from '"+filename+"':\n"+output);
                        return output;
                    }
                }
            }
        } catch (Exception e) {
            // Ignore
        }
        return null;
    }


    /**
     * Get data from devices device platform or cached config (deprecated)
     * @param
     * @return Value or "unknown
     * @throws Exception
     */
    protected String getPlatformData(int thr, String leaf) throws Exception {

        // First try devices device platform
        String p = "/ncs:devices/device{"+device_id+"}/platform/" + leaf;
        try {
            if (maapi.exists(thr, p)) {
                return ConfValue.getStringByValue(p, maapi.getElem(thr, p));
            }
        } catch (MaapiException ignore) {
            // Ignore Exception
        }

        // Second try config cached-show version
        if (includeCachedShowVersion) {
            p = confRoot + "cached-show/version/" + leaf;
            try {
                if (maapi.exists(thr, p)) {
                    return ConfValue.getStringByValue(p, maapi.getElem(thr, p));
                }
            } catch (MaapiException ignore) {
                // Ignore Exception
            }
        }

        return UNKNOWN;
    }


    /**
     * Get data police mode setting
     * @param
     * @return Value or default "cirmode"
     */
    private String getIosPolice(NedWorker worker, int thr, boolean cdbLookupOk) {
        String police;

        // (1) Specified in ned-setting cisco-ios api police-format
        if (!"auto".equals(policeFormat)) {
            police = policeFormat.replace(" ", "-");
            traceInfo(worker, "iospolice (ned-setting) = " + police);
            return police;
        }

        // (2) Specified in 'tailfned police'
        if (cdbLookupOk) {
            String p = confRoot + "tailfned/police";
            try {
                if (maapi.exists(thr, p)) {
                    police = ConfValue.getStringByValue(p, maapi.getElem(thr, p));
                    traceInfo(worker, "iospolice (tailfned) = " + police);
                    return police;
                }
            } catch (Exception ignore) {
                // Ignore Exception
            }
        }

        // (3) Auto-detect from iosmodel
        police = null;
        final String cirflat = "cirflat";
        if (iosmodel.contains("ME-3400")) {
            police = "cirmode";
        } else if (iosmodel.contains("C3550")) {
            police = "numflat";
        } else if (iosmodel.contains("C3750")) {
            police = cirflat;
        } else if (getMatch(iosmodel, "(C45(?:0[0-9]))") != null) {
            police = "cirmode-bpsflat";
        } else if (iosmodel.contains("ME-4924")) {
            police = "cirmode-bpsflat";
        } else if (getMatch(iosmodel, "(C65(?:0[3469]|13))") != null) {
            police = cirflat;
        } else if (iosmodel.contains("12404")) {
            police = cirflat;
        } else if (iosmodel.contains("Catalyst")) {
            police = "bpsflat";
        } else if (iosmodel.contains("vios-")) {
            this.iosname = "ViOS";
        } else if (iosmodel.contains("vios_l2")) {
            this.iosname = "ViOS";
            police = cirflat;
        } else if (iosmodel.contains("10000")) {
            police = "numflat";
        }
        if (police != null) {
            traceInfo(worker, "iospolice (auto-detected) = " + police);
        } else {
            police = "cirmode";
            traceInfo(worker, "iospolice (default) = " + police);
        }
        return police;
    }


    /*
     **************************************************************************
     * show
     **************************************************************************
     */

    /**
     * Retrieve running config from device
     * @param
     * @throws Exception
     */
    @Override
    public void show(NedWorker worker, String toptag) throws Exception {

        // Only respond to the first toptag
        if (!"interface".equals(toptag)) {
            worker.showCliResponse("");
            return;
        }

        final long start = tick(0);
        if (session != null && trace) {
            session.setTracer(worker);
        }
        final int toTh = worker.getToTransactionId();
        logInfo(worker, "BEGIN SHOW (th="+toTh+")");

        // Get config from device
        String res = "";
        try {
            lastGetConfig = getConfig(worker);
            res = modifyInput(worker, true, -1, lastGetConfig);
        } finally {
            this.lastTransformedConfig = null;
            this.syncFile = null;
        }

        // cisco-ios extended-parser
        try {
            locks.reset(worker);
            if (this.turboParserEnable) {
                traceInfo(worker, "Parsing config using turbo-mode");
                if (parseAndLoadXMLConfigStream(maapi, worker, schema, res)) {
                    res = ""; // Turbo-parser succeeded, clear config to bypass CLI
                }
            } else {
                traceInfo(worker, "Parsing config using robust-mode");
                res = filterConfig(res, schema, maapi, worker, null, false).toString();
            }
        } catch (Exception e) {
            logError(worker, "extended-parser "+nedSettings.getString(EXTENDED_PARSER)+" exception ERROR: ", e);
            this.turboParserEnable = false;
            this.robustParserMode = false;
        }

        logInfo(worker, "DONE SHOW "+tickToString(start));
        worker.showCliResponse(res);
    }


    /**
     * Get running-config from device
     * @param
     * @return
     * @throws Exception
     */
    private String getConfig(NedWorker worker) throws Exception {

        long progt = nedReportProgress(worker, "reading config...", 0);
        try {
            final long start = setReadTimeout(worker);
            String res;

            // showOffline
            if (this.offlineData != null) {
                logInfo(worker, "BEGIN reading config (showOffline)");
                res = this.offlineData;
            }

            // devices device <dev> live-status exec any sync-from-file <file>
            else if (syncFile != null) {
                logInfo(worker, "BEGIN reading config (sync-from-file "+syncFile+")");
                res = print_line_exec(worker, "file show " + syncFile);
                if (res.contains("Error: failed to open file")) {
                    throw new NedException("failed to sync from file " + syncFile);
                }
                res = res.replace("\r\r\r\n", "\r\n");
                res = res.replace("\r\r\n", "\r\n");
            }

            // ned-settings cisco-ios read show-running-method scp-transfer
            else if (showRunningConfig.startsWith("scp-transfer")) {
                logInfo(worker, "BEGIN reading config ("+showRunningConfig+")");
                if (nedSettings.getString("proxy/remote-connection") != null) {
                    throw new NedException("read/show-running-method scp-transfer is not supported with proxy mode");
                }
                try {
                    res = scpGetConfig(worker);
                } catch (Exception e) {
                    if (!"scp-transfer-fallback".equals(showRunningConfig)) {
                        throw e;
                    }
                    logInfo(worker, "WARNING: SCP transfer failed '"+e.getMessage()+"' - fallback to show run");
                    res = print_line_exec(worker, "show running-config");
                }
            }

            // <command>
            else {
                logInfo(worker, "BEGIN reading config ("+showRunningConfig+")");
                res = print_line_exec(worker, showRunningConfig);
                if (res.contains("Invalid input detected")) {
                    throw new NedException("failed to show config using '"+showRunningConfig+"'");
                }
            }

            // Trim running-config
            res = trimConfig(worker, res);

            // Online device - call additional show commands
            if (session != null && isDevice()) {
                lastTimeout = setReadTimeout(worker);

                // Insert missing 'show boot' config from show boot
                res = res + getConfigBoot(worker);

                // Insert config shown in "show version"
                res = res + getConfigVersion(worker);

                // Insert 'logging console X' shown in "show logging"
                res = getConfigLogging(worker) + res;

                // Insert missing VLAN config from show vlan
                res = res + getConfigVlan(worker, res);
            }

            // Done
            logInfo(worker, "DONE reading config "+tickToString(start));
            nedReportProgress(worker, "reading config ok", progt);
            return res;

        } catch (Exception e) {
            nedReportProgress(worker, "reading config error", progt);
            throw e;
        }
    }


    /**
     * Show boot config
     * @param
     * @return
     * @throws Exception
     */
    private String getConfigBoot(NedWorker worker) throws Exception {
        if (!haveShowBoot) {
            return "";
        }

        traceInfo(worker, "reading config - show boot");
        String boot = print_line_exec(worker, "show boot");
        if ((boot = findLine(boot, "BOOT path-list:")) != null) {
            boot = boot.substring(15).trim();
            if (!boot.isEmpty()) {
                boot = "boot system " + boot;
                return boot.trim() + "\n";
            }
        }

        traceInfo(worker, "Disabling 'show boot' check");
        haveShowBoot = false;
        return "";
    }


    /**
     * Show logging config
     * @param
     * @return
     * @throws Exception
     */
    private String getConfigLogging(NedWorker worker) throws Exception {

        traceInfo(worker, "reading config - show logging");

        // NOTE: Not supported on older IOS versions [12.2(33)]
        String showbuf = print_line_exec(worker, "show logging xml");
        if (isExecError(showbuf)) {
            traceInfo(worker, "WARNING: unable to determine logging status due to too old IOS");
            return "";
        }

        String res = "";
        res += getConfigLoggingType(showbuf, "console");
        res += getConfigLoggingType(showbuf, "monitor");
        res += getConfigLoggingType(showbuf, "buffer");
        traceInfo(worker, "transformed <= inserted "+stringQuote(res)+" from 'show logging xml'");
        return "\n" + res;
    }


    /**
     *
     * @param
     * @return
     */
    private String getConfigLoggingType(String showbuf, String type) {
        String name = type + "-logging";
        String line = findLine(showbuf, "<"+name);
        if (line == null) {
            return "";
        }
        if (line.trim().startsWith("<"+name+">disabled<")) {
            return "";
        }
        Pattern p = Pattern.compile("<"+name+" level=\"(\\S+?)\" ");
        Matcher m = p.matcher(line);
        if (!m.find()) {
            return "";
        }

        if ("buffer".equals(type)) {
            type = "buffered";
        }
        return "logging " + type + " " + m.group(1) + "\n";
    }


    /**
     *
     * @param
     * @return
     * @throws Exception
     */
    private String getConfigVersion(NedWorker worker) throws Exception {

        traceInfo(worker, "reading config - show version");

        String showbuf = print_line_exec(worker, "show version | include password-recovery");
        if (!showbuf.contains("password-recovery")) {
            return "";
        }
        if (showbuf.contains("enabled")) {
            traceInfo(worker, "transformed <= inserted 'service password-recovery' from 'show version'");
            return "\nservice password-recovery\n";
        }
        if (showbuf.contains("disabled")) {
            traceInfo(worker, "transformed <= inserted 'no service password-recovery' from 'show version'");
            return "\nno service password-recovery\n";
        }

        return "";
    }


    /**
     * Extract vlan config from show vlan
     * @param
     * @return
     * @throws Exception
     */
    private String getConfigVlan(NedWorker worker, String dump) throws Exception {
        String res;
        int i;
        boolean vtpClient = false;
        StringBuilder vtpResult = new StringBuilder("\n");
        if (haveShowVtpStatus) {

            traceInfo(worker, "reading config - show vtp status");
            String vtpStatus = print_line_simulated(worker, "show vtp status");
            if (isExecError(vtpStatus)) {
                traceInfo(worker, "Disabling 'show vtp status' check");
                haveShowVtpStatus = false;
            }

            // Extract VTP "config" from 'show vtp status'
            else {
                // vtp mode
                if ((res = findLine(vtpStatus, "VTP Operating Mode")) != null) {
                    String mode = res.replaceAll("VTP Operating Mode\\s+:\\s+(\\S+)", "$1").trim().toLowerCase();
                    vtpResult.append("vtp mode " + mode + "\n");
                    if ("client".equals(mode)) {
                        vtpClient = true;
                    }
                }

                // vtp domain
                if ((res = findLine(vtpStatus, "VTP Domain Name")) != null
                    && (i = res.indexOf(':')) > 0) {
                    String value = res.substring(i+1).trim();
                    if (!value.isEmpty()) {
                        vtpResult.append("vtp domain " + value + "\n");
                    }
                }

                // vtp version
                if ((res = getMatch(vtpStatus, "VTP Version[ ]+[:] running VTP(\\d+)")) != null) {
                    vtpResult.append("vtp version " + res + "\n");
                } else if (((res = findLine(vtpStatus, "VTP version running")) != null // 2960 & 7600
                     || (res = findLine(vtpStatus, "VTP Version")) != null) // 4500
                    && (i = res.indexOf(':')) > 0) {
                    String value = res.substring(i+1).trim();
                    if (!value.isEmpty()) {
                        vtpResult.append("vtp version " + value + "\n");
                    }
                }

                // vtp pruning
                if ((res = findLine(vtpStatus, "VTP Pruning Mode")) != null) {
                    String value = res.replaceAll("VTP Pruning Mode\\s+:\\s+(\\S+)", "$1").trim();
                    if ("Enabled".equals(value)) {
                        vtpResult.append("vtp pruning\n");
                    }
                }

                traceInfo(worker, "transformed <= inserted "+stringQuote(vtpResult.toString())
                          +" from 'show vtp status'");
            }
        }

        // If VTP Client, do not add vlan's to config.
        if (vtpClient) {
            traceInfo(worker, "Found VTP Client, do not list vlan(s) using show vlan");
            return vtpResult.toString();
        }

        //
        // Add vlan entries:
        //

        // First try 'show vlan'
        res = "";
        if (haveShowVlan) {
            traceInfo(worker, "reading config - show vlan");
            res = print_line_simulated(worker, "show vlan");
            if (res.indexOf("\n----") < 0) {
                traceInfo(worker, "Disabling 'show vlan' check");
                haveShowVlan = false;
            }
        }

        // If that fails, then try 'show vlan-switch'
        if (haveShowVlanSwitch && !haveShowVlan) {
            traceInfo(worker, "reading config - show vlan-switch");
            res = print_line_simulated(worker, "show vlan-switch");
            if (isExecError(res)) {
                traceInfo(worker, "Disabling 'show vlan-switch' check");
                haveShowVlanSwitch = false;
                return vtpResult.toString();
            }
        }

        // No support for either show vlan or show vlan-switch
        if (res.isEmpty() || (!haveShowVlanSwitch && !haveShowVlan)) {
            return vtpResult.toString();
        }

        // Strip all text before first entry
        if ((i = res.indexOf("\n----")) < 0) {
            return vtpResult.toString();
        }
        if ((i = res.indexOf('\n', i+1)) < 0) {
            return vtpResult.toString();
        }
        res = res.substring(i+1);

        // Parse lines, create:
        // vlan #
        //  name <name>
        // !
        String[] vlans = res.split("\r\n");
        StringBuilder sb = new StringBuilder();
        for (i = 0; i < vlans.length; i++) {
            if (vlans[i] == null || "".equals(vlans[i])) {
                break;
            }
            // Skip multi line entries. Each new starts with a digit
            if (!Character.isDigit(vlans[i].trim().charAt(0))) {
                continue;
            }
            String[] tokens = vlans[i].split(" +");
            if (tokens.length < 3) {
                break;
            }
            int status = vlans[i].indexOf(" active ");
            if (status < 0) {
                continue;
            }
            String vlan = "vlan " + tokens[0];
            if (dump.contains("\n"+vlan+"\r") || dump.contains("\n"+vlan+" \r")) {
                continue;
            }
            sb.append(vlan + "\n");

            // vlan * / name
            String name = vlans[i].substring(tokens[0].length(), status).trim();
            if (!name.isEmpty()
                && !(name.startsWith("VLAN") && name.endsWith(tokens[0]))) { // ignore default names
                sb.append(" name "+name+"\n");
            }

            sb.append("!\n");
        }
        if (sb.length() > 0) {
            traceInfo(worker, "transformed <= inserted "+stringQuote(sb.toString())+" from 'show vlan[-switch]'");
        }

        return vtpResult.toString() + sb.toString();
    }


    /**
     *
     * @param
     * @return
     * @throws Exception
     */
    private String scpGetConfig(NedWorker worker) throws Exception {

        final int retryCount = nedSettings.getInt("connection/number-of-retries");
        final int waitTime = nedSettings.getInt("connection/time-between-retry");

        // Connect using SSH
        Connection scpConn = new Connection(ip.getHostAddress(), port);
        for (int retries = retryCount; retries >= 0; retries--) {
            traceInfo(worker, "SCP connecting to " + ip.getHostAddress()
                      +":"+port+" ["+(1+retryCount-retries)+"/"+retryCount+"]");
            try {
                scpConn.connect(null, 0, connectTimeout);
                break;
            } catch (Exception e) {
                if (retries == 0) {
                    throw new NedException("read/show-running-method scp-transfer failed to open SCP connection", e);
                } else {
                    resetTimeout(worker, this.connectTimeout + (waitTime * 1000), 0);
                    sleep(worker, waitTime * (long)1000, true);
                }
            }
        }

        // Authenticate SSH connection
        scpConn.authenticateWithPassword(ruser, pass);
        if (!scpConn.isAuthenticationComplete()) {
            throw new NedException("read/show-running-method scp-transfer isAuthenticationComplete() = false");
        }
        traceInfo(worker, "SCP authenticated");

        // Send SCP get command
        final String file = "running-config";
        traceInfo(worker, "SCP fetching file: " + file);
        ch.ethz.ssh2.Session scpSession = scpConn.openSession();
        scpSession.execCommand("scp -f " + file);

        // Get running-config file
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        try {
            SCPClient scpClient = new SCPClient(scpConn);
            InputStream in = new SCPInputStream(scpClient, scpSession);
            reader = new BufferedReader(new InputStreamReader(in));
            String line;
            lastTimeout = setReadTimeout(worker);
            while ((line = reader.readLine()) != null) {
                sb.append(line+"\r\n");
                lastTimeout = resetReadTimeout(worker, lastTimeout);
            }
        } catch (Exception e) {
            throw new NedException("SCP download Exception: "+e.getMessage(), e);
        } finally {
            if (reader != null) {
                reader.close();
            }
            if (scpSession != null) {
                scpSession.close();
            }
            scpConn.close();
        }

        String res = sb.toString();
        traceInfo(worker, "SCP got "+res.length()+" bytes");

        // Replace single char '^C' with two char ^C
        byte[] bytes = res.getBytes();
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < res.length(); ++i) {
            if (bytes[i] == 3) {
                result.append("^");
                result.append("C");
            } else {
                result.append(res.charAt(i));
            }
        }
        res = result.toString();
        traceVerbose(worker, "\nSHOW_SCP:\n'"+res+"'");
        return res;
    }


    /**
     *
     * @param
     * @return
     */
    private String trimConfig(NedWorker worker, String res) {
        int d;

        // Strip everything before and including the following comments:
        int i = res.indexOf("Current configuration");
        if (i >= 0 && (d = res.indexOf('\n', i)) > 0) {
            res = res.substring(d+1);
        }

        i = res.indexOf("Last configuration change");
        if (i >= 0 && (d = res.indexOf('\n', i)) > 0) {
            res = res.substring(d+1);
        }

        i = res.indexOf("No configuration change since last restart");
        if (i >= 0 && (d = res.indexOf('\n', i)) > 0) {
            res = res.substring(d+1);
        }

        i = res.indexOf("No entries found.");
        if (i >= 0 && (d = res.indexOf('\n', i)) > 0) {
            res = res.substring(d+1);
        }

        i = res.lastIndexOf("NVRAM config last updated"); // multiple entries
        if (i >= 0 && (d = res.indexOf('\n', i)) > 0) {
            res = res.substring(d+1);
        }

        // Strip all text after and including the last 'end'
        i = res.lastIndexOf("\nend");
        if (i >= 0) {
            res = res.substring(0,i);
        }

        // Strip clock-period, device may change it, i.e. not config
        res = stripLineAll(worker, res, "ntp clock-period");

        // Strip console log messages
        res = stripLineAll(worker, res, "%");

        // Strip incomplete comments (e.g. crypto ikev2 profile)
        res = stripLineAll(worker, res, "! Profile incomplete");
        res = stripLineAll(worker, res, "! This profile is incomplete");

        // After reading/stripping device config, trim for consistency
        res = res.trim() + "\r\n";

        return res;
    }


    /**
     * NETSIM line-by-line input transformations
     * @param
     * @return
     */
    private String modifyInputNetsim(NedWorker worker, String res) {
        String toptag = "";
        StringBuilder sb = new StringBuilder();
        String[] lines = res.split("\n");
        for (int n = 0; n < lines.length; n++) {
            String line = lines[n];
            String trimmed = line.trim();
            if (trimmed.isEmpty()) {
                continue;
            }
            String ninput = null;

            // Update toptag
            if (isTopExit(line)) {
                toptag = "";
            } else if (Character.isLetter(line.charAt(0))) {
                toptag = trimmed;
            }

            //
            // ! meta-data ::
            // ! exit-meta-data-
            if (trimmed.startsWith(META_DATA) || trimmed.startsWith("! exit-meta-data-")) {
                ninput = "";
            }

            //
            // ' description '
            //
            else if (line.contains(" description ")) {
                ninput = quoteDescription(toptag, line);
            }

            //
            // voice translation-rule * / rule
            //
            else if ("no".equals(rollBackOctal)
                     && toptag.startsWith("voice translation-rule ") && trimmed.startsWith("rule ")) {
                ninput = line.replace("\\", "\\\\");
            }

            //
            // line * / exec-timeout 10 0
            //
            else if (toptag.startsWith("line ") && line.startsWith("line ")) {
                traceVerbose(worker, "transformed <= injected: 'exec-timeout 10 0' first in "+trimmed);
                sb.append(line+"\n exec-timeout 10 0\n");
                continue;
            }

            //
            // Append (modified) line to buffer
            //
            if (ninput != null && !ninput.equals(lines[n])) {
                if (ninput.isEmpty()) {
                    traceVerbose(worker, "transformed <= stripped '"+trimmed+"'");
                    continue;
                }
                traceVerbose(worker, "transformed <= '"+trimmed+"' to '"+ninput.trim()+"'");
                sb.append(ninput+"\n");
            } else if (lines[n] != null && !lines[n].isEmpty()) {
                sb.append(line+"\n");
            }
        }

        return sb.toString();
    }


    /**
     * Modify input from device
     * @param
     * @return
     * @throws Exception
     */
    private String modifyInput(NedWorker worker, boolean isShow, int toTh, String res) throws Exception {
        final long start0 = tick(0);
        int i;
        String match;
        String[] group;

        logInfo(worker, "BEGIN in-transforming");
        lastTransformedConfig = null;
        res = "\n" + res;

        //
        // Inject config
        //
        res = injectInput(worker, isShow, toTh, res);

        //
        // NETSIM - transform and leave early
        //
        if (isNetsim() && syncFile == null && offlineData == null) {
            res = modifyInputNetsim(worker, res);
            logInfo(worker, "DONE in-transforming (NETSIM) "+tickToString(start0));
            traceVerbose(worker, "\nSHOW_AFTER:\n"+res);
            return res;
        }

        //
        // REAL DEVICES BELOW
        //

        //
        // Quote multi-line texts
        //   group(1) = command
        //   group(3) = text to quote
        //   group(4) = additional unquoted append (optional)
        //
        traceInfo(worker, "in-transforming - quoting multi-line texts");
        String[] quoteTexts = {
            // menu <name> title ^C
            // <title text>
            // ^C
            "\n(menu \\S+ title) (\\^C)(.*?)\\^C",

            // aaa authentication fail-message
            "\n(aaa authentication fail-message) (\\^C)(.*?)\\^C",

            //   macro name <name>
            //    xxx
            //    yyy
            //   @
            "\n(macro name \\S+)(\r\n)(.*?\r\n)@",

            // banner <name>
            "\n(banner \\S+) (\\S\\S)(.*?)\\2\\S*?\r",

            // certificate <name>
            //  aaa bbb ccc
            //  ... ... ...
            //  xxx yyy zzz
            // \tquit
            "\n( certificate .*?(\r\n))(.*?\r\n)[ \t]+(quit)"
        };
        for (int n = 0; n < quoteTexts.length; n++) {
            Pattern p = Pattern.compile(quoteTexts[n], Pattern.DOTALL);
            Matcher m = p.matcher(res);
            StringBuffer sb = new StringBuffer();
            while (m.find()) {
                String line = m.group(1);
                String quoted = stringQuote(m.group(3));
                if (m.groupCount() == 4) {
                    quoted += ("\r\n" + m.group(4));
                }
                traceVerbose(worker, "transformed <= quoted '"+line+"' text");
                m.appendReplacement(sb, Matcher.quoteReplacement("\n"+line+" "+quoted));
            }
            m.appendTail(sb);
            res = sb.toString();
        }

        //
        // MAIN LINE-BY-LINE LOOP
        //
        traceInfo(worker, "in-transforming - line-by-line loop");
        String toptag = "";
        String[] lines = res.split("\n");
        StringBuilder sbin = new StringBuilder();
        for (int n = 0; n < lines.length; n++) {
            String line = lines[n];
            String trimmed = line.trim();
            if (trimmed.isEmpty()) {
                continue;
            }
            String input = null;
            boolean silent = false;

            // Update toptag
            if (isTopExit(line)) {
                toptag = "";
            } else if (Character.isLetter(line.charAt(0))) {
                toptag = trimmed;
            }

            //
            // ' description '
            //
            if (line.contains(" description ")) {
                input = quoteDescription(toptag, line);
                silent = (devTraceLevel < TRACE_DEBUG2);
            }

            //
            // errdisable
            //
            else if (toptag.startsWith("errdisable")) {
                input = line.replace("channel-misconfig (STP)", "channel-misconfig");
            }

            //
            // class-map * / match vlan *
            //
            else if (toptag.startsWith("class-map ") && trimmed.startsWith("match vlan ")
                     && (match = getMatch(trimmed, "match vlan[ ]+(.+)")) != null) {
                input = line.replace(match,match.replace(" ", ","));
            }

            //
            // interface * / ntp broadcast key <key> destination <address>
            //
            else if (toptag.startsWith("interface ")
                     && trimmed.startsWith("ntp broadcast ") && trimmed.contains(" destination ")) {
                // Move destination address (list key) first.
                input = line.replaceFirst("ntp broadcast (.*?) (destination \\S+)",
                                          "ntp broadcast $2 $1");
            }

            //
            // interface * / service-policy in|out
            //
            else if (toptag.startsWith("interface ")
                     && (line.startsWith("  service-policy in ") || line.startsWith("  service-policy out "))) {
                input = line.replace("  service-policy in ", "  service-policy input ");
                input = input.replace("  service-policy out ", "  service-policy output ");
            }

            //
            // controller SONET *
            //
            else if (toptag.startsWith("controller SONET ")) {
                if (line.contains(" clock source ")) {
                    // controller SONET * / au-3 * / tug-2 * t1 * clock source
                    input = line.toLowerCase();
                } else {
                    // controller SONET * / sts-1 x - y mode sts-3c
                    input = line.replaceFirst("sts-1 (.*?) mode ", "sts-1 \"$1\" mode ");
                }
            }

            //
            // crypto pki server
            //
            else if (toptag.startsWith("crypto pki server ") && trimmed.startsWith("issuer-name ")) {
                input = line.replaceFirst("issuer-name (.*)", "issuer-name \"$1\"");
            }

            //
            // crypto isakmp policy * / encr
            //
            else if (toptag.startsWith("crypto isakmp policy ") && line.startsWith(" encryption ")) {
                input = line.replace("encryption ", "encr ");
            }

            //
            // ip explicit-path * / index
            //
            else if (toptag.startsWith("ip explicit-path ") && line.startsWith("ip explicit-path ")) {
                // insert missing 'index <value>' (not shown in running-config)
                int index = 0;
                boolean ipepmod = false;
                for (i = n + 1; i < lines.length; i++) {
                    if (lines[i].startsWith(" index ")
                               && (match = getMatch(lines[i], " index (\\d+) ")) != null) {
                        index = Integer.parseInt(match);
                    } else if (lines[i].startsWith(" next-address ")
                               || lines[i].startsWith(" exclude-address ")) {
                        ipepmod = true;
                        lines[i] = " index " + (++index) + lines[i];
                    } else if (!lines[i].trim().isEmpty()) {
                        break;
                    }
                }
                if (ipepmod) {
                    traceVerbose(worker, "transformed <= injected index(es) in '"+line+"'");
                }
            }

            //
            // ip access-list unordered standard|extended *
            //
            else if ((line.startsWith("ip access-list extended ") || line.startsWith("ip access-list standard "))
                && getMatch(trimmed, "^ip access-list (?:standard|extended) ("+ipACLunorderedRegex+")\\s*$") != null) {
                input = line.replace("ip access-list ", "ip access-list unordered ");
            }

            //
            // ip access-list - ned-settings cisco-ios api access-list-resequence
            //
            else if (resequenceACL && line.startsWith("ip access-list extended ")) {
                sbin.append(lines[n]+"\n");
                for (n = n + 1; n < lines.length; n++) {
                    if ("!".equals(lines[n].trim())) {
                        break;
                    }
                    if (lines[n].trim().startsWith("remark ")) {
                        traceVerbose(worker, "transformed <= stripped '"+lines[n].trim()+"'");
                        continue;
                    }
                    if ((match = getMatch(lines[n], "^( \\d+) .*$")) != null) {
                        String stripped = lines[n].replace(match, "");
                        traceVerbose(worker, "transformed <= '"+lines[n]+"' to '"+stripped+"'");
                        sbin.append(stripped+"\n");
                    } else {
                        sbin.append(lines[n]+"\n");
                    }
                }
            }

            //
            // ipv6 access-list - ned-settings cisco-ios api new-ip-access-list
            //
            else if (newIpACL && line.startsWith("ipv6 access-list ")) {
                // Note: device trims sequence numbers spaced 10 from the previous
                sbin.append(lines[n]+"\n");
                int sequence = 0;
                for (n = n + 1; n < lines.length; n++) {
                    if ("!".equals(lines[n].trim())) {
                        break;
                    }
                    if ((match = getMatch(lines[n], "^ sequence (\\d+) ")) != null) {
                        sequence = Integer.parseInt(match);
                        sbin.append(lines[n]+"\n");
                    } else {
                        sequence = sequence + 10;
                        traceVerbose(worker, "transformed <= injected sequence number "+sequence+" in '"+lines[n]+"'");
                        sbin.append(" sequence "+sequence+lines[n]+"\n");
                    }
                }
            }

            //
            // ip source binding *
            //
            else if (trimmed.startsWith("ip source binding ")
                     && (match = getMatch(trimmed, "interface ([A-Za-z]+)\\d+")) != null) {
                String ifname = expandInterfaceName(match);
                input = line.replace("interface "+match, "interface "+ifname);
            }

            //
            // redirect server-group * / server ip * port *
            //
            else if (toptag.startsWith("redirect server-group ")
                     && line.startsWith(" server ip") && !line.contains(" port")) {
                input = " " + trimmed + " port 0";
            }

            //
            // logging discriminator *
            //
            else if (toptag.startsWith("logging discriminator ")) {
                String[] tokens = trimmed.split("(?= (?:severity|facility|mnemonics|msg-body))");
                input = tokens[0];
                for (int t = 1; t < tokens.length; t++) {
                    if ((match = getMatch(tokens[t], "\\S+ (?:drops|includes) (.+)")) != null) {
                        tokens[t] = tokens[t].replace(match, stringQuote(match));
                    }
                    input += (" "+tokens[t]);
                }
            }

            //
            // policy-map
            //
            else if (toptag.startsWith("policy-map ")) {

                // policy-map * / class * / random-detect drops 'precedence-based' name
                if (trimmed.startsWith("random-detect aggregate")) {
                    input = line.replace("random-detect aggregate",
                                         "random-detect precedence-based aggregate");
                } else if ("random-detect".equals(trimmed)) {
                    input = line.replace("random-detect", "random-detect precedence-based");
                } else if (line.contains(" mark-probability")) {
                    input = line.replace(" mark-probability", " mark-prob");
                }

                // 'policy-map * / class * / police' string replacement
                else if (trimmed.startsWith("police ")) {
                    if (trimmed.startsWith("police cir ")
                        || trimmed.startsWith("police rate ")
                        || trimmed.startsWith("police aggregate ")
                        || trimmed.matches("police (\\d+) bps (\\d+) byte.*")) {
                        // Ignore police cir/rate/aggregate and "bpsflat " bps&byte (Catalyst)
                        sbin.append(lines[n]+"\n");
                        continue;
                    }
                    if (hasPolice("cirmode") || hasPolice("cirflat")) {
                        // Insert missing [cir|bc|be]
                        input = line.replaceAll("police (\\d+) (\\d+) (\\d+)",
                                                "police cir $1 bc $2 be $3");
                        input = input.replaceAll("police (\\d+) (\\d+)",
                                                 "police cir $1 bc $2");
                        input = input.replaceAll("police (\\d+)",
                                                 "police cir $1");
                    }
                }
            }

            //
            // spanning-tree mst configuration / instance * vlan <val>, <val2>
            //
            else if (toptag.startsWith("spanning-tree mst configuration")
                     && findString(" instance [0-9]+ vlan ", line) >= 0) {
                input = line.replace(", ", ",");
            }

            //
            // monitor session * filter vlan *
            // monitor session * source vlan *
            // monitor session * source remote vlan *
            // monitor session * destination remote vlan *
            //
            else if (toptag.startsWith("monitor session ") && trimmed.contains(" vlan ")) {
                input = line.replace(" , ",",").replace(" - ","-");
            }

            //
            // l2tp-class / password encryption aes
            //
            else if (toptag.startsWith("l2tp-class ") && "password encryption aes".equals(trimmed)) {
                input = "";
            }

            //
            // crypto keyring / ! Keyring unusable for nonexistent vrf
            //
            else if (toptag.startsWith("crypto keyring ")
                     && trimmed.contains("! Keyring unusable for nonexistent vrf")) {
                input = line.replace("! Keyring unusable for nonexistent vrf", "");
            }

            //
            // parameter-map type regexp * / pattern *
            //
            else if (toptag.startsWith("parameter-map type regex ")
                     && trimmed.startsWith("pattern ")
                     && (match = getMatch(trimmed, "pattern (.*)")) != null) {
                input = line.replace(match, stringQuote(match));
            }

            //
            // router bgp * / address-family ipv4 vrf *
            //
            else if (toptag.startsWith("router bgp ")
                     && (trimmed.startsWith("address-family ipv4 vrf ")
                         || trimmed.startsWith("address-family ipv6 vrf "))) {
                input = line.replaceFirst(" vrf ", " unicast vrf ");
            }

            //
            // track * ipv6 route
            //
            else if (toptag.startsWith("track ") && trimmed.contains(" ipv6 route :: ")) {
                input = line.replace(" :: ", " ::/0 ");
            }

            //
            // et-analytics / inactive-timeout
            //
            else if (toptag.startsWith("et-analytics")
                     && trimmed.startsWith("inactive_timeout ")) {
                input = line.replace("inactive_timeout", "inactive-timeout");
            }

            // interface * + interface * / stackwise-virtual
            else if (autoStackwiseVirtualPatch && toptag.startsWith("interface ")
                     && line.startsWith(" interface ") && res.contains(" stackwise-virtual ")) {
                input = line.substring(1);
            }

            //
            // snmp-server host * and ned-settings cisco-ios api new-snmp-server-host true
            //
            else if (newSnmpServerHost
                     && toptag.startsWith("snmp-server host ")
                     && (match = getMatch(trimmed, "snmp-server host \\S+ (\\S+)")) != null
                     && (!"informs".equals(match) && !"traps".equals(match))) {
                input = line.replace(match, "traps " + match);
            }

            //
            // string-quote strings
            //
            else if (toptag.startsWith("snmp-server ")
                     && (match = getMatch(trimmed, "snmp-server (?:contact|location) (.+)")) != null) {
                input = line.replace(match, stringQuote(match));
            } else if (toptag.startsWith("alias ")
                       && (match = getMatch(trimmed, "alias \\S+ \\S+ (.*)")) != null) {
                input = line.replace(match, stringQuote(match));
            } else if (toptag.startsWith("crypto isakmp key ")
                       && (match = getMatch(trimmed, "crypto isakmp key (\\S+) "
                                            +"(?:address|hostname|address ipv6) \\S+")) != null) {
                input = line.replace(match, stringQuote(match));
            } else if (toptag.startsWith("event manager applet ") && trimmed.startsWith("action ")
                       && (match = getMatch(trimmed, "action \\d+ regexp (.*)")) != null) {
                input = line.replace(match, stringQuote(match));
            } else if (toptag.startsWith("crypto pki profile enrollment ")
                       && trimmed.startsWith("authentication command ")
                       && (match = getMatch(trimmed, "authentication command (.*)")) != null) {
                input = line.replace(match, stringQuote(match));
            } else if (toptag.startsWith("utd ")
                       && (match = getMatch(trimmed, "signature id \\d+ comment (.*)")) != null) {
                input = line.replace(match, stringQuote(match));
            } else if (toptag.startsWith("utd ")
                       && (match = getMatch(trimmed, "^(?:content )?text (.*)")) != null) {
                input = line.replace(match, stringQuote(match));
            }
            else if (toptag.startsWith("chat-script ") && trimmed.startsWith("chat-script ")
                     && (match = getMatch(trimmed, "chat-script \\S+ (.+)")) != null) {
                input = line.replace(match, stringQuote(match));
            }
            else if (toptag.startsWith("kron policy-list ") && trimmed.startsWith("cli ")
                     && (match = getMatch(trimmed, "cli (.+)")) != null) {
                input = line.replace(match, stringQuote(match));
            }
            else if (toptag.startsWith("crypto pki trustpoint ") && trimmed.startsWith("subject-name ")
                     && (match = getMatch(trimmed, "subject-name (.+)")) != null) {
                input = line.replace(match, stringQuote(match));
            }

            //
            // password-quote strings
            //
            else if (toptag.startsWith("voice translation-rule ") && trimmed.startsWith("rule ")
                     && (group = getMatches(trimmed, "rule (\\d+) ([/].*[/]) ([/].*[/])")) != null
                     && Integer.parseInt(group[0]) == 3) {
                // voice translation-rule * / rule
                input = " rule "+group[1]+" "+passwordQuote(group[2])+" "+passwordQuote(group[3]);
            }

            //
            // quote password
            //
            else if (trimmed.startsWith("crypto isakmp key 6 ")
                     && (match = getMatch(trimmed, "crypto isakmp key 6 (\\S+) "
                                          +"(?:address|hostname|address ipv6) \\S+")) != null) {
                // crypto isakmp key 6
                input = line.replace(match, passwordQuote(match));

            } else if (trimmed.startsWith("authentication-key 6 ")
                       && (match = getMatch(trimmed, "authentication-key 6 (\\S+)")) != null) {
                // router lisp * / authentication-key 6
                input = line.replace(match, passwordQuote(match));

            } else if (trimmed.startsWith("ipv4 etr map-server ")
                       && (match = getMatch(trimmed, "ipv4 etr map-server \\S+ key 6 (\\S+)")) != null) {
                // router lisp * / ipv4 etr map-server
                input = line.replace(match, passwordQuote(match));

            } else if (toptag.startsWith("crypto ") && trimmed.startsWith("pre-shared-key ")
                       && (match = getMatch(trimmed, "pre-shared-key(?: local| remote)? 6 (\\S+)")) != null) {
                // crypto ikev2 keyring / peer * / pre-shared-key
                input = line.replace(match, passwordQuote(match));

            } else if (toptag.startsWith("crypto ") && trimmed.startsWith("aaa authorization group ")
                       && (match = getMatch(trimmed,"aaa authorization group (?:psk|eap) list \\S+ password 6 (\\S+)"))
                       != null) {
                // crypto ikev2 profile * / aaa authorization group
                input = line.replace(match, passwordQuote(match));

            } else if (toptag.startsWith("crypto ") && trimmed.startsWith("authentication ")
                       && (match = getMatch(trimmed, "authentication (?:local|remote) pre-share key 6 (\\S+)"))
                       != null) {
                // crypto ikev2 profile * / authentication
                input = line.replace(match, passwordQuote(match));

            } else if (toptag.startsWith("crypto isakmp client configuration group")
                       && (match = getMatch(trimmed, "\\s+key 6 (\\S+)")) != null) {
                // crypto isakmp client configuration group * /
                input = line.replace(match, passwordQuote(match));

            } else if (toptag.startsWith("crypto keyring ") && trimmed.startsWith("pre-shared-key address ")
                       && (match = getMatch(trimmed, "pre-shared-key address \\S+(?: \\S+)? key 6 (\\S+)")) != null) {
                // crypto keyring * / pre-shared-key address
                input = line.replace(match, passwordQuote(match));
            }
            else if (toptag.startsWith("router bgp ")
                     && (match = getMatch(trimmed, "neighbor \\S+ password(?: [0-7])? (.*)")) != null) {
                // router bgp * / neighbor * password *
                input = line.replace(match, passwordQuote(match));
            }

            //
            // transform single lines
            //
            else if (trimmed.startsWith("ip domain-name")) {
                input = line.replace("ip domain-name", "ip domain name");
            } else if (trimmed.startsWith("ip domain-list")) {
                input = line.replace("ip domain-list", "ip domain list");
            } else if (trimmed.startsWith("no ip domain-lookup")) {
                input = line.replace("no ip domain-lookup", "no ip domain lookup");
            } else if ("line con 0".equals(trimmed)) {
                input = "line console 0";
            } else if (trimmed.startsWith("aaa authorization ")) {
                input = line.replaceAll("aaa authorization (.*)local if-authenticated",
                                        "aaa authorization $1if-authenticated local");
            }

            //
            // transform no-list lists/leaves
            //
            if (trimmed.startsWith("no ip forward-protocol udp ")) {
                input = line.replaceAll("no ip forward-protocol udp (\\S+)",
                                        "ip forward-protocol udp $1 disabled");
            } else if (trimmed.startsWith("no cable cm-status enable ")) {
                input = line.replace("no cable cm-status enable ",
                                     "cable cm-status enable no-list ");
            } else if (trimmed.startsWith("no passive-interface ")) {
                input = line.replace("no passive-interface ",
                                     "disable passive-interface ");
            } else if (trimmed.startsWith("no network-clock-participate wic ")) {
                input = line.replace("no network-clock-participate wic ",
                                     "network-clock-participate wic wic-disabled ");
            } else if (trimmed.startsWith("no wrr-queue random-detect ")) {
                input = line.replace("no wrr-queue random-detect ",
                                     "no-list wrr-queue random-detect ");
            } else if (trimmed.startsWith("no rcv-queue random-detect ")) {
                input = line.replace("no rcv-queue random-detect ",
                                     "no-list rcv-queue random-detect ");
            } else if (trimmed.startsWith("no spanning-tree vlan ")) {
                input = line.replace("no spanning-tree vlan ",
                                     "spanning-tree vlan no-list ");
            } else if (trimmed.startsWith("no mac-address-table learning vlan ")) {
                input = line.replace("no mac-address-table learning vlan ",
                                     "mac-address-table learning vlan no-list ");
            } else if (trimmed.startsWith("no ip igmp snooping vlan ")) {
                input = line.replace("no ip igmp snooping vlan ",
                                     "ip igmp snooping vlan no-list ");
            } else if (trimmed.startsWith("no ip next-hop-self eigrp ")) {
                input = line.replace("no ip next-hop-self eigrp ",
                                     "ip next-hop-self eigrp no-list ");
            } else if (trimmed.startsWith("no ip split-horizon eigrp ")) {
                input = line.replace("no ip split-horizon eigrp ",
                                     "ip split-horizon eigrp no-list ");
            } else if (toptag.startsWith("parameter-map type ") && trimmed.startsWith("no application-inspect ")) {
                input = line.replace("no application-inspect ",
                                     "application-inspect no-list ");
            } else if (trimmed.startsWith("no hw-module module ") && trimmed.endsWith(" logging onboard")) {
                input = line.replace("no hw-module module ", "hw-module module no-list ");
            }

            //
            // transform no-enable leaves
            //
            else if (trimmed.startsWith("no cts server test ") && trimmed.endsWith(" enable")) {
                input = line.replace("no ", "").replace(" enable", " no-enable");
            }

            //
            // strip single lines
            //
            else if ("boot-start-marker".equals(trimmed) || "boot-end-marker".equals(trimmed)) {
                lines[n] = ""; // silent
            } else if (trimmed.startsWith("radius-server source-ports ")) {
                input = "";
            } else if ("no logging console".equals(trimmed)) {
                input = ""; // 'show logging xml' adds 'logging console <level>'
            } else if (trimmed.startsWith("license udi")) {
                input = ""; // not config
            } else if (trimmed.startsWith("! Incomplete")) {
                input = ""; // comments
            } else if ("ip msdp cache-sa-state".equals(toptag)) {
                input = ""; // config? (can't be disabled)
            } else if ("cpp system-default".equals(toptag)) {
                input = ""; //config? (can't be disabled)
            }

            //
            // cable profile ssd * / ssd
            //
            else if (toptag.startsWith("cable profile ssd ") && trimmed.startsWith("ssd ")) {
                input = line.replace(" HTTP ", " http ").replace(" TFTP ", " tftp ");
            }

            //
            // Convert space to comma for range-list-syntax leaf-list's
            //
            // cable profile service-group * / load-balance docsis-group * profile * / downstream sg-channel *
            else if (toptag.startsWith("cable profile service-group ") && line.startsWith("  downstream sg-channel ")
                     && (match = getMatch(line, " sg-channel ([0-9 -]+)$")) != null) {
                input = line.replace(match, match.replace(" ", ","));
            }
            if (input == null) {
                String[] spaceToComma = {
                    // Fix cable rf-channels channel-list x-y z bandwidth-percent
                    // Fix cable rf-channels controller ? channel-list x-y z bandwidth-percent
                    " channel-list (.+) bandwidth-percent",
                    " downstream sg-channel (.+) profile \\S+",

                    " downstream sg-channel (.+) rf-bandwidth-percent \\d+",
                    " downstream sg-channel .+ profile \\S+ upstream (.+)"
                };
                for (int j = 0; j < spaceToComma.length; j++) {
                    Pattern p = Pattern.compile(spaceToComma[j]);
                    Matcher m = p.matcher(line);
                    if (m.find()) {
                        if (input == null) {
                            input = line;
                        }
                        String replacement = m.group(1).replace(" ", ","); // type leaf-list
                        if (j >= 2) {
                            replacement = "\"" + m.group(1) + "\""; // type string
                        }
                        input = input.substring(0,m.start(1))+replacement+input.substring(m.end(1));
                    }
                }
            }

            //
            // Transform lines[n] -> XXX
            //
            if (input != null && !input.equals(lines[n])) {
                if (input.isEmpty()) {
                    if (!silent) {
                        traceVerbose(worker, "transformed <= stripped '"+trimmed+"'");
                    }
                    continue;
                }
                if (!silent) {
                    traceVerbose(worker, "transformed <= '"+trimmed+"' to '"+input.trim()+"'");
                }
                sbin.append(input+"\n");
            } else if (lines[n] != null && !lines[n].isEmpty()) {
                sbin.append(lines[n]+"\n");
            }

        } // for loop
        res = sbin.toString();


        //
        // Update secrets - replace (unchanged) encrypted secrets with cleartext
        //
        traceInfo(worker, "in-transforming - updating secrets");
        res = secrets.update(worker, res, false);


        //
        // APPEND TRANSFORMATIONS (may add, delete or reorder lines)
        //
        traceInfo(worker, "in-transforming - appending config");
        lines = res.split("\n");
        sbin = new StringBuilder();
        for (int n = 0; n < lines.length; n++) {
            String line = lines[n];
            String trimmed = line.trim();
            if (trimmed.isEmpty()) {
                continue;
            }
            String nexttrim = (n + 1 < lines.length) ? lines[n+1].trim() : "";
            boolean split = false;

            // Update toptag
            if (isTopExit(line)) {
                toptag = "";
            } else if (Character.isLetter(line.charAt(0))) {
                toptag = trimmed; // Strip '\r'
            }

            // autoInterfaceSwitchportStatus = true
            if (autoInterfaceSwitchportStatus && line.startsWith("interface ")) {
                sbin.append(line+"\n");
                if (trimmed.contains("Ethernet") || trimmed.contains("Port-channel")) {
                    res = print_line_exec(worker, "show " + trimmed + " switchport | i Switchport");
                    if (res.contains("Switchport: Enabled")) {
                        sbin.append(" switchport\n");
                    } else if (res.contains("Switchport: Disabled")) {
                        sbin.append(" no switchport\n");
                    }
                }
                continue;
            }

            //
            // shutdown vlan *
            //
            else if (line.startsWith("shutdown vlan ") && (match = getMatch(line, "vlan (\\d+)")) != null) {
                sbin.append("vlan "+match+"\n shutdown\nexit\n");
                traceVerbose(worker, "transformed <= shutdown vlan "+match+" to vlan "+match+" / shutdown");
                continue;
            }

            //
            // interface * / switchport trunk allowed vlan
            //
            else if (toptag.startsWith("interface ")
                     && trimmed.startsWith("switchport trunk allowed vlan ")
                     && nexttrim.startsWith("switchport trunk allowed vlan add ")) {
                traceVerbose(worker, "transformed <= joined '"+toptag+"' switchport trunk allowed vlan entries");
                sbin.append(" " + trimmed);
                for (n = n + 1; n < lines.length; n++) {
                    trimmed = lines[n].trim();
                    if ((match = getMatch(trimmed, "switchport trunk allowed vlan add (.*)")) == null) {
                        break;
                    }
                    sbin.append("," + match);
                }
                sbin.append("\n");
                // fall through to add break line
            }

            //
            // interface * / ipv6 nd inspection vlan
            //
            else if (toptag.startsWith("interface ")
                     && trimmed.startsWith("ipv6 nd inspection vlan ")
                     && nexttrim.startsWith("ipv6 nd inspection vlan add ")) {
                traceVerbose(worker, "transformed <= joined '"+toptag+"' ipv6 nd inspection vlan entries");
                sbin.append(" " + trimmed);
                for (n = n + 1; n < lines.length; n++) {
                    trimmed = lines[n].trim();
                    if ((match = getMatch(trimmed, "ipv6 nd inspection vlan add (.*)")) == null) {
                        break;
                    }
                    sbin.append("," + match);
                }
                sbin.append("\n");
                // fall through to add break line
            }

            //
            // mac-address-table learning vlan no-list
            //
            else if (line.startsWith("mac-address-table learning vlan no-list ")
                     && (line.contains(",") || getMatch(line, "(\\d+[-]\\d+)") != null)) {
                int num = 0;
                String[] ranges = trimmed.substring(40).trim().split(",");
                for (int r = 0; r < ranges.length; r++) {
                    Pattern p = Pattern.compile("(\\d+)(?:[-](\\d+))?");
                    Matcher m = p.matcher(ranges[r]);
                    if (!m.find()) {
                        continue;
                    }
                    int start = Integer.parseInt(m.group(1));
                    int end = m.group(2) != null ? Integer.parseInt(m.group(2)) : start;
                    for (int v = start; v <= end; v++) {
                        sbin.append("mac-address-table learning vlan no-list "+v+"\n");
                        num++;
                    }
                }
                traceVerbose(worker, "transformed <= split '"+trimmed+"' in "+num);
                continue;
            }

            //
            // interface * / vlan-range *
            //
            else if (toptag.startsWith("interface ")
                     && line.startsWith(" vlan-range ")
                     && (line.contains("-") || line.contains(","))) {

                StringBuffer content = new StringBuffer();
                for (n = n + 1; n < lines.length; n++) {
                    if (!lines[n].startsWith("  ")) {
                        n = n - 1;
                        break;
                    }
                    content.append(lines[n]+"\n");
                }

                int num = 0;
                String[] ranges = trimmed.substring(11).trim().split(",");
                for (int r = 0; r < ranges.length; r++) {
                    Pattern p = Pattern.compile("(\\d+)(?:[-](\\d+))?");
                    Matcher m = p.matcher(ranges[r]);
                    if (!m.find()) {
                        continue;
                    }
                    int start = Integer.parseInt(m.group(1));
                    int end = m.group(2) != null ? Integer.parseInt(m.group(2)) : start;
                    for (int v = start; v <= end; v++) {
                        sbin.append(" vlan-range "+v+"\n");
                        sbin.append(content);
                        num++;
                    }
                }
                traceVerbose(worker, "transformed <= split '"+trimmed+"' in "+num);
                continue;
            }

            //
            // route-map * / set extcommunity rt
            //
            else if (toptag.startsWith("route-map ")
                     && trimmed.matches("^set extcommunity rt [0-9: ]+ additive$")) {
                // Join 'set extcommunity rt ... additive' lines, split by device (e.g. asr1k)
                traceVerbose(worker, "transformed <= joined '"+toptag+"' set extcommunity rt additive entries");
                sbin.append(" set extcommunity rt");
                for (; n < lines.length; n++) {
                    trimmed = lines[n].trim();
                    if ((match = getMatch(trimmed, "set extcommunity rt( [0-9: ]+) additive$")) == null) {
                        break;
                    }
                    sbin.append(match);
                }
                sbin.append(" additive\n");
                continue;
            }

            //
            // route-map * / match interface
            //
            else if (toptag.startsWith("route-map ")
                     && trimmed.startsWith("match interface ")
                     && nexttrim.startsWith("match interface ")) {
                // Join lines
                traceVerbose(worker, "transformed <= joined '"+toptag+"' match interface entries");
                sbin.append(" "+trimmed);
                for (n = n + 1; n < lines.length; n++) {
                    trimmed = lines[n].trim();
                    if ((match = getMatch(trimmed, "match interface( .+)")) == null) {
                        break;
                    }
                    sbin.append(match);
                }
                sbin.append("\n");
                continue;
            }

            //
            // aaa accounting
            //
            else if (line.startsWith("aaa accounting ") && nexttrim.startsWith("action-type")) {
                traceVerbose(worker, "transformed <= compacted '"+trimmed+"'");
                // action-type
                line = trimmed + nexttrim.replace("action-type", "");
                nexttrim = (++n + 1 < lines.length) ? lines[n+1].trim() : "";
                // optional broadcast
                if ("broadcast".equals(nexttrim)) {
                    line += " broadcast";
                    nexttrim = (++n + 1 < lines.length) ? lines[n+1].trim() : "";
                }
                // optional group
                if (nexttrim.startsWith("group ")) {
                    line += (" " + nexttrim);
                    n++;
                }
                sbin.append(line + "\n");
                continue;
            }

            //
            // call-home * / profile * / [no ]active
            // call-home * / profile * / [no ]reporting smart-call-home-data
            //
            else if (toptag.startsWith("call-home") && line.startsWith(" profile ")) {
                sbin.append(line+"\n");
                String callprof = print_line_exec(worker, "show call-home "+trimmed);
                if (!isExecError(callprof)) {
                    if (callprof.contains("Profile status: ACTIVE")) {
                        sbin.append("  active\n");
                    } else {
                        sbin.append("  no active\n");
                    }
                    if (callprof.contains("Smart Licensing")) {
                        sbin.append("  reporting smart-licensing-data\n");
                    } else {
                        sbin.append("  no reporting smart-licensing-data\n");
                    }
                }
                continue;
            }

            //
            // monitor session * source vlan *
            //
            else if (toptag.startsWith("monitor session ")
                     && trimmed.contains(" source vlan ")
                     && (group = getMatches(trimmed, "((?:monitor session \\d+)? source vlan )(\\S+)( \\S+)?"))
                     != null) {
                String suffix = group[3] != null ? group[3] : "";
                String[] vlans = group[2].split(",");
                for (i = 0; i < vlans.length; i++) {
                    String[] entry;
                    if ((entry = getMatches(vlans[i], "(\\d+)-(\\d+)")) != null) {
                        split = true;
                        int start = Integer.parseInt(entry[1]);
                        int end = Integer.parseInt(entry[2]);
                        for (int j = start; j <= end; j++) {
                            sbin.append(group[1] + j + suffix + "\n");
                        }
                    } else {
                        sbin.append(group[1] + vlans[i] + suffix + "\n");
                    }
                }
                if (split || vlans.length > 1) {
                    traceVerbose(worker, "transformed <= split '"+trimmed+"'");
                }
                continue;
            }

            //
            // monitor session * source|destination interface *
            // monitor session * type local / source|destination interface *
            // monitor session * type [e]rspan-source / source interface *
            //
            else if (toptag.startsWith("monitor session ")
                     && (group = getMatches(line, "((?:monitor session \\d+)? (?:source|destination) interface) (.*)"))
                     != null) {
                String suffix;
                String interfaceString = group[2];
                if ((suffix = getMatch(interfaceString, "( rx| tx| both|(?: encapsulation .*)|(?: ingress vlan .*))"))
                    != null) {
                    interfaceString = interfaceString.replace(suffix, "");
                } else {
                    suffix = "";
                }
                String[] interfaces = interfaceString.split(" , ");
                for (i = 0; i < interfaces.length; i++) {
                    String[] entry;
                    if ((entry = getMatches(interfaces[i].trim(), "(\\S+)/(\\d+) - (\\d+)")) != null) {
                        split = true;
                        int start = Integer.parseInt(entry[2]);
                        int end = Integer.parseInt(entry[3]);
                        for (int j = start; j <= end; j++) {
                            sbin.append(group[1] + " " + entry[1] + "/" + j + suffix + "\n");
                        }
                    } else {
                        sbin.append(group[1] + " " + interfaces[i].trim() + suffix + "\n");
                    }
                }
                if (split || interfaces.length > 1) {
                    traceVerbose(worker, "transformed <= split '"+trimmed+"'");
                }
                continue;
            }

            // monitor session * filter address-type
            else if (toptag.startsWith("monitor session ") && trimmed.contains("filter address-type")
                     && !trimmed.endsWith(" rx") && !trimmed.endsWith(" tx")) {
                sbin.append(trimmed+" rx\n");
                sbin.append(trimmed+" tx\n");
                continue;
            }

            //
            // qos map dscp policed * to dscp
            // qos map dscp * to tx-queue
            // qos map dscp * to cos
            // qos map cos * to dscp
            //
            else if (trimmed.startsWith("qos map dscp policed ")) {
                String[] tokens = trimmed.split(" +");
                split = appendLines(sbin, tokens, 4, 3, 1);
            }
            else if (trimmed.startsWith("qos map ") && trimmed.contains(" to ")) {
                String[] tokens = trimmed.split(" +");
                split = appendLines(sbin, tokens, 3, 3, 1);
            }

            //
            // mls qos map policed-dscp *
            //
            else if (trimmed.startsWith("mls qos map policed-dscp ") && trimmed.contains(" to ")) {
                String[] tokens = trimmed.split(" +");
                split = appendLines(sbin, tokens, 4, 2, 1);
            }

            //
            // ip name-server [vrf <vrf>] <address 1> .. [address N]
            //
            else if (trimmed.startsWith("ip name-server ")) {
                String[] tokens = trimmed.split(" +");
                if ("vrf".equals(tokens[2])) {
                    split = appendLines(sbin, tokens, 4, 0, 1);
                } else {
                    split = appendLines(sbin, tokens, 2, 0, 1);
                }
            }

            //
            // router isis * / purge-transmit strict
            //
            else if (toptag.startsWith("router isis") && "purge-transmit strict".equals(trimmed)) {
                sbin.append(" purge-transmit strict level-1\n");
                sbin.append(" purge-transmit strict level-2\n");
                continue;
            }

            //
            // router ospf * / discard-route
            //
            else if (toptag.startsWith("router ospf ") && "no discard-route".equals(trimmed)) {
                sbin.append(" discard-route external disabled\n");
                sbin.append(" discard-route internal disabled\n");
                continue;
            } else if (toptag.startsWith("router ospf ") && "no discard-route external".equals(trimmed)) {
                sbin.append(" discard-route external disabled\n");
                continue;
            } else if (toptag.startsWith("router ospf ") && "no discard-route internal".equals(trimmed)) {
                sbin.append(" discard-route internal disabled\n");
                continue;
            }

            //
            // table-map *
            //
            else if (toptag.startsWith("table-map ") && trimmed.startsWith("map from ")) {
                String[] tokens = trimmed.split(" +");
                split = appendLines(sbin, tokens, 2, 2, 1);
            }

            //
            // line * / exec-timeout 10 0
            //
            else if (toptag.startsWith("line ") && line.startsWith("line ")) {
                traceVerbose(worker, "transformed <= injected: 'exec-timeout 10 0' first in "+trimmed);
                sbin.append(line+"\n"+" exec-timeout 10 0\n");
                continue;
            }

            //
            // policy-map * / class * / random-detect ? values
            //
            else if (toptag.startsWith("policy-map ")
                     && line.startsWith("  random-detect") && line.contains(" values ")) {
                Pattern p = Pattern.compile("(  random-detect \\S+ values )(\\d+ .?\\d+)( minimum-thresh .+)");
                Matcher m = p.matcher(line);
                if (m.find()) {
                    traceVerbose(worker, "transformed <= split '"+m.group(0)+"'");
                    String[] values = m.group(2).split("[ ]+");
                    for (int v = 0; v < values.length; v++) {
                        sbin.append(m.group(1)+values[v]+m.group(3)+"\n");
                    }
                    continue;
                }
            }

            //
            // Log or add if not split
            //
            if (split) {
                traceVerbose(worker, "transformed <= split '"+trimmed+"'");
            } else {
                sbin.append(lines[n]+"\n");
            }
        }
        res = sbin.toString();


        //
        // SINGLE BUFFER TRANSFORMATIONS:
        //

        //
        // Split line ranges into multiple single lines with config, e.g. line 0/2/15 0/3/0 or line vty 2 7
        //
        res = modifyInputExpandLine(worker, res);
        if (expandedLineVtyFormat) {
            res = modifyInputExpandLineVty(worker, res);
        }

        //
        // DONE
        //
        logInfo(worker, "DONE in-transforming "+tickToString(start0));
        if (syncFile != null || offlineData != null) {
            traceVerbose(worker, "\nSHOW_AFTER_FILE:\n"+res);
        } else {
            traceVerbose(worker, "\nSHOW_AFTER:\n"+res);
        }

        // Respond with updated show buffer
        return res;
    }


    /**
     * Expand abbrevitated interface name
     * @param
     * @return Full interface name
     */
    private String expandInterfaceName(String abbrevName) {
        String[][] ifNameMap = {
            { "Gi", "GigabitEthernet" },
            { "Fa", "FastEthernet" },
            { "Et", "Ethernet" },
            { "Te", "TenGigabitEthernet" },
            { "Po", "Port-Channel" },
            { "Vl", "Vlan" }
        };
        for (int i = 0; i < ifNameMap.length; i++) {
            if (abbrevName.equals(ifNameMap[i][0])) {
                return ifNameMap[i][1];
            }
        }
        return abbrevName;
    }


    /**
     * Quote description string
     * @param
     * @return Quoted description
     */
    private String quoteDescription(String toptag, String line) {

        // Ignore quoting the following service-insertion descriptions
        if (toptag.startsWith("service-insertion ")) {
            return line;
        }

        int i = line.indexOf(" description ");

        // Special case for: ip msdp description <hostname> <description>
        int offset = 13;
        if (line.trim().startsWith("ip msdp description ")) {
            int space = line.indexOf(' ', i + offset);
            if (space > 0) {
                offset = space - i + 1;
            }
        }

        // Quote description string
        String desc = stringQuote(line.substring(i+offset).trim());
        return line.substring(0,i+offset) + desc;
    }


    /**
     *
     * @param x = max values per line
     * @return
     */
    private boolean appendLines(StringBuilder sb, String[] tokens, int start, int end, int x) {
        if (tokens.length - start <= x) {
            return false;
        }

        StringBuffer prefix = new StringBuffer(tokens[0]);
        for (int n = 1; n < start; n++) {
            prefix.append(" " + tokens[n]);
        }

        final int length = tokens.length - end;
        StringBuffer postfix = new StringBuffer();
        for (int n = length; n < tokens.length; n++) {
            postfix.append(" " + tokens[n]);
        }

        for (int n = start; n < length; n = n + x) {
            StringBuffer values = new StringBuffer();
            for (int j = n; (j < n + x) && (j < length); j++) {
                values.append(" " + tokens[j]);
            }
            sb.append(prefix.toString() + values.toString() + postfix.toString() + "\n");
        }
        return true;
    }


    /**
     * Split line ranges into multiple single lines with config, e.g. line 0/2/15 0/3/0
     * @param
     * @return
     * @throws Exception
     */
    private String modifyInputExpandLine(NedWorker worker, String res) throws Exception {

        Pattern p = Pattern.compile("\n(line (\\d+)/(\\d+)/(\\d+) "
                                    +"\\2/(\\d+)/(\\d+))\r(.*?)?(?=\nline |\n!)", Pattern.DOTALL);
        Matcher m = p.matcher(res);
        StringBuffer sb = new StringBuffer();
        boolean logonce = true;
        String match;
        while (m.find()) {
            if (logonce) {
                traceInfo(worker, "in-transforming - splitting range terminal lines");
                logonce = false;
            }
            String slot = m.group(2);
            int subslotStart = Integer.parseInt(m.group(3));
            int portStart = Integer.parseInt(m.group(4));
            int subslotEnd = Integer.parseInt(m.group(5));
            int portEnd = Integer.parseInt(m.group(6));
            String config = "";
            if (m.groupCount() == 7) {
                config = m.group(7);
            }
            String buf = "";
            int num = 0;
            if (subslotStart == subslotEnd) {
                // Single subslot, portStart and portEnd on same line
                for (int port = portStart; port <= portEnd; port++, num++) {
                    buf += "\nline "+slot+"/"+subslotStart+"/"+port+"\r"+config;
                }
            }

            else {
                // Range of multiple subslots, need to look up min & max lines
                for (int s = subslotStart; s <= subslotEnd; s++) {
                    String root = slot+"/"+s+"/";
                    String linebuf = print_line_exec(worker, "show line | i "+root);
                    if (!linebuf.contains(root)) {
                        traceInfo(worker, "ERROR: failed to look up line "+root);
                        break;
                    }
                    String[] lines = linebuf.trim().split("\n");

                    // Get start
                    if ((match = getMatch(lines[0], root+"(\\d+) ")) == null) {
                        break;
                    }
                    int start = Integer.parseInt(match);
                    if (s == subslotStart) {
                        start = Math.max(start, portStart);
                    }

                    // Get end
                    if ((match = getMatch(lines[lines.length-1], root+"(\\d+) ")) == null) {
                        break;
                    }
                    int end = Integer.parseInt(match);
                    if (s == subslotEnd) {
                        end = Math.min(end, portEnd);
                    }

                    // Create single line
                    for (int port = start; port <= end; port++, num++) {
                        buf += "\nline "+root+port+"\r"+config;
                    }
                }
            }
            if (num > 0) {
                traceVerbose(worker, "transformed <= split '"+m.group(1)+"' into "+num+" lines");
                m.appendReplacement(sb, buf);
            } else {
                traceInfo(worker, "ERROR: failed to split up terminal line "+stringQuote(m.group(0)));
                m.appendReplacement(sb, m.group(0));
            }
        }
        m.appendTail(sb);
        return sb.toString();
    }


    /**
     * Split line vty ranges into multiple single lines with config, e.g. line vty 2 7
     * @param
     * @return
     * @throws Exception
     */
    private String modifyInputExpandLineVty(NedWorker worker, String res) {
        Pattern p = Pattern.compile("\n(line vty (\\d+)[ ]+(\\d+))\r(.*?)?(?=\nline |\n!)", Pattern.DOTALL);
        Matcher m = p.matcher(res);
        StringBuffer sb = new StringBuffer();
        boolean logonce = true;
        while (m.find()) {
            if (logonce) {
                traceInfo(worker, "in-transforming - splitting line vty range config");
                logonce = false;
            }
            int start = Integer.parseInt(m.group(2));
            int end = Integer.parseInt(m.group(3));
            String config = "";
            if (m.groupCount() == 4) {
                config = m.group(4);
            }

            // Range of vty lines, create single lines
            String buf = "";
            for (int port = start; port <= end; port++) {
                buf += "\nline vty "+port+"\r"+config;
            }

            if (end - start > 0) {
                traceVerbose(worker, "transformed <= split '"+m.group(1)+"' into "+(1+end-start)+" vty lines");
                m.appendReplacement(sb, buf);
            } else {
                traceInfo(worker, "WARNING: failed to split up vty line "+stringQuote(m.group(0)));
                m.appendReplacement(sb, m.group(0));
            }
        }
        m.appendTail(sb);
        return sb.toString();
    }


    /**
     * Inject config in input
     * @param
     * @return
     * @throws Exception
     */
    private String injectInput(NedWorker worker, boolean isShow, int toTh, String res) throws Exception {
        final long start = tick(0);

        logVerbose(worker, "BEGIN in-injecting");

        // Start transaction if none open
        int th = toTh;
        if (th == -1) {
            setUserSession(worker);
            th = maapi.startTrans(Conf.DB_RUNNING, Conf.MODE_READ);
        }

        StringBuilder first = new StringBuilder();
        StringBuilder last = new StringBuilder();

        //
        // tailfned api access-list-method
        //
        if (isDevice()) {
            if (newIpACL) {
                traceInfo(worker, "transformed <= inserted tailfned api new-ip-access-list");
                first.append("tailfned api new-ip-access-list\n");
            }
            if (resequenceACL) {
                traceInfo(worker, "transformed <= inserted tailfned api resequence-access-list");
                first.append("tailfned api resequence-access-list\n");
            }
            if (newSnmpServerHost) {
                traceInfo(worker, "transformed <= inserted tailfned api new-snmp-server-host");
                first.append("tailfned api new-snmp-server-host\n");
            }
            if (expandedLineVtyFormat) {
                traceInfo(worker, "transformed <= inserted tailfned api expanded-line-vty-format");
                first.append("tailfned api expanded-line-vty-format\n");
            }
        }

        //
        // tailfned police
        //
        String match;
        String police = getIosPolice(worker, th, isShow);
        traceInfo(worker, "transformed <= inserted tailfned police "+police);
        if ((match = getMatch(res, "(tailfned police .*)")) != null) {
            res = res.replace(match, "tailfned police "+police);
        } else {
            first.append("tailfned police "+police+"\n");
        }

        //
        // read/replace-config ned-setting - inject/replace in running-config
        //
        if (!replaceConfig.isEmpty()) {
            traceInfo(worker, "in-transforming - replace-config ned-setting");
            for (int n = 0; n < replaceConfig.size(); n++) {
                String[] entry = replaceConfig.get(n);
                if (entry[3] != null &&
                    ((isShow && "trans-id-only".equals(entry[3])) ||
                     (!isShow && "config-only".equals(entry[3])))) {
                    continue;
                }
                String regexp = entry[1];
                String replacement = entry[2];
                try {
                    Pattern p = Pattern.compile(regexp+"(?:[\r])?", Pattern.DOTALL);
                    Matcher m = p.matcher(res);
                    StringBuffer sb = new StringBuffer();
                    while (m.find()) {
                        traceInfo(worker, "transformed <= replaced "+stringQuote(m.group(0))
                                  +" with " + matcherToString(m, replacement));
                        m.appendReplacement(sb, replacement);
                    }
                    m.appendTail(sb);
                    res = sb.toString();
                } catch (Exception e) {
                    logError(worker, "ERROR in read/replace-config '"+entry[0]+"' regexp="
                             +stringQuote(regexp)+" replacement="+stringQuote(replacement), e);
                }
            }
        }

        //
        // Not isShow early exit
        //
        if (!isShow) {
            logVerbose(worker, "DONE in-injecting (checksum only) "+tickToString(start));
            if (toTh == -1) {
                maapi.finishTrans(th);
            }
            return first.toString() + res + last.toString();
        }

        //
        // Insert cached-show 'config'
        //
        last.append("\n");
        if (includeCachedShowVersion) {
            last.append("cached-show version version " + iosversion + "\n");
            if (!xeversion.isEmpty()) {
                last.append("cached-show version xe-version " + xeversion + "\n");
            }
            last.append("cached-show version model " + iosmodel + "\n");
            if (licenseLevel != null) {
                last.append("cached-show version license level " + licenseLevel + "\n");
            }
            if (licenseType != null) {
                last.append("cached-show version license type " + licenseType + "\n");
            }
        }
        if (includeCachedShowInventory) {
            for (int i = 0; i < cachedShowInventory.size(); i++) {
                String[] entry = cachedShowInventory.get(i);
                last.append("cached-show inventory name " + entry[0]);
                if (!entry[1].trim().isEmpty()) {
                    last.append(" sn " + entry[1]);
                }
                last.append("\n");
            }
        }

        //
        // read/inject-config ned-setting - inject config in running-config
        //
        if (!injectConfig.isEmpty()) {
            traceInfo(worker, "in-transforming - injecting config");
            for (int n = injectConfig.size()-1; n >= 0; n--) {
                String[] entry = injectConfig.get(n);
                if (entry[1] == null) {
                    // no regexp given
                    if (entry[3] != null && entry[3].startsWith("after")) {
                        // inject last
                        traceVerbose(worker, "transformed <= injected: "+stringQuote(entry[2])+" last in config");
                        last.append(entry[2] + "\n");
                    } else {
                        // inject first [default]
                        traceVerbose(worker, "transformed <= injected: "+stringQuote(entry[2])+" first in config");
                        first.append(entry[2] + "\n");
                    }
                } else {
                    // regexp inject (default to 'after-each')
                    if (entry[3] == null) {
                        entry[3] = "after-each";
                    }
                    res = injectData(worker, res, entry, "<=");
                }
            }
        }

        //
        // Insert inject interface config first in matching interface(s)
        //
        if (!interfaceConfig.isEmpty()) {
            traceInfo(worker, "in-transforming - injecting interface config");
            Pattern p = Pattern.compile("\ninterface (\\S+)(?: \\S+)?");
            Matcher m = p.matcher(res);
            StringBuffer sb = new StringBuffer();
            while (m.find()) {
                String ifname = m.group(1);
                String inject = "";
                for (int n = 0; n < interfaceConfig.size(); n++) {
                    String[] entry = interfaceConfig.get(n);
                    if (findString(entry[0], ifname) >= 0) {
                        inject += ("\r\n " + entry[1]);
                    }
                }
                if (!inject.isEmpty()) {
                    traceVerbose(worker, "transformed <= injected: "+stringQuote(inject)
                                 + " first in interface "+ifname);
                }
                m.appendReplacement(sb, m.group(0) + inject);
            }
            m.appendTail(sb);
            res = sb.toString();
        }

        //
        // DEFAULTS - inject hidden defaults values
        //
        if (isDevice() && session != null) {
            traceInfo(worker, "in-transforming - injecting default values");
            res = defaults.inject(worker, res, th);
        }

        //
        // Inject with CDB lookups
        //
        try {
            if (isDevice()) {
                // Insert missing 'snmp-server ... v3 ...' config from show snmp user
                // WARNING: Can't inject from getTransId() with commit queues.
                last.append(injectSnmpUser(worker, res, th));

                // Inject from CDB:
                //  key config-key password-encrypt
                //  cts credentials id * password
                injectCachedExec(worker, first, th);
            }
        } finally {
            if (toTh == -1) {
                maapi.finishTrans(th);
            }
        }

        // Return config
        logVerbose(worker, "DONE in-injecting "+tickToString(start));
        return first.toString() + res + last.toString();
    }


    /**
     * Inject config in CDB not shown on device in show run to avoid diff
     * @param
     */
    private void injectCachedExec(NedWorker worker, StringBuilder first, int th) {

        //
        // key config-key password-encrypt
        //
        ConfValue val;
        try {
            val = maapi.safeGetElem(th, confRoot + "key/config-key/password-encrypt");
            if (val != null) {
                String password = val.toString();
                traceInfo(worker, "SECRETS - transformed <= injected 'key config-key password-encrypt "+password+"'");
                first.append("key config-key password-encrypt " + password + "\n");
            }
        } catch (Exception ignore) {
            // Ignore Exception
        }

        //
        //  cts credentials id * password
        //
        try {
            val = maapi.safeGetElem(th, new ConfPath(confRoot + "cts/credentials/id"));
            if (val != null) {
                String id = val.toString();
                val = maapi.safeGetElem(th, new ConfPath(confRoot + "cts/credentials/password"));
                if (val != null) {
                    String password = val.toString();
                    traceInfo(worker, "transformed <= injected 'cts credentials id "+id+" password <HIDDEN>'");
                    first.append("cts credentials id "+id+" password "+password + "\n");
                }
            }
        } catch (Exception ignore) {
            // Ignore Exception
        }
    }


    /**
     * Inject SNMP user config using data from both 'show snmp user' and CDB
     * @param
     * @return
     * @throws Exception
     */
    private String injectSnmpUser(NedWorker worker, String dump, int th) throws Exception {

        if (!haveShowSnmpUser || syncFile != null || offlineData != null || session == null) {
            return "";
        }

        //
        // Get snmp user info
        //
        traceInfo(worker, "reading config - show snmp user");
        String res = print_line_exec(worker, "show snmp user");
        if (isExecError(res)) {
            traceInfo(worker, "Disabling 'show snmp user' check");
            haveShowSnmpUser = false;
            return "";
        }
        if (res.contains("SNMP agent not enabled")) {
            return "";
        }

        //
        // Parse output and inject passwords from CDB
        //
        StringBuilder result = new StringBuilder("\n");
        try {
            int b = res.indexOf("\nUser name: ");
            if (b < 0) {
                return "";
            }

            while (b >= 0) {

                // User name:
                String name = getString(res, b+12);
                String root = confRoot + "snmp-server/user{"+name+"}/";

                // Engine ID:
                int e = res.indexOf("\nEngine ID: ", b);
                if (e < 0) {
                    break;
                }
                String engineID = getString(res, e+12).toLowerCase().trim();
                traceVerbose(worker, "SNMP-USER name="+name+" engineID="+engineID);

                // Lookup remote
                String remote = "";
                ConfValue val;
                Pattern p = Pattern.compile("snmp-server engineID remote (\\S+)(?: udp-port (\\S+))? "+engineID);
                Matcher m = p.matcher(dump);
                if (m.find()) {
                    remote = " remote "+m.group(1);
                    root = confRoot + "snmp-server/user-remote/user{"+name+" "+m.group(1)+"}/";
                    traceVerbose(worker, "SNMP-USER remote="+m.group(1)+" m.group(2)="+m.group(2));
                    if (m.group(2) != null) {
                        remote += (" udp-port "+m.group(2));
                    } else if ((val = maapi.safeGetElem(th, new ConfPath(root+"udp-port"))) != null) {
                        remote += (" udp-port "+val.toString());
                    }
                }

                // Authentication Protocol:
                e = res.indexOf("\nAuthentication Protocol: ", b);
                if (e < 0) {
                    break;
                }
                final String auth = getString(res, e+26).toLowerCase();

                // Privacy Protocol:
                e = res.indexOf("\nPrivacy Protocol: ", b);
                if (e < 0) {
                    break;
                }
                String priv = getString(res, e+19).toLowerCase().trim();
                if (priv.indexOf("aes") == 0) {
                    priv = "aes " + priv.substring(3);
                }

                // Group-name:
                int end = res.indexOf("\nGroup-name: ", b);
                if (end < 0) {
                    break;
                }
                String group = getString(res, end+13);

                // Get access list info
                String acl = "";
                e = res.indexOf("IPv6 access-list: ", b);
                if (e > 0 && e < end) {
                    acl = "ipv6 " + getString(res, e+18);
                } else {
                    e = res.indexOf("access-list: ", b);
                    if (e > 0 && e < end) {
                        acl = getString(res, e+13);
                    }
                }

                // Begin making entry
                result.append("\nsnmp-server user "+name+" "+group+remote+" v3");

                // Add optional 'auth' params
                if (!"none".equals(auth)) {
                    String authPw = "NOT-SET-IN-NCS";
                    if ((val = maapi.safeGetElem(th, new ConfPath(root+"auth-password"))) != null) {
                        authPw = val.toString();
                    }
                    result.append(" auth " + auth + " " +authPw);
                }

                // Add optional 'priv' params
                if (!"none".equals(priv)) {
                    String privPw = "NOT-SET-IN-NCS";
                    if ((val = maapi.safeGetElem(th, new ConfPath(root+"priv-password"))) != null) {
                        privPw = val.toString();
                    }
                    result.append(" priv " + priv + " " + privPw);
                }

                // Add optional 'access' params
                if (!acl.isEmpty()) {
                    result.append(" access " + acl);
                }

                // Get next entry
                b = res.indexOf("\nUser name: ", b+12);
            }
        } catch (Exception ex) {
            throw new NedException("injectSnmpUser():", ex);
        }

        traceInfo(worker, "transformed <= inserted: "+stringQuote(result.toString())+" from 'show snmp user'");
        return result.toString();
    }


    /*
     **************************************************************************
     * showOffline
     **************************************************************************
     */

    /**
     * Parse and input given config
     * @param
     * @throws Exception
     */
    // @Override
    public void showOffline(NedWorker worker, String toptag, String data) throws Exception {
        try {
            logInfo(worker, "BEGIN SHOW-OFFLINE");
            // Append \r if missing to simulate show run on device
            StringBuilder sb = new StringBuilder();
            String[] lines = data.split("\n");
            for (int n = 0; n < lines.length; n++) {
                if (lines[n].endsWith("\r")) {
                    sb.append(lines[n]+"\n");
                } else {
                    sb.append(lines[n]+"\r\n");
                }
            }
            data = sb.toString();
            this.offlineData = data;
            show(worker, toptag);
            logInfo(worker, "DONE SHOW-OFFLINE");
        } finally {
            this.offlineData = null;
        }
    }


    /*
     **************************************************************************
     * showPartial
     **************************************************************************
     */

    /**
     * Retrieve partial running config from device
     * @param
     * @throws Exception
     */
    @Override
    public void showPartial(NedWorker worker, String[] cmdpaths) throws Exception {
        final long start = tick(0);
        logInfo(worker, "BEGIN SHOW-PARTIAL String[]");
        showPartialInternal(schema, maapi, turboParserEnable, worker, cmdpaths);
        logInfo(worker, "DONE SHOW-PARTIAL "+tickToString(start));
    }


    /**
     * Retrieve partial running config from device
     * @param
     * @throws Exception
     */
    @Override
    public void showPartial(NedWorker worker, ConfPath[] paths) throws Exception {
        final long start = tick(0);
        logInfo(worker, "BEGIN SHOW-PARTIAL ConfPath[]");
        showPartialInternal(schema, maapi, turboParserEnable, worker, paths);
        logInfo(worker, "DONE SHOW-PARTIAL "+tickToString(start));
    }


    /*
     **************************************************************************
     * getDeviceConfiguration
     **************************************************************************
     */

    /**
     * Get device configuration
     * @param
     * @return
     * @throws Exception
     */
    @Override
    protected String getDeviceConfiguration(NedWorker worker) throws Exception {
        String config = getConfig(worker);
        return modifyInput(worker, true, -1, config);
    }


    /*
     **************************************************************************
     * getTransId
     **************************************************************************
     */

    /**
     * Calculate transaction-id
     * @param
     * @throws Exception
     */
    @Override
    public void getTransId(NedWorker worker) throws Exception {
        final long start = tick(0);
        if (trace) {
            session.setTracer(worker);
        }

        final int toTh = worker.getToTransactionId();
        String log = "BEGIN GET-TRANS-ID (th="+toTh+" method="+transIdMethod;
        log += lastTransformedConfig != null ? " T" : "";
        log += lastGetConfig != null ? " G" : "";
        logInfo(worker, log+")");

        // NETSIM, optionally use confd-state transaction id
        String res;
        if (isNetsim() && "confd-state-trans-id".equals(transIdMethod)) {
            res = print_line_exec(worker, "show confd-state internal cdb datastore running transaction-id");
            if (res.contains("error")) {
                throw new NedException("Failed to run get confd running transaction-id");
            }
            res = res.substring(res.indexOf(' ')+1).trim();
            logInfo(worker, "DONE GET-TRANS-ID ("+res+")");
            worker.getTransIdResponse(res);
            return;
        }


        // Use last cached transformed config from applyConfig() secret code
        if (transIdMethod.startsWith("config-hash") && lastTransformedConfig != null) {
            res = lastTransformedConfig;
            lastGetConfig = null;
            lastTransformedConfig = null;
        }

        // config-hash-cached - use last cached config from show() if available
        else if ("config-hash-cached".equals(transIdMethod) && lastGetConfig != null) {
            res = modifyInput(worker, false, -1, lastGetConfig);
            lastGetConfig = null;
        }

        // Use 'Last configuration change' string from running-config
        else if ("last-config-change".equals(transIdMethod) && isDevice()) {
            res = print_line_exec(worker, "show running-config | include Last configuration change");
            if (!res.contains("Last configuration change")) {
                throw new NedException("Failed to get running-config 'Last configuration change' string");
            }
            res = res + res + res + res;
        }

        // Use 'show configuration id' command
        else if ("config-id".equals(transIdMethod) && isDevice()) {
            res = print_line_exec(worker, "show configuration id");
            if (isExecError(res)) {
                throw new NedException("Failed to use 'show configuration id' for transaction id");
            }
            res = res + res + res + res;
        }

        // Use 'show configuration history' command
        else if ("config-history".equals(transIdMethod) && isDevice()) {
            res = print_line_exec(worker, "show configuration history");
            if (isExecError(res)) {
                throw new NedException("Failed to use 'show configuration history' for transaction id");
            }
            res = res + res + res + res;
        }

        // Use running-config for string data
        else {
            String config = getConfig(worker);
            res = modifyInput(worker, false, -1, config);
        }

        // Trim config of dynamic info
        res = stripLineAll(worker, res, "Load for ");
        res = stripLineAll(worker, res, "Time source is NTP");
        res = stripLineAll(worker, res, "No time source");
        res = res.trim();

        // ned-settings cisco-ios read transaction-id-method config-hash-modeled
        if ("config-hash-modeled".equals(transIdMethod)) {
            traceInfo(worker, "Stripping unmodeled config from hash transaction id calculation");
            res = filterConfig(res, schema, maapi, worker, null, false).toString();
        }

        // Sort certain config since some IOS devices reorder entries after reboot
        res = checksumSortConfig(worker, res);

        traceVerbose(worker, "TRANS-ID-BUF=\n+++ begin\n"+res+"\n+++ end");

        // Calculate checksum of running-config
        byte[] bytes = res.getBytes("UTF-8");
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] thedigest = md.digest(bytes);
        BigInteger md5Number = new BigInteger(1, thedigest);
        String md5String = md5Number.toString(16);

        logInfo(worker, "DONE GET-TRANS-ID ("+md5String+") "+tickToString(start));
        worker.getTransIdResponse(md5String);
    }


    /**
     *
     * @param
     * @return
     */
    private String checksumSortConfig(NedWorker worker, String res) {

        //
        // Sort lines:
        //

        // top mode lines
        res = sortLines(worker, res, "ip route vrf ");
        res = sortLines(worker, res, "ipv6 route "); // including "ipv6 route vrf "
        res = sortLines(worker, res, "ip nat translation max-entries vrf ");

        // router bgp * /
        res = sortLines(worker, res, "aggregate-address ");
        res = sortLines(worker, res, "neighbor ");

        // route-map * / match policy-list *
        res = sortLines(worker, res, "match policy-list ");

        //
        // Sort words in lines:
        //
        String toptag = "";
        StringBuilder sb = new StringBuilder();
        String[] lines = res.split("\n");
        for (int n = 0; n < lines.length; n++) {
            String trimmed = lines[n].trim();
            if (trimmed.isEmpty()) {
                continue;
            }
            if (isTopExit(lines[n])) {
                toptag = "";
            } else if (Character.isLetter(lines[n].charAt(0))) {
                toptag = lines[n].trim();
            }

            // route-map * / match interface *
            if (toptag.startsWith("route-map")
                && trimmed.startsWith("match interface ")) {
                String sortedline = sortWords(worker, trimmed, 2);
                sb.append(sortedline+"\n");
            }

            // Default, do not reorder
            else {
                sb.append(lines[n]+"\n");
            }
        }
        res = sb.toString();

        return res;
    }


    /**
     *
     * @param
     * @return
     */
    private String sortLines(NedWorker worker, String res, String sortline) {

        // Sort subsequent lines
        int numSorted = 0;
        StringBuilder sb = new StringBuilder();
        String[] lines = res.split("\n");
        for (int n = 0; n < lines.length; n++) {
            String line = lines[n];
            if (line.trim().isEmpty()) {
                continue;
            }
            if (!line.trim().startsWith(sortline)) {
                sb.append(line+"\n");
                continue;
            }

            // First matching line, assemble all subsequent matching lines
            ArrayList<String> arraylist = new ArrayList<>();
            arraylist.add(line);
            for (int s = n + 1; s < lines.length; s++) {
                if (!lines[s].trim().startsWith(sortline)) {
                    break;
                }
                arraylist.add(lines[s]);
                lines[s] = "";
            }

            // Only one line, continue
            if (arraylist.size() == 1) {
                sb.append(line+"\n");
                continue;
            }

            // Sort lines and add back in place sorted
            numSorted += arraylist.size();
            String[] sortlines = arraylist.toArray(new String[arraylist.size()]);
            Arrays.sort(sortlines);
            if (logVerbose) {
                sb.append("! sort begin\n");
            }
            for (int s = 0; s < sortlines.length; s++) {
                sb.append(sortlines[s]+"\n");
            }
            if (logVerbose) {
                sb.append("! sort end\n");
            }
        }
        if (numSorted < 1) {
            return res;
        }

        traceInfo(worker, "transformed <= sorted "+numSorted+" '"+sortline+"' lines for hash checksum");
        return sb.toString();
    }


    /**
     *
     * @param
     * @return
     */
    private String sortWords(NedWorker worker, String trimmed, int start) {
        String[] lines = trimmed.split(" +");
        Arrays.sort(lines, start, lines.length);

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < lines.length; i++) {
            sb.append(" "+lines[i]);
        }
        String sortedline = sb.toString();

        if (!sortedline.equals(trimmed)) {
            traceInfo(worker, "transformed <= sorted '"+trimmed+"' for hash checksum");
        }
        return sortedline;
    }


    /*
     **************************************************************************
     * prepareDry
     **************************************************************************
     */

    /**
     * Display config for commit dry-run outformat native
     * @param
     * @throws Exception
     */
    @Override
    public void prepareDry(NedWorker worker, String data) throws Exception {
        String originalIosModel = this.iosmodel;
        if (trace && session != null) {
            session.setTracer(worker);
        }
        if (nsoRunningVersion < 0x7000000 && nsoRunningVersion != 0x6060400) {
            traceInfo(worker, "\n"+data);
        }

        // ShowRaw used in debugging, to see cli commands before modification
        final long start = tick(0);
        if (showRaw || data.contains("tailfned raw-run\n")) {
            logInfo(worker, "BEGIN PREPARE-DRY raw");
            showRaw = false;
            logInfo(worker, "DONE PREPARE-DRY "+tickToString(start));
            worker.prepareDryResponse(data);
            return;
        }

        // Init String builder
        StringBuilder sb = new StringBuilder();
        if (devPrepareDryModel != null) {
            // cisco-ios developer prepare-dry-model
            if (logVerbose) {
                sb.append("! Generated for "+devPrepareDryModel+" model\n");
            }
            this.iosmodel = devPrepareDryModel;
        }

        // Log and trace before changes
        String log = "BEGIN PREPARE-DRY model="+iosmodel+" version="+iosversion;
        if (session == null) {
            log += " offline";
        }
        logInfo(worker, log);

        // Modify output
        try {
            data = modifyOutput(worker, data, "PREPARE-DRY");

            // Rebuild data, trimming meta and tailfned if not log-verbose
            String[] lines = data.split("\n");
            for (int i = 0; i < lines.length; i++) {
                if (lines[i].trim().startsWith(META_DATA)
                    || lines[i].startsWith("tailfned ")) {
                    if (logVerbose) {
                        sb.append(lines[i]+"\n");
                    }
                    continue;
                }
                // Modify texts
                String line = modifyTexts(worker, lines[i]);
                sb.append(line+"\n");
            }
            data = sb.toString().trim()+"\n";
        } finally {
            this.iosmodel = originalIosModel;
        }

        logInfo(worker, "DONE PREPARE-DRY "+tickToString(start));
        worker.prepareDryResponse(data);
    }


    /*
     **************************************************************************
     * applyConfig
     **************************************************************************
     *
     * NSO PHASES:
     *          prepare (send data to device)
     *           /   \
     *          v     v
     *       abort | commit (send confirmed commit)
     *               /   \
     *              v     v
     *          revert | persist (send confirming commit)
     */

    /**
     * Apply config
     * @param
     * @throws Exception
     */
    @Override
    public void applyConfig(NedWorker worker, int cmd, String data)
        throws NedException, IOException, SSHSessionException, ApplyException {
        final long start = tick(0);
        if (trace) {
            session.setTracer(worker);
        }
        logInfo(worker, "BEGIN APPLY-CONFIG");

        // Apply the commit
        doApplyConfig(worker, cmd, data);

        // Debug code:
        if (failphase.contains("apply")) {
            traceInfo(worker, "DEBUG: Simulating failed applyConfig");
            failphase = failphase.replace("apply", "");
            doApplyConfig(worker, cmd, "\nhostname applydiff\n");
            throw new NedException("DEBUG :: Exception in applyConfig");
        }

        logInfo(worker, "DONE APPLY-CONFIG "+tickToString(start));
    }


    /**
     *
     * @param
     * @throws Exception
     */
    private void doApplyConfig(NedWorker worker, int cmd, String data)
        throws NedException, IOException, SSHSessionException, ApplyException {

        // Empty data
        if (data.trim().isEmpty()) {
            traceInfo(worker, "Empty data -> skip next writeMemory");
            ignoreNextWrite = true;
            return;
        }

        // Clear cached data
        lastPrompt = "";
        lastGetConfig = null;
        lastTransformedConfig = null;
        ignoreNextWrite = false;
        lastTransactionId = worker.getToTransactionId();

        // Modify data
        if (!data.contains("tailfned raw-run\n")) {
            data = modifyOutput(worker, data, "APPLY-CONFIG");
        }

        // Config trimmed to empty
        if (data.trim().isEmpty()) {
            traceInfo(worker, "Trimmed data -> skip next writeMemory");
            ignoreNextWrite = true;
            return;
        }

        //
        // Send data
        //
        String[] lines = data.split("\n");
        traceInfo(worker, "BEGIN SENDING "+lines.length+" line(s):"+data);
        final long start = nedReportProgress(worker, "sending config...", 0);
        int fromTh = worker.getFromTransactionId();
        int toTh = worker.getToTransactionId();
        try {
            // Attach to CDB
            maapiAttach(worker, fromTh, toTh);

            // Reconnect to device if remote end closed connection due to being idle
            if (session.serverSideClosed() || "reconnect".equals(failphase)) {
                failphase = "";
                reconnectDevice(worker);
            }

            // Enter config mode
            enterConfig(worker);

            // NETSIM - ned-settings cisco-ios write transfer-via-file
            if (isNetsim() && writeTransferViaFile && isLocalIp(this.ip)) {
                transferViaFile(worker, cmd, data);
            }

            // REAL DEVICE or remote NETSIM
            else {
                try {
                    sendConfig(worker, cmd, lines);
                } catch (ApplyException e) {
                    // We may have changed the config-key on device, restore it from old value
                    if (data.contains("\nkey config-key password-encrypt ")) {
                        restoreConfigKey(worker, cmd, lines, fromTh);
                    }
                    throw e;
                }
            }

            // Exit config mode
            exitConfig(worker);

            // All commands accepted by device, cache secrets, defaults and locks
            try {
                if (secrets.apply(worker, data)) {
                    traceVerbose(worker, "SECRETS - new secrets, caching encrypted entries");
                    String config = getConfig(worker);
                    lastTransformedConfig = modifyInput(worker, false, toTh, config);
                }
                if (isDevice()) {
                    defaults.cache(worker, lines);
                }

            } catch (Exception e) {
                throw new NedException(e.getMessage(), e);
            }

            // Done
            traceInfo(worker, "DONE SENDING "+lines.length+" line(s) "+tickToString(start));
            nedReportProgress(worker, "sending config ok", start);

        } catch (Exception e) {
            nedReportProgress(worker, "sending config error", start);
            throw e;

        } finally {
            maapiDetach(worker, fromTh, toTh);
        }
    }


    /**
     * Check if local ip address
     * @param
     * @return True if local ip address, else false
     */
    private boolean isLocalIp(InetAddress addr) {
        if (addr.isAnyLocalAddress() || addr.isLoopbackAddress()) {
            return true;
        }
        try {
            return NetworkInterface.getByInetAddress(addr) != null;
        } catch (Exception e) {
            return false;
        }
    }


    /**
     * NETSIM optimization - transfer config via file
     * @param
     */
    private void transferViaFile(NedWorker worker, int cmd, String data) throws ApplyException {

        setWriteTimeout(worker);

        // Write config to /tmp temporary file
        String tmpfile = "/tmp/"+device_id+"-apply-config.txt";
        traceInfo(worker, "Writing config to " + tmpfile);
        if (!writeFile(data, tmpfile)) {
            throw new ApplyException(tmpfile, "failed to write config to /tmp", true, true);
        }

        // Load config from /tmp temporary file
        traceInfo(worker, "Loading config from " + tmpfile);
        try {
            String res = print_line_exec(worker, "load merge " + tmpfile);
            if (res.contains("Error:")) {
                if (writeIgnoreAbortErrors && cmd == NedCmd.ABORT_CLI) {
                    traceInfo(worker, "ignoring ABORT load merge "+tmpfile+" error: "+stringQuote(res));
                } else {
                    throw new NedException(stringQuote(res));
                }
            }
        } catch (Exception e) {
            throw new ApplyException(e.getMessage(), " load merge "+tmpfile+" ERROR", true, true);
        }
    }


    /**
     * Restore key config-key value
     * @param
     */
    private void restoreConfigKey(NedWorker worker, int cmd, String[] lines, int fromTh) {
        if (cmd == NedCmd.ABORT_CLI || cmd == NedCmd.REVERT_CLI) {
            return;
        }
        String oldkey = maapiGetLeafString(worker, fromTh, confRoot+"key/config-key/password-encrypt");
        if (oldkey == null) {
            return;
        }
        try {
            String[] newlines = new String[3];
            for (int n = 0; n < lines.length; n++) {
                if (lines[n].startsWith("no key config-key password-encrypt")) {
                    traceInfo(worker, "SECRETS - restoring 'key config-key password-encrypt");
                    newlines[0] = lines[n];
                    newlines[1] = lines[n+1];
                    newlines[2] = "key config-key password-encrypt "+oldkey;
                    enterConfig(worker);
                    sendConfig(worker, cmd, newlines);
                    exitConfig(worker);
                    return;
                }
            }
        } catch (Exception ignore) {
            // Ignore Exception
        }
    }


    /**
     * Modify texts for real devices
     * @param
     * @return
     * @throws NedException
     */
    private String modifyTexts(NedWorker worker, String line) throws NedException {

        if (isNetsim()) {
            return line;
        }

        // banner motd|exec|incoming|login|prompt-timeout|etc.
        String match;
        String trimmed = line.trim();
        if (trimmed.startsWith("banner ")) {
            Pattern p = Pattern.compile("banner (\\S+)[ ]+(.*)");
            Matcher m = p.matcher(trimmed);
            if (m.find()) {
                String message = textDequote(m.group(2));
                message = message.replace("\r", "");  // device adds \r itself
                message = message.replace("\t", " ");  // can't include TAB
                traceVerbose(worker, "transformed => dequoted banner "+m.group(1));
                line = "banner "+m.group(1)+" ^"+message+"^";
                waitForEcho = Echo.TEXT;
            }
        }

        // aaa authentication fail-message
        else if ((match = getMatch(trimmed, "aaa authentication fail-message (.*)")) != null) {
            String message = stringDequote(match);
            message = message.replaceAll("\\r", "");  // device adds \r itself
            line = "aaa authentication fail-message " + "^" + message + "^";
            waitForEcho = Echo.TEXT;
        }

        // menu <name> title ^C <title text> \n^C
        else if (line.matches("^\\s*menu \\S+ title .*$")) {
            int i = line.indexOf("title ");
            String title = stringDequote(line.substring(i+6).trim());
            title = title.replaceAll("\\r", "");  // device adds \r itself
            line = line.substring(0,i+6) + "^" + title + "^";
            waitForEcho = Echo.TEXT;
        }

        // macro name <name> "command1\r\ncommand2\r\ncommandN\r\n"
        else if (line.matches("^\\s*macro name .*$")) {
            int i = line.indexOf("macro name ");
            i = line.indexOf(' ',i+11);
            String commands = stringDequote(line.substring(i+1).trim());
            commands = commands.replaceAll("\\r", "");  // device adds \r itself
            line = line.substring(0,i+1) + "\n" + commands + "@";
            waitForEcho = Echo.TEXT;
        }

        return line;
    }


    /**
     *
     * @param
     * @return
     * @throws NedException
     */
    private String modifyOutputLineNetsim(String data) throws NedException {

        String[] lines = data.split("\n");
        StringBuilder sbout = new StringBuilder();
        for (int n = 0; n < lines.length; n++) {
            String line = lines[n];
            String trimmed = lines[n].trim();
            String noutput = null;

            // description patch for netsim, quote text and escape "
            int i;
            if ((i = line.indexOf("description ")) >= 0) {
                String desc = line.substring(i+12).trim(); // Strip initial white spaces, added by NCS
                if (desc.charAt(0) != '"') {
                    desc = desc.replaceAll("\\\"", "\\\\\\\""); // Convert " to \"
                    noutput = line.substring(0,i+12) + "\"" + desc + "\""; // Quote string, add ""
                }
            }

            // voice translation-rule * / rule
            else if ("no".equals(rollBackOctal)
                     && getMatches(trimmed, "rule (\\d+) ((?:[\"])?[/].*?[/](?:[\"])?) "
                                   +"((?:[\"])?[/].*?[/](?:[\"])?)") != null) {
                noutput = line.replace("\\", "\\\\");
            }

            // Transform lines[n] -> XXX
            if (noutput != null && !noutput.equals(lines[n])) {
                sbout.append(noutput+"\n");
            } else if (lines[n] != null && !lines[n].isEmpty()) {
                sbout.append(lines[n]+"\n");
            }
        }

        return "\n" + sbout.toString();
    }


    /**
     * Modify line by line for output to real device
     * @param
     * @return
     * @throws Exception
     */
    private String modifyOutputLine(NedWorker worker, String data, int toTh) throws NedException {
        String[] group;
        String match;

        String[] lines = data.split("\n");
        StringBuilder sb = new StringBuilder();
        String toptag = "";
        String meta = "";
        for (int n = 0; n < lines.length; n++) {
            String output = null;
            String line = lines[n];
            String trimmed = lines[n].trim();
            if (trimmed.isEmpty()) {
                continue;
            }

            final String cmdtrim = trimmed.startsWith("no ") ? trimmed.substring(3) : trimmed;
            String nextline = (n + 1 < lines.length) ? lines[n+1] : "";

            // Update toptag
            if (isTopExit(line)) {
                toptag = "";
            } else if (Character.isLetter(line.charAt(0))) {
                toptag = trimmed;
            }

            //
            // key config-key password-encrypt - delete key before change
            //
            if (trimmed.contains("key/config-key/password-encrypt :: support-encrypted-password")) {
                traceInfo(worker, "transformed => injected 'no key config-key password-encrypt'");
                sb.append("no key config-key password-encrypt\n");
            }

            //
            // Collect all meta-data in 'meta'
            //
            if (trimmed.startsWith(META_DATA)) {
                if (!line.equals(nextline)) {
                    if (autoBgpNbrPasswordPatch && nextline.startsWith(" neighbor ")) {
                        // IOS bug patch: router bgp * / neighbor * password 7 [CISCOIOS-1418]
                        if ((match = getMatch(line, "router/bgp\\{\\d+\\}/neighbor\\{(\\S+?)\\}/password/text"))
                            != null) {
                            traceInfo(worker, "transformed => injected bgp neighbor "+match
                                      +" password reset [bgp-nbr-password-patch]");
                            sb.append(" neighbor "+match+" password 0 reset-pw-ios-patch\n");
                        }
                    }

                    // Ignore/strip duplicate meta-data annotation
                    meta += (line + "\n");
                    sb.append(line + "\n");
                }
                continue;
            }

            //
            // Ignore duplicate lines:
            //   no cos map *
            //   no qos map dscp policed
            //   no mls cos map X *
            //
            if ((trimmed.startsWith("no qos map ")
                 || trimmed.startsWith("no qos map dscp policed")
                 || trimmed.startsWith("no mls qos map "))
                && line.equals(nextline)) {
                continue;
            }

            //
            // meta-data "suppress-no-command" or hard-coded suppress (e.g. ACR)
            //
            if (meta.contains(" :: suppress-no-command") && trimmed.startsWith("no ")) {
                // suppress command that can not be removed on the device.
                output = "!suppressed: "+line;
            } else if (line.startsWith("no interface CEM-ACR") || line.startsWith("no controller SONET-ACR")) {
                output = "!suppressed: "+line;
            }

            //
            // enable algorithm-type secret
            //
            else if (line.startsWith("enable secret ") || line.startsWith("no enable secret ")) {
                output = line.replaceFirst("secret (algorithm-type \\S+)", "$1 secret");
            }

            //
            // class-map * / match vlan *
            //
            else if (toptag.startsWith("class-map ") && cmdtrim.startsWith("match vlan ")) {
                output = line.replace(",", " ");
            }

            //
            // vlan * / name
            //
            else if (toptag.startsWith("vlan ") && trimmed.startsWith("name ")
                     && trimmed.substring(5).trim().contains(" ")
                     && (match = getMatch(trimmed, "name\\s+(\\S.+)")) != null) {
                output = line.replace(match, "\""+match+"\"");
            }

            //
            // cable profile service-group * / load-balance docsis-group * profile * / downstream sg-channel *
            // cable profile service-group * / mac-domain * / downstream sg-channel * [upstream "*"]
            //
            else if (toptag.startsWith("cable profile service-group ")
                     && cmdtrim.startsWith("downstream sg-channel ")) {
                if ((match = getMatch(line, " sg-channel (\\S+)")) != null) {
                    output = line.replace(" sg-channel "+match, " sg-channel "+match.replace(",", " "));
                    output = output.replace("\"", ""); // upstream .+
                }
            }

            //
            // redirect server-group * / server ip * port *
            //
            else if (toptag.startsWith("redirect server-group ")
                     && line.startsWith(" server ip") && line.endsWith(" port 0")) {
                output = line.replace(" port 0", "");
            }

            //
            // no cable service class * name <name>
            //
            else if (trimmed.startsWith("no cable service class ")
                     && (match = getMatch(trimmed, "^no cable service class (\\d+) name \\S+$")) != null) {
                output = "no cable service class "+match;
            }

            //
            // parameter-map type regexp * / pattern *
            //
            else if (toptag.startsWith("parameter-map type regex ")
                     && cmdtrim.startsWith("pattern ")
                     && (match = getMatch(trimmed, "pattern \\\"(.*)\\\"$")) != null) {
                output = " pattern " + match;
            }

            //
            // crypto pki profile enrollment * / authentication command
            //
            else if (toptag.startsWith("crypto pki profile enrollment ")
                     && cmdtrim.startsWith("authentication command ")
                     && (match = getMatch(trimmed, "authentication command\\s+(\\\".*\\\")")) != null) {
                output = " authentication command " + passwordDequote(match);
            }

            //
            // no crypto ikev2 authorization policy default
            //
            else if ("no crypto ikev2 authorization policy default".equals(trimmed)) {
                output = "default crypto ikev2 authorization policy";
            }

            //
            // router ospf * / discard-route
            //
            else if (toptag.startsWith("router ospf ") && "discard-route external disabled".equals(trimmed)) {
                output = " no discard-route external\n";
            } else if (toptag.startsWith("router ospf ") && "discard-route internal disabled".equals(trimmed)) {
                output = " no discard-route internal\n";
            }

            //
            // cts credentials id
            //
            else if (trimmed.startsWith("cts credentials id ")) {
                output = "do "+trimmed;
            } else if (trimmed.startsWith("no cts credentials id ")) {
                output = "!"+trimmed;
            }

            //
            // chat-script * <script>
            //
            else if (toptag.contains("chat-script ")
                     && (match = getMatch(trimmed, "chat-script \\S+ (.*)")) != null) {
                String script = stringDequote(match);
                output = line.replace(match, script);
            }

            //
            // kron policy-list * / cli *
            //
            else if (toptag.startsWith("kron policy-list ")
                     && cmdtrim.startsWith("cli ")
                     && (match = getMatch(trimmed, "cli (.+)")) != null) {
                output = line.replace(match, stringDequote(match));
            }

            //
            // event manager applet * / action * regexp
            //
            else if (toptag.startsWith("event ")
                     && line.matches("^\\s*action \\d+ regexp .*$")) {
                int i = line.indexOf(" regexp \"");
                if (i > 0) {
                    String regexp = stringDequote(line.substring(i+8));
                    output = line.substring(0,i+8) + regexp;
                }
            }

            //
            // crypto pki trustpoint * / subject-name
            //
            else if (toptag.startsWith("crypto pki trustpoint ") && cmdtrim.startsWith("subject-name ")
                     && (match = getMatch(line, "subject-name\\s+(\\\".+\\\")")) != null) {
                output = line.replace(match, stringDequote(match));
            }

            //
            // alias <mode> <name> *
            //
            else if (cmdtrim.startsWith("alias ") &&
                     (group = getMatches(line, "((?:no )?alias \\S+ \\S+ )\\\"(.*)\\\"")) != null) {
                output = group[1] + passwordDequote(group[2]);
            }

            //
            // snmp-server location|contact *
            //
            else if (cmdtrim.startsWith("snmp-server ")
                     && (group = getMatches(line, "((?:no )?snmp-server (?:location|contact) )\\\"(.*)\\\"")) != null) {
                output = group[1] + passwordDequote(group[2]);
            }

            //
            // interface * / ip address
            //
            else if (toptag.startsWith("interface ") && "ip address".equals(trimmed)) {
                output = " !ip address";
            }

            // interface * / cable rf-channels channel-list x-y z bandwidth-percent
            // interface * / cable rf-channels controller ? channel-list x-y z bandwidth-percent
            //
            else if (toptag.startsWith("interface ")
                     && line.contains("cable rf-channels ") && line.contains(" channel-list ")) {
                output = line.replace(",", " ");
            }

            //
            // disable passive-interface
            //
            else if (line.contains("no disable passive-interface ")) {
                output = line.replace("no disable passive-interface ", "passive-interface ");
            }
            else if (line.contains("disable passive-interface ")) {
                output = line.replace("disable passive-interface ", "no passive-interface ");
            }

            //
            // no-list - generic trick for no-lists
            //
            else if (line.contains("no-list ")) {
                line = line.replace("no-list ", "");
                if (line.matches("^\\s*no .*$")) {
                    output = line.replace("no ", "");
                } else {
                    output = line.replace(line.trim(), "no " + line.trim());
                }
            }

            //
            // no-enable -> no enable
            //
            else if (cmdtrim.startsWith("cts server test ") && trimmed.endsWith(" no-enable")) {
                output = line.replace(" no-enable", " enable");
                if (output.startsWith("no ")) {
                    output = output.substring(3);
                } else {
                    output = "no "+output;
                }
            }

            //
            // ip access-list unordered standard|extended *
            //
            else if (cmdtrim.startsWith("ip access-list unordered ")) {
                output = line.replace(" unordered", "");

                // Duplicate name check with unordered list entry [RT36535]
                if (line.startsWith("ip access-list ")
                    && (match = getMatch(output, "ip access-list (?:standard|extended) (\\S+)")) != null) {
                    // Make sure that an standard|extended access-list entry does not exist with same name
                    String path = confRoot+"ip/access-list/standard/std-named-acl{"+match+"}";
                    if (output.startsWith("ip access-list extended ")) {
                        path = confRoot+"ip/access-list/extended/ext-named-acl{"+match+"}";
                    }
                    if (maapiExists(worker, toTh, path)) {
                        throw new NedException("'"+line+"' : Name conflict with an IP access list");
                    }
                }
            }

            // Duplicate name check with unordered list entry [RT36535]
            // ip access-list standard|extended *
            //
            else if (!isDry && line.startsWith("ip access-list ")
                     && (match = getMatch(line, "access-list (?:standard|extended) (\\S+)")) != null) {
                // Make sure that an unordered access-list entry does not exist with same name
                String path = confRoot+"ip/access-list/unordered{"+match+"}";
                if (maapiExists(worker, toTh, path)) {
                    throw new NedException("'"+line+"' : Name conflict with an unordered IP access list");
                }
            }

            //
            // ip forward-protocol udp
            //
            else if (line.contains("ip forward-protocol udp ") && line.contains(" disabled")) {
                line = line.replace(" disabled", "");
                if (line.contains("no ip")) {
                    output = line.replace("no ip", "ip");
                } else {
                    output = "no " + line;
                }
            }

            //
            // no mpls ip propagate-ttl forwarded
            //
            else if ("mpls ip propagate-ttl forwarded".equals(line)) {
                output = "mpls ip propagate-ttl";
            }

            //
            // no network-clock-participate wic *
            //
            else if (line.contains("network-clock-participate wic wic-disabled ")) {
                output = line.replace("network-clock-participate wic wic-disabled ",
                                      "no network-clock-participate wic ");
            }

            //
            // policy-map * / class * / police - bpsflat (catalyst style)
            //
            else if (toptag.startsWith("policy-map ")
                     && hasPolice("bpsflat") && cmdtrim.startsWith("police ")) {
                output = line.replaceAll("police (\\d+) bps (\\d+) byte", "police $1 $2");
            }

            //
            // no ip ssh server|client algorithm mac .*
            //
            else if (line.matches("^no ip ssh (server|client) algorithm mac .*$")) {
                output = line.substring(0,line.indexOf("mac")+3);
                output = output.replace("no", "default");
            }

            //
            // no ip ssh server|client algorithm encryption .*
            //
            else if (line.matches("^no ip ssh (server|client) algorithm encryption .*$")) {
                output = line.substring(0,line.indexOf("encryption")+10);
                output = output.replace("no", "default");
            }

            //
            // ip mroute-cache
            //
            else if (line.matches("^\\s*ip mroute-cache$") && this.useIpMrouteCacheDistributed) {
                output = line + " distributed";
            }

            //
            // monitor session * filter vlan *
            // monitor session * source vlan *
            // monitor session * source remote vlan *
            // monitor session * destination remote vlan *
            //
            else if (line.contains("monitor session") && line.contains(" vlan ")) {
                output = line.replace(","," , ").replace("-"," - ");
            }

            //
            // controller SONET * / sts-1 "x - y" mode sts-3c
            //
            else if (toptag.startsWith("controller ") && cmdtrim.startsWith("sts-1 ")) {
                output = line.replace("\"", "");
            }

            //
            // crypto pki certificate chain * / certificate *
            //
            else if (toptag.startsWith("crypto pki certificate ")
                     && line.startsWith(" certificate ")
                     && nextline.trim().startsWith("\"")) {
                // Add certificate line and dequote certificate
                traceVerbose(worker, "transformed => dequoted '"+trimmed+"'");
                sb.append(lines[n++]+"\n");
                lines[n] = stringDequote(lines[n].trim()); // note: prompt shows after each line
            }

            //
            // interface * / no ipv6 nd inspection vlan
            //
            else if (toptag.startsWith("interface ")
                     && trimmed.matches("^no ipv6 nd inspection vlan( add)? \\d+.*$")) {
                // Remove entry
                line = line.replace(" add", "");
                output = line.replace("no ipv6 nd inspection vlan",
                                      "ipv6 nd inspection vlan remove");
            }

            //
            // interface * / bridge-group
            //
            else if (toptag.startsWith("interface ")
                     && (match = getMatch(trimmed, "^no bridge-group (\\d+)$")) != null) {
                // Strip all but the first top-list delete
                sb.append(lines[n++]+"\n");
                for (; n < lines.length; n++) {
                    if (lines[n].contains(" bridge-group " + match)) {
                        traceVerbose(worker, "transformed => stripped '"+lines[n].trim()+"'");
                        continue;
                    }
                    break;
                }
            }

            //
            // voice translation-rule * / rule
            //
            else if (toptag.startsWith("voice translation-rule ") && cmdtrim.startsWith("rule ")
                     && (group = getMatches(trimmed, "rule (\\d+) ((?:[\"])?[/].*?[/](?:[\"])?)"
                                            +" ((?:[\"])?[/].*?[/](?:[\"])?)")) != null
                     && Integer.parseInt(group[0]) == 3) {
                String matchingP = passwordDequote(group[2]);
                String replacementP = passwordDequote(group[3]);
                output = " rule "+group[1]+" "+matchingP+" "+replacementP;
            }

            //
            // connect * / xconnect
            // interface * / xconnect
            // interface * / service instance * ethernet / xconnect
            // interface CEM* / cem * / xconnect
            // interface ATM* / pvc * / xconnect
            //
            else if (trimmed.startsWith("no xconnect ") && "no exit".equals(nextline.trim())) {
                traceInfo(worker, "transformed => stripped invalid 'no exit' [NSO-PATCH]");
                lines[n+1] = "";
            } else if (toptag.startsWith("interface ")
                       && getMatch(trimmed, "^xconnect (\\S+) \\d+ pw-class \\S+$") != null
                       && "exit".equals(nextline.trim())) {
                traceInfo(worker, "transformed => stripped 'exit' due to IOS anomaly, ignoring mode");
                lines[n+1] = "";
            }

            //
            // route-map * / set community *
            //
            else if (toptag.startsWith("route-map ") && trimmed.startsWith("set community ")) {
                String[] token = trimmed.split(" +");
                for (int base = 2; base < token.length; base += 10) {
                    line = " set community";
                    for (int i = base; i < base+10 && i < token.length; i++) {
                        if (!"additive".equals(token[i])) {
                            line += (" " + token[i]);
                        }
                    }
                    if (" set community".equals(line)) {
                        break; // last entry was a non-added additive only
                    }
                    if (trimmed.contains(" additive")) {
                        sb.append(line+" additive\n");
                    } else {
                        sb.append(line+"\n");
                    }
                }
                traceVerbose(worker, "transformed => formatted '"+trimmed+"'");
                continue;
            }

            //
            // route-map * / set extcommunity rt *
            //
            else if (toptag.startsWith("route-map ")
                     && trimmed.startsWith("set extcommunity rt ") && trimmed.endsWith(" additive")
                     && trimmed.length() > 100) {
                String[] rts = trimmed.split(" ");
                int base;
                for (base = 3; base < rts.length-1; base += 10) {
                    sb.append(" set extcommunity rt");
                    for (int i = base; i < base+10 && i < rts.length-1; i++) {
                        sb.append(" " + rts[i]);
                    }
                    sb.append(" additive\n");
                }
                if (base > 3) {
                    traceVerbose(worker, "transformed => split '"+trimmed+"'");
                }
                continue;
            }

            //
            // showOffline:
            // cached-show *
            //
            else if (cmdtrim.startsWith("cached-show ")) {
                continue; // Silent discard
            }

            //
            // meta-data "secret" | "support-encrypted-password"
            //
            line = output != null ? output : line;
            if ((meta.contains(":: secret") || meta.contains(":: support-encrypted-password"))
                && (match = getMatch(line, " (\\\".*\\\")")) != null) {
                // Passwords need to be dequoted using passwordDequote before sent to device
                output = line.replace(match, passwordDequote(match));
            }

            //
            // Transform lines[n] -> XXX
            //
            meta = "";
            if (output != null && !output.equals(lines[n])) {
                if (output.isEmpty()) {
                    traceVerbose(worker, "transformed => stripped '"+trimmed+"'");
                    continue;
                }
                traceVerbose(worker, "transformed => '"+trimmed+"' to '"+output.trim()+"'");
                sb.append(output+"\n");
            } else if (lines[n] != null && !lines[n].isEmpty()) {
                sb.append(lines[n]+"\n");
            }
        }
        return "\n" + sb.toString();
    }


    /**
     * Modify output before sending to device
     * @param
     * @return
     * @throws Exception
     */
    private String modifyOutput(NedWorker worker, String data, String function) throws NedException {

        logInfo(worker, "BEGIN out-transforming");
        final long start = nedReportProgress(worker, "modifying output...", 0);
        int fromTh = worker.getFromTransactionId();
        int toTh = worker.getToTransactionId();
        this.isDry = "PREPARE-DRY".equals(function);
        try {

            // Attach to CDB
            maapiAttach(worker, fromTh, toTh);

            // Reset timeout to NED standard
            lastTimeout = setReadTimeout(worker);

            // Inject unlock|relock to unlock locked config (before parseCLIDiff) to only inject current)
            data = locks.inject(worker, data, toTh, fromTh, relock);

            // Trigger custom extensions (see yang + IOSCliExtensions)
            this.outputData = "\n" + data;
            this.extInjectFirst = new StringBuilder();
            data = parseCLIDiff(worker, data);
            traceInfo(worker, "DONE NEDCOM parseCLIDiff() "+tickToString(start));
            data = this.extInjectFirst.toString() + data;

            // Attach transaction
            maapiAttach(worker, fromTh, toTh);

            // Scan meta-data and modify data (after parseCLIDiff to not make config unparsable)
            data = metaData.modifyData(worker, data, toTh, fromTh, maapi, iosmodel);

            // Trim output
            data = trimOutput(worker, data, toTh);

            // Reorder data (nedDiff 2nd in order to let user override ordering)
            data = reorderData(worker, data, fromTh);
            data = nedDiff.reorder(worker, data);

            // modify access-list
            if (resequenceACL) {
                traceInfo(worker, function + " out-transforming - ip access-lists (resequence)");
                data = nedAcl.modify(worker, data, fromTh, toTh);
            }

            // Line-by-line transformations
            if (isNetsim()) {
                data = modifyOutputLineNetsim(data);
            } else {
                data = modifyOutputLine(worker, data, toTh);
            }

            // Trim empty interface if deleted in same transaction
            data = trimInterfaces(worker, data);

            // write/replace-commit ned-setting - replace/filter config in commit
            if (!replaceCommit.isEmpty()) {
                traceInfo(worker, function + " out-transforming - inject-commit ned-setting");
                data = replaceCommitData(worker, data);
            }

            // write/inject-command ned-setting - inject command(s) [OLD API]
            if (!injectCommand.isEmpty()) {
                traceInfo(worker, function + " out-transforming - inject-command ned-setting");
                for (int n = 0; n < injectCommand.size(); n++) {
                    String[] entry = injectCommand.get(n);
                    data = injectData(worker, data, entry, "=>");
                }
            }

            // Done
            logInfo(worker, "DONE out-transforming "+tickToString(start));
            nedReportProgress(worker, "modifying output ok", start);
            return data;

        } catch (Exception e) {
            nedReportProgress(worker, "modifying output error", start);
            throw e;

        } finally {
            maapiDetach(worker, fromTh, toTh);
        }
    }


    /**
     * Trim interface exit and re-enter
     * Trim deleted empty interface changes
     * Trim interface changes after defaulted interface
     * @param
     * @return
     */
    private String trimInterfaces(NedWorker worker, String data) {

        //
        // Pass 1
        //
        StringBuilder sb = new StringBuilder();
        String[] lines = data.split("\n");
        String toptag = "";
        for (int n = 0; n < lines.length; n++) {
            String line = lines[n];
            if (line.isEmpty()) {
                sb.append("\n");
                continue;
            }

            // Trim in and out of interfaces
            final String nextline = (n + 1 < lines.length) ? lines[n+1] : "";
            if ("exit".equals(line)
                && toptag.startsWith("interface ") && nextline.equals(toptag)) {
                traceVerbose(worker, "transformed => trimmed '"+toptag+"' exit and re-enter");
                n = n + 1;
                continue;
            }

            // Update toptag
            if (isTopExit(line)) {
                toptag = "";
            } else if (Character.isLetter(line.charAt(0))) {
                toptag = line; // output, no trim needed
            }

            sb.append(line+"\n");
        }
        data = sb.toString();

        //
        // Pass 2
        //
        sb = new StringBuilder();
        lines = data.split("\n");
        for (int n = 0; n < lines.length; n++) {
            String nextline = (n + 1 < lines.length) ? lines[n+1] : "";
            String nextline2 = (n + 2 < lines.length) ? lines[n+2] : "";

            // Trim empty interface
            if (lines[n].startsWith("interface ")
                && "exit".equals(nextline)
                && data.contains("\nno "+lines[n]+"\n")) {
                traceVerbose(worker, "transformed => trimmed empty '"+lines[n]+"'");
                n++; // Also skip the "exit"
            }

            // Trim empty interface (with commment)
            else if (lines[n].startsWith("interface ")
                       && nextline.trim().startsWith("!") && "exit".equals(nextline2)
                       && data.contains("\nno "+lines[n]+"\n")) {
                traceVerbose(worker, "transformed => trimmed empty '"+lines[n]+"'");
                n = n + 2; // Also skip the comment and the "exit"
            }

            // Add line
            else {
                sb.append(lines[n]+"\n");
            }
        }

        return "\n" + sb.toString();
    }


    /**
     * Trim output data, e.g. delete of (large) ip prefix-list lists
     * @param
     * @return
     * @throws NedException
     */
    private String trimOutput(NedWorker worker, String data, int toTh) throws NedException {
        String match;
        String[] lines = data.split("\n");
        StringBuilder sb = new StringBuilder();
        String ipPfxPath = null;
        String toptag = "";
        data = "\n" + data;
        String trimRouterBgp = getMatch(data, "\nno (router bgp \\d+)");
        for (int n = 0; n < lines.length; n++) {
            String line = lines[n];
            String trimmed = line.trim();
            if (trimmed.isEmpty()) {
                continue;
            }

            if (isTopExit(line)) {
                toptag = "";
            } else if (Character.isLetter(line.charAt(0))) {
                toptag = line;
            }

            // router bgp * / no neighbor * remote-as|activate|peer-group
            // router bgp * / address-family * / no neighbor * remote-as|activate|peer-group
            if (toptag.startsWith("router bgp ")
                && trimmed.startsWith("no neighbor ")
                && (match = getMatch(line, "([ ]+no neighbor \\S+) (?:remote-as|activate|peer-group)")) != null) {
                // Compress neighbor delete if remote-as|activate|peer-group are deleted
                traceVerbose(worker, "transformed => compacted '"+line+"'");
                for (; n < lines.length - 1; n++) {
                    String nextline = (n + 2 < lines.length) ? lines[n+2] : "";
                    if (lines[n+1].startsWith(match+" ")) {
                        continue; // no neighbor <id> <leaf>
                    } else if (lines[n+1].trim().startsWith(META_DATA) && nextline.startsWith(match+" ")) {
                        n = n + 1;
                        continue;
                    }
                    break;
                }
            }

            // no router bgp *
            else if (trimRouterBgp != null && line.startsWith(trimRouterBgp)) {
                traceVerbose(worker, "transformed => trimmed deleted '"+line+"' changes");
                for (n = n + 1; n < lines.length; n++) {
                    if (isTopExit(lines[n])) {
                        break;
                    }
                }
                continue;
            }

            // no ip prefix-list <name> seq <entry>
            else if (line.startsWith("no ip prefix-list ")) {

                // Get ip prefix-list root path
                if (ipPfxPath == null) {
                    String seqNo = maapiGetLeafString(worker, toTh, confRoot+"ip/prefix-list/sequence-number");
                    traceVerbose(worker, "ip prefix-list sequence-number = "+ seqNo);
                    if (seqNo != null && "false".equals(seqNo)) {
                        ipPfxPath = confRoot+"ip/prefix-list/prefixes-no-seq";
                    } else {
                        ipPfxPath = confRoot+"ip/prefix-list/prefixes";
                    }
                }

                // Compress prefix-list delete
                String name;
                if ((name = getMatch(line, "no ip prefix-list (\\S+) ")) != null
                    && !maapiExists(worker, toTh, ipPfxPath+"{"+name+"}")) {
                    int num = 1;
                    for (int t = n + 1; t < lines.length; t++) {
                        if (!lines[t].startsWith("no ip prefix-list "+name+" ")) {
                            break;
                        }
                        lines[t] = "";
                        num++;
                    }
                    if (num > 1) {
                        traceVerbose(worker, "transformed => trimmed "+num+" 'no ip prefix-list "+name+"' lines");
                        sb.append("no ip prefix-list "+name+"\n");
                        continue;
                    }
                }
            }

            // interface * / no shutdown
            else if (" no shutdown".equals(line)
                     && toptag.startsWith("interface Vlan")
                     && data.contains("\nno "+toptag+"\n")) {
                // Trim 'no shutdown' if interface is deleted in same transaction
                traceVerbose(worker, "transformed => trimmed no shutdown in "+toptag);
                continue;
            }

            // Add line
            sb.append(line+"\n");
        }

        return "\n" + sb.toString();
    }


    /**
     * Reorder output data
     * @param
     * @return
     */
    private String reorderData(NedWorker worker, String data, int fromTh) {

        String[] lines = data.split("\n");

        //
        // Pass 1 - reorder top mode config (e.g. routes, interfaces etc)
        //
        StringBuilder first = new StringBuilder();
        StringBuilder middle = new StringBuilder();
        StringBuilder last = new StringBuilder();
        String match;
        String toptag = "";
        for (int n = 0; n < lines.length; n++) {
            String line = lines[n];
            if (line.isEmpty()) {
                continue;
            }
            String trimmed = line.trim();
            String cmdtrim = trimmed.startsWith("no ") ? trimmed.substring(3) : trimmed;

            if (isTopExit(line)) {
                toptag = "";
            } else if (Character.isLetter(line.charAt(0))) {
                toptag = line; // Note: no trim needed due to output and no trailing \r
            }

            // Routes should always be deleted first and added last [CISCOIOS-1105]
            if (line.startsWith("no ip route ") || line.startsWith("no ipv6 route ")) {
                traceVerbose(worker, "transformed => moved '"+line+"' first (route)");
                first.append(line+"\n");
            } else if (line.startsWith("ip route ") || line.startsWith("ipv6 route ")) {
                traceVerbose(worker, "transformed => moved '"+line+"' last (route)");
                last.append(line+"\n");
            }

            // Always delete main interface channel-group first (or sub-interface not accessible)
            else if (toptag.startsWith("interface ")
                     && toptag.matches("^interface [A-Za-z0-9/]+$")
                     && line.startsWith(" no channel-group ")) {
                traceVerbose(worker, "transformed => moved '"+toptag+" /"+line+"' first");
                first.append(toptag+"\n"+line+"\nexit\n");
            }

            // Always delete LISP interfaces last
            else if (line.startsWith("no interface LISP")) {
                traceVerbose(worker, "transformed => moved '"+line+"' last (lisp)");
                last.append(line+"\n");
            }

            // Delete vlan ranges last
            else if (line.startsWith("no vlan ") && line.contains("-")) {
                traceVerbose(worker, "transformed => moved '"+line+"' last (vlan range)");
                last.append(line+"\n");
            }

            // Reverse order of line vty deletes [RT24125]
            else if (line.startsWith("no line vty ")) {
                traceVerbose(worker, "transformed => moved '"+line+"' last (reversed)");
                last.insert(0, line+"\n");
            }

            // Restore terminal length
            else if (toptag.startsWith("line ") && cmdtrim.startsWith("length ")) {
                middle.append(line+"\n");
                if (last.indexOf("do terminal length 0\n") < 0) {
                    last.append("do terminal length 0\n");
                }
            }

            // Put delete of ip[v6] prefix-list before create [CISCOIOS-904]
            else if ((line.startsWith("ip prefix-list ") || line.startsWith("ipv6 prefix-list "))
                     && (match = getMatch(line, "(ip(?:v6)? prefix-list \\S+ seq \\d+ )")) != null) {
                for (int p = n + 1; p < lines.length; p++) {
                    if (lines[p].startsWith("no "+match)) {
                        traceVerbose(worker, "transformed => moved '"+lines[p]+"' up");
                        middle.append(lines[p]+"\n");
                        lines[p] = "";
                        break;
                    }
                }
                middle.append(lines[n]+"\n");
            }

            // AppNav interface and service-insertion ordering
            else if (line.startsWith("no service-insertion ") || line.startsWith("no interface AppNav")) {
                // Move to first
                traceVerbose(worker, "transformed => moved '"+line+"' first");
                first.append(line+"\n");
            }

            // interface * / no service-policy input|output - non sub-mode interface delete first
            else if (toptag.startsWith("interface ")
                     && (line.startsWith(" no service-policy output ") || line.startsWith(" no service-policy input "))
                     && (match = getMatch(toptag, "(interface [A-Za-z0-9/]+)[.][0-9]+")) != null
                     && middle.indexOf(match+"\n") >= 0) {
                traceVerbose(worker, "transformed => injected sub-interface '"+line.substring(4)+"' delete first");
                first.append(toptag+"\n"+line+"\n"+"exit\n");
            }

            // no ip sla group schedule *
            // ip sla group schedule * delete
            else if (line.startsWith("no ip sla group schedule ")
                     || getMatch(line, "ip sla group schedule (\\d+) delete") != null) {
                traceVerbose(worker, "transformed => moved '"+line+"' first");
                first.append(line+"\n");
            }

            // no platform smart-sfp interface *
            else if (line.startsWith("no platform smart-sfp interface ")) {
                traceVerbose(worker, "transformed => moved '"+line+"' last");
                last.append(line+"\n");
            }

            // Default case
            else {
                middle.append(line+"\n");

                // Special service-policy policy-map inject patch [CISCOIOS-649]
                if ((match = getMatch(lines[n], "^\\s+service-policy (?:input|output) (\\S+)$")) != null) {
                    boolean found = false;
                    for (int b = n; b >= 0; b--) {
                        if (lines[b].matches("^policy-map(?: type \\S+)? "+match+"$")) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        for (int f = n + 1; f < lines.length; f++) {
                            if (lines[f].matches("^policy-map(?: type \\S+)? "+match+"$")) {
                                traceInfo(worker, "transformed => injected '"+lines[f]
                                          +"' before use in service-policy");
                                first.append(lines[f]+"\nexit\n");
                                break;
                            }
                        }
                    }
                }
            }
        }
        data = "\n" + first.toString() + middle.toString() + last.toString();


        //
        // Pass 2 - reorder policy-map contents
        //
        data = reorderPolicyMap(worker, data, fromTh);


        //
        // Pass 3 - string buffer swapping
        //
        StringBuilder sb = new StringBuilder();
        lines = data.split("\n");
        toptag = "";
        for (int n = 0; n < lines.length; n++) {
            int swap = 0;
            String line = lines[n];
            String trimmed = lines[n].trim();
            if (trimmed.isEmpty()) {
                continue;
            }
            String nextline = (n + 1 < lines.length) ? lines[n+1] : "";
            String nexttrim2 = (n + 2 < lines.length) ? lines[n+2].trim() : "";
            String nexttrim3 = (n + 3 < lines.length) ? lines[n+3].trim() : "";
            if (isTopExit(line)) {
                toptag = "";
            } else if (Character.isLetter(line.charAt(0))) {
                toptag = trimmed;
            }

            // router ospf * / max-metric router-lsa
            if (toptag.startsWith("router ospf")
                && trimmed.startsWith("max-metric router-lsa ")
                && nextline.trim().startsWith("no max-metric router-lsa ")) {
                swap = 1;
            }

            // router * / distribute-list
            else if (toptag.startsWith("router ")
                     && trimmed.startsWith("distribute-list ")
                     && nextline.trim().startsWith("no distribute-list ")) {
                swap = 2;
            }

            // redistribute ?
            else if ((match = getMatch(nextline, "no redistribute (.*)")) != null
                     && trimmed.startsWith("redistribute "+match+" ")) {
                swap = 3;
            }

            // ip sla * / threshold + timeout
            else if (toptag.startsWith("ip sla ")
                     && trimmed.startsWith("threshold ") && nextline.trim().startsWith("timeout ")
                     && (match = getMatch(trimmed, "threshold[ ]+(\\S+)")) != null
                     && Integer.parseInt(match) > 5000) {
                swap = 4;
            }

            // ip sla * / no timeout + no threshold
            else if (toptag.startsWith("ip sla ")
                     && trimmed.startsWith("no timeout ") && nextline.trim().startsWith("no threshold ")
                     && (match = getMatch(trimmed, "no timeout[ ]+(\\S+)")) != null
                     && Integer.parseInt(match) > 5000) {
                swap = 5;
            }

            // object-group network * / no * + *
            else if (toptag.startsWith("object-group network ")
                     && line.startsWith(" no ")
                     && nextline.startsWith(" ") && !nextline.startsWith(" no ")) {
                swap = 6;
            }

            // <= NSO-4.5.3 patch for bad order in remove-before-change with secrets
            else if (trimmed.startsWith(META_DATA) && trimmed.equals(nexttrim2)
                     && !nextline.trim().startsWith("no ") && nexttrim3.startsWith("no ")) {
                traceInfo(worker, "transformed => swapped '"+nextline+"' and '"+nexttrim3+"'");
                String temp = lines[n+1];
                lines[n+1] = lines[n+3];
                lines[n+3] = temp;
            }

            // Add line and nextline, swapped
            if (swap > 0) {
                traceInfo(worker, "transformed => swapped["+swap+"] '"+line+"' and '"+nextline+"'");
                sb.append(nextline+"\n");
                sb.append(line+"\n");
                n = n + 1;
                continue;
            }

            // Default
            sb.append(line+"\n");
        }
        data = sb.toString();

        // Reordering complete
        return "\n" + data;
    }


    /**
     * Reorder policy-map for output
     * @param
     * @return
     */
    private String reorderPolicyMap(NedWorker worker, String data, int fromTh) {

        //
        // Reorder policy-map * / class * / bandwidth
        //
        StringBuilder sb = new StringBuilder();
        String[] lines = data.split("\n");
        for (int n = 0; n < lines.length; n++) {

            // Non-policy-map, add
            if (!lines[n].startsWith("policy-map ")) {
                sb.append(lines[n]+"\n");
                continue;
            }

            // Add policy-map header
            sb.append(lines[n]+"\n");
            String pmName = lines[n] + " /";
            String cName = "";

            // Creates classes buffers
            StringBuilder[] classes = new StringBuilder[3];
            for (int c = 0; c < 3; c++) {
                classes[c] = new StringBuilder();
            }

            // Loop through policy-map and reorder classes depending on bandwidth
            for (n = n + 1; n < lines.length; n++) {

                if (isTopExit(lines[n])) {
                    break;
                }

                // policy-map / description' or policy-map / !
                if (!lines[n].startsWith(" class ")) {
                    // Note: deleted class-maps end up first, which is always lowered bandwidth
                    sb.append(lines[n]+"\n");
                    continue;
                }
                cName = lines[n];

                // Add class in temporary string builder and determine prio
                int prio = 1;  // 0 = first, 1 = default, 2 = last
                StringBuilder cb = new StringBuilder();
                for (; n < lines.length; n++) {

                    // Check if bandwidth was lowered or raised
                    if (lines[n].trim().startsWith(META_DATA)) {
                        String[] metas = lines[n].trim().split(" :: ");
                        if ("pm-class-bandwidth".equals(metas[2])) {
                            // Do not reorder newly created classes
                            String cPath = metas[1].substring(0, metas[1].lastIndexOf('}')+1);
                            if ("class-default".equals(cName) || maapiExists(worker, fromTh, cPath)) {
                                String match;
                                long oldVal = maapiGetLeafLong(worker, fromTh, metas[1], 0);
                                long newVal = 0;
                                if (!lines[n+1].trim().startsWith("no ")
                                    && (match = getMatch(lines[n+1], metas[3])) != null) {
                                    newVal = Long.parseLong(match);
                                }
                                if (newVal < oldVal) {
                                    prio = 0;
                                } else if (newVal > oldVal) {
                                    prio = 2;
                                }
                                traceVerbose(worker, pmName+cName+" bandwidth: old="+oldVal
                                             +" new="+newVal+" prio="+prio);
                            }
                            continue; // Strip this meta
                        }
                    }

                    // Add line and check if end of class
                    cb.append(lines[n]+"\n");
                    if (" !".equals(lines[n])) {
                        break;
                    }
                }

                // Add class accordingly to priority
                if (prio != 1) {
                    traceVerbose(worker, "transformed => reordered "+pmName+cName
                                 +" due to bandwidth change ("+prio+")");
                }
                classes[prio].append(cb);
            }

            // Add classes in policy-map + policy-map footer
            for (int c = 0; c < 3; c++) {
                sb.append(classes[c]);
            }
            sb.append("!\n");

        }
        data = "\n" + sb.toString();


        //
        // Put policy-map bandwidth & priority percent subtractions first in order
        // to avoid error: "Sum total of class bandwidths exceeds 100 percent"
        //
        Pattern p = Pattern.compile("\npolicy-map (\\S+).*?\n!", Pattern.DOTALL);
        Matcher m = p.matcher(data);
        StringBuffer sbf = new StringBuffer();
        while (m.find()) {
            String polmap = m.group(0);
            if (hasString("\n  no (bandwidth|priority) percent", polmap)
                && hasString("\n  (bandwidth|priority) percent \\d+", polmap)) {
                lines = polmap.split("\n");
                sb = new StringBuilder("\n");
                for (int n = 0; n < lines.length; n++) {
                    if (lines[n].trim().startsWith("!")
                        || lines[n].startsWith("policy-map ")
                        || lines[n].startsWith(" class ")
                        || lines[n].startsWith("  no bandwidth percent")
                        || lines[n].startsWith("  no priority percent")) {
                        sb.append(lines[n]+"\n");
                    }
                }
                traceInfo(worker, "transformed => injected pre-delete of bandwidth|priority percent in policy-map "
                          +m.group(1)+" classes");
                polmap = sb.toString() + polmap;
            }
            m.appendReplacement(sbf, Matcher.quoteReplacement(polmap));
        }
        m.appendTail(sbf);
        data = "\n" + sbf.toString();

        return data;
    }


    /**
     *
     * @param
     * @return
     */
    private String replaceCommitData(NedWorker worker, String data) {
        for (int n = 0; n < replaceCommit.size(); n++) {
            String[] entry = replaceCommit.get(n);
            String regexp = entry[1];
            String replacement = entry[2];
            try {
                Pattern p = Pattern.compile(regexp, Pattern.DOTALL);
                Matcher m = p.matcher(data);
                StringBuffer sb = new StringBuffer();
                while (m.find()) {
                    traceInfo(worker, "transformed => replaced "+stringQuote(m.group(0))+" with "
                              + matcherToString(m, replacement));
                    m.appendReplacement(sb, replacement); // note: not quoted, want regexp replacements
                }
                m.appendTail(sb);
                data = sb.toString();
            } catch (Exception e) {
                logError(worker, "ERROR in replace-commit '"+entry[0]+"' regexp="+stringQuote(regexp)
                         +" replacement="+stringQuote(replacement), e);
            }
        }
        return data;
    }


    /**
     * Enter config mode
     * @param
     * @throws Exception
     */
    protected void enterConfig(NedWorker worker) throws NedException, IOException, SSHSessionException {

        if (inConfig) {
            traceVerbose(worker, "NOTICE: already in config mode");
            return;
        }

        session.print("config t\n");
        NedExpectResult res = session.expect(EC, worker);

        // Aborted | Error | syntax error | error
        if (res.getHit() > 3) {
            throw new NedException("failed to enter config mode");
        }

        // "Configuration mode is locked by process"
        else if (res.getHit() == 1) {
            throw new NedException(res.getText()+expectGetMatch(res));
        }

        // "Do you want to kill that session and continue"
        else if (res.getHit() == 0) {
            session.print("yes\n");
            res = session.expect(EC2, worker);
            if (res.getHit() > 2) {
                // Aborted | Error | syntax error | error
                throw new NedException("failed to enter config mode");
            }
        }
        inConfig = true;
    }


    /**
     * Exit config mode
     * @param
     * @throws Exception
     */
    protected void exitConfig(NedWorker worker) throws IOException, SSHSessionException {

        traceVerbose(worker, "exitConfig()");

        while (true) {
            session.print("exit\n");
            NedExpectResult res = session.expect(new String[] {
                    "\\A\\S*\\(config\\)#",
                    "\\A\\S*\\(cfg\\)#",
                    CFG_PROMPT,
                    "\\A\\S*\\(cfg.*\\)#",
                    PROMPT}, worker);
            if (res.getHit() == 4) {
                inConfig = false;
                return;
            }
        }
    }


    /**
     * Send config to device
     * @param
     * @throws Exceptions
     */
    private void sendConfig(NedWorker worker, int cmd, String[] lines)
        throws NedException, IOException, SSHSessionException, ApplyException {

        warningsBuf = "";

        // Set reboot timer
        lastReboot = 0;
        try {
            if (applyRebootTimer > 0) {
                setReload(worker, applyRebootTimer, true);
                lastReboot = System.currentTimeMillis();
            }
        } catch (NedException e) {
            exitConfig(worker);
            throw e;
        }

        // Send commands to device
        modeStack = new ModeStack();
        lastTimeout = setReadTimeout(worker);
        String trimmed = "";
        try {
            String meta = "";
            String toptag = "";
            for (int n = 0 ; n < lines.length ; n++) {
                trimmed = lines[n].trim();
                if (trimmed.isEmpty()) {
                    continue;
                }

                // Modify toptag
                modeStack.update(lines[n]);
                if (isTopExit(lines[n])) {
                    toptag = "";
                } else if (Character.isLetter(lines[n].charAt(0))) {
                    toptag = trimmed;
                }

                // Ignore sending meta-data to device, cache it for checks
                if (trimmed.startsWith(META_DATA)) {
                    meta += (trimmed + "\n");
                    continue;
                }

                // Bulk mode, send chunk of commands before checking replies
                if (chunkSize > 1) {
                    int e;
                    for (e = n; e < lines.length; e++) {
                        int bulk = isBulkConfig(lines[e]);
                        if (bulk == 0) {
                            break;
                        }
                        if (bulk == 1) {
                            continue;
                        }
                        for (e = e + 1; e < lines.length; e++) {
                            if (isTopExit(lines[e])) {
                                break;
                            }
                        }
                    }
                    if (e - n > 1) {
                        sendBulkConfig(worker, lines, n, e);
                        n = e - 1;
                        continue;
                    }
                }

                // Ignore all other comments
                if (trimmed.startsWith("!")) {
                    continue;
                }

                // Text mode
                waitForEcho = Echo.WAIT;
                trimmed = modifyTexts(worker, trimmed);
                if (waitForEcho == Echo.TEXT) {
                    if (trimmed.contains("\t")) {
                        waitForEcho = Echo.DONTWAIT;
                        traceInfo(worker, "Enabling dontwait");
                    } else {
                        String[] textlines = trimmed.split("\\n");
                        traceInfo(worker, "Sending '"+textlines[0]+"' -> enabling text mode");
                    }
                }

                // Reset reboot timer
                if (applyRebootTimer > 0) {
                    resetReload(worker);
                }

                // Update timeout
                lastTimeout = resetReadTimeout(worker, lastTimeout);

                // Send line to device
                print_line_wait(worker, cmd, trimmed, 0, meta, n);
                meta = "";
            } // for(;;)

        } catch (ApplyException e) {
            if (e.inConfigMode) {
                exitConfig(worker);
            }
            if (applyRebootTimer > 0) {
                cancelReload(worker, false);
            }
            logInfo(worker, "DONE "+nedCmdFullName(cmd)+" - ERROR SENDING: "
                    +stringQuote(e.getMessage())+" "+tickToString(lastTimeout));
            throw e;
        }

        if (applyRebootTimer > 0) {
            cancelReload(worker, true);
        }
    }


    /**
     *
     * @param
     * @return
     */
    private int isBulkConfig(String line) {
        String cmd = line.startsWith("no ") ? line.substring(3) : line;

        // Non-mode lists
        String[] nonModeLists = {
            "access-list ",
            "ip as-path access-list ",
            "ip community-list ",
            "ip prefix-list ",
            "ip route"
        };
        for (int n = 0; n < nonModeLists.length; n++) {
            if (cmd.startsWith(nonModeLists[0])) {
                return 1;
            }
        }

        // Mode lists
        String[] modeLists = {
            "class-map ",
            "ip access-list ",
            "ip explicit-path ",
            "ipv6 access-list ",
            "policy-map ",
            "route-map "
        };
        for (int n = 0; n < modeLists.length; n++) {
            if (line.startsWith(modeLists[0])) {
                return 2;
            }
            if (line.startsWith("no "+modeLists[0])) {
                return 1;
            }
        }

        return 0;
    }


    /**
     *
     * @param
     * @throws Exception
     */
    private void sendBulkConfig(NedWorker worker, String[] lines, int start, int end)
        throws NedException, IOException, SSHSessionException, ApplyException {
        int n;
        int length = end - start;

        traceInfo(worker, "BULK SENDING "+length+" lines [chunk "+chunkSize+"]");

        lastTimeout = setReadTimeout(worker);
        for (int i = start; i < end; i += chunkSize) {

            // Copy in up to chunkSize config commands in chunk
            int num;
            StringBuilder chunk = new StringBuilder();
            for (num = 0, n = i; n < end && n < (i + chunkSize); n++) {
                String line = lines[n];
                if (line == null || line.isEmpty()) {
                    continue;
                }
                String trimmed = line.trim();
                if (trimmed.startsWith(META_DATA)) {
                    continue;
                }
                if ("!".equals(trimmed)) {
                    continue;
                }
                chunk.append(line + "\n");
                num++;
            }

            // Send chunk of X lines to device
            traceVerbose(worker, "  BULK SENDING lines "+i+"-"+(i+num-1)+" / "+length);
            session.print(chunk.toString());

            // Check device reply of one line at the time
            for (n = i; n < end && n < (i + chunkSize); n++) {
                String line = lines[n];
                if (line == null || line.isEmpty()) {
                    continue;
                }
                String trimmed = line.trim();
                if (trimmed.startsWith(META_DATA)) {
                    continue;
                }
                if ("!".equals(trimmed)) {
                    continue;
                }

                // Reset timeout if needed
                lastTimeout = resetReadTimeout(worker, lastTimeout);

                // Check device echo and possible input error
                noprint_line_wait(worker, trimmed);
            }
        }
    }


    /**
     * Set reload timer on device
     * @param
     * @throws Exception
     */
    private void setReload(NedWorker worker, int minutes, boolean configMode) throws NedException {
        final long start = tick(0);
        traceInfo(worker, "BEGIN setting REBOOT timer to "+minutes+" minutes");

        // Config mode
        String prompt = PRIVEXEC_PROMPT;
        String cmdpfx = "";
        if (configMode) {
            cmdpfx = "do ";
            prompt = CONFIG_PROMPT;
        }

        // Run the reload command, wait for prompt and first notice
        try {
            // Send reload command and wait for echo
            String cmd = cmdpfx + "reload in "+minutes;
            session.println(cmd);
            session.expect(cmd, worker);

            // Confirm reload
            NedExpectResult res;
            String lastText = "";
            lastTimeout = setReadTimeout(worker);
            while (minutes > 0) {
                traceVerbose(worker, "Waiting for reload confirmation prompt(s)");
                res = session.expect(new String[] {
                        "\\A.*System configuration has been modified.+\\[[Yy]es/[Nn]o\\]",
                        "\\A.*unless the configuration register boot bits are non-zero.*",
                        "\\A.*Proceed with reload\\?[ ]+\\[confirm\\]" });
                lastText = res.getText();
                traceReceived(worker, res);
                if (res.getHit() == 0) {
                    traceVerbose(worker, "Sending 'no'");
                    session.println("no");
                } else if (res.getHit() == 1) {
                    throw new NedException(lastText+expectGetMatch(res));
                } else {
                    traceVerbose(worker, "Confirming with carriage-return + newline");
                    session.print("\r\n");
                    break;
                }
            }

            // Sync input (due to randomly appearing SHUTDOWN banner with or without prompt)
            traceVerbose(worker, "Syncing prompt");
            cmd = cmdpfx + "show reload";
            session.println(cmd);
            res = session.expect(new Pattern[] { Pattern.compile(cmd) }, worker);
            traceReceived(worker, res);

            traceVerbose(worker, "Waiting for prompt");
            res = session.expect(new Pattern[] { Pattern.compile(prompt) }, worker);
            traceReceived(worker, res);

            traceInfo(worker, "DONE REBOOT timer set to "+minutes+" minutes "+tickToString(start));

        } catch (Exception e) {
            throw new NedException(e.getMessage(), e);
        }
    }


    /**
     * Reset reload timer on device
     * @param
     * @throws Exception
     */
    private void resetReload(NedWorker worker) throws NedException {
        final long time = System.currentTimeMillis();
        final long diff = time - lastReboot; // in milliseconds
        if (diff > (500 * 60 * applyRebootTimer)) {
            setReload(worker, applyRebootTimer, true);
            lastReboot = System.currentTimeMillis();
        }
    }


    /**
     * Cancel reload timer on device
     * @param
     * @throws Exception
     */
    private void cancelReload(NedWorker worker, boolean configMode) {
        final long start = tick(0);
        traceInfo(worker, "BEGIN cancelling REBOOT timer");

        // Config mode
        String prompt = PRIVEXEC_PROMPT;
        String cmdpfx = "";
        if (configMode) {
            cmdpfx = "do ";
            prompt = CONFIG_PROMPT;
        }

        // Run the reload command, wait for prompt and first notice
        try {
            // Send reload cancel command and wait for echo
            String cmd = cmdpfx + "reload cancel";
            session.println(cmd);
            session.expect(cmd, worker);

            // Confirm reload
            // Note: The SHUTDOWN banner may appear before or after the prompt
            lastTimeout = setReadTimeout(worker);
            traceVerbose(worker, "Waiting for reload 'SHUTDOWN' notice");
            NedExpectResult res = session.expect(new String[] {
                    "SHUTDOWN",
                    "No reload is scheduled" }, worker);
            traceReceived(worker, res);
            if (res.getHit() == 1) {
                traceInfo(worker, "DONE REBOOT timer was not running "+tickToString(start));
                return;
            }

            // Sync input (since some IOS device show prompt after and some do not)
            traceVerbose(worker, "Syncing prompt");
            cmd = cmdpfx + "show reload";
            session.println(cmd);
            res = session.expect(new Pattern[] { Pattern.compile(cmd) }, worker);
            traceReceived(worker, res);

            traceVerbose(worker, "Waiting for prompt");
            res = session.expect(new Pattern[] { Pattern.compile(prompt) }, worker);
            traceReceived(worker, res);

            traceInfo(worker, "DONE REBOOT timer cancelled "+tickToString(start));

        } catch (Exception e) {
            logError(worker, "cancelReload ERROR: "+e.getMessage(), e);
        }
    }


    /**
     *
     * @param
     * @return
     */
    private int interfaceGetLine(String[] lines, String line, int i) {
        for (int n = i; n < lines.length; n++) {
            if (isTopExit(lines[n])) {
                break;
            }
            if (lines[n].trim().matches(line)) {
                return n;
            }
        }
        return -1;
    }


    /**
     * Send a config command line to device, wait for reply and check error
     * @param
     * @throws Exception
     */
    private void print_line_wait(NedWorker worker, int cmd, String line,
                                 int retrying, String meta, int num)
        throws NedException, IOException, SSHSessionException, ApplyException {
        String orgLine = line;
        NedExpectResult res;
        boolean decrypted = false;

        //traceVerbose(worker, "PRINT: line="+stringQuote(line)+" lastPrompt="+stringQuote(lastPrompt));

        // dirty patch to fix error that happens in timeout
        if ("config t".equals(line)) {
            traceVerbose(worker, "ignored malplaced 'config t'");
            return;
        }

        // Modify tailfned police for testing
        if (line.startsWith("tailfned police ")) {
            iospolice = line.substring(16);
            traceInfo(worker, "SET tailfned police to: "+iospolice);
        }

        // Ignore setting/deleting tailfned|xxyyzztop 'config'
        if (isDevice()) {
            if (line.startsWith("tailfned ") || line.startsWith("no tailfned ")) {
                traceInfo(worker, "ignored tailfned config: " + line);
                return;
            }
            if (line.startsWith("xxyyzztop") || line.startsWith("no xxyyzztop")) {
                traceInfo(worker, "ignored deprecated state variable: " + line);
                return;
            }
        }

        // Ignore setting/deleting cached-show 'config'
        if (line.contains("cached-show ")) {
            traceInfo(worker, "ignored non-config: " + line);
            return;
        }

        // password - may be maapi encrypted, decrypt to cleartext
        if (meta != null &&
            (meta.contains(" :: secret") || meta.contains(" :: support-encrypted-password"))) {
            String decryptedLine = decryptPassword(worker, line);
            if (!decryptedLine.equals(line)) {
                decrypted = true;
                if (trace) {
                    worker.trace("*" + orgLine + "\n\n", "out", device_id);
                    if (!logVerbose) {
                        session.setTracer(null);
                    }
                }
                line = decryptedLine;
            }
        }

        // Send line (insert CTRL-V before all '?')
        traceVerbose(worker, "Sending["+nedCmdName(cmd)+num+"]: '"+line+"'");
        session.print(stringInsertCtrlV(line) + "\n");

        // Optional delay, used e.g. to not overload link/device
        if (deviceOutputDelay > 0) {
            sleep(worker, deviceOutputDelay, false);
        }

        // Wait for echo
        if (waitForEcho == Echo.WAIT) {
            if (line.length() > 253 && isDevice()) {
                traceInfo(worker, "Waiting for echo of long line ["+line.trim().length()+" characters]");
                session.expect(new String[] { Pattern.quote(line.substring(0,253)) }, worker);
                session.expect(new String[] { "[\\u0006]*" }, worker);
            } else {
                session.expect(new String[] { Pattern.quote(line) }, worker);
            }
        }

        // Text mode, wait for echo for each line
        else if (waitForEcho == Echo.TEXT) {
            for (String wait: line.split("\n")) {
                res = session.expect(new String[] { Pattern.quote(wait), " Invalid input detected at " }, worker);
                if (res.getHit() == 1) {
                    throw new ApplyException(res.getText(), true, true);
                }
            }
        }

        // Enable tracing if disabled due to sending decrypted clear text passwords
        if (decrypted) {
            if (trace) {
                session.setTracer(worker);
                worker.trace("*" + orgLine + "\n", "out", device_id);  // simulated echo
            }
            line = orgLine;
        }

        // Wait for prompt
        res = session.expect(plw, worker);

        // Check for a blocking confirmation prompt
        if (waitForEcho == Echo.WAIT && res.getHit() >= 4) {
            traceVerbose(worker, "PROMPTED: " + res.getText());

            // Matched write/inject-answer
            if (res.getHit() >= PLW0.length) {
                // First check all entries, matching optional ml-question
                String[] entry = null;
                for (int n = 0; n < injectAnswer.size(); n++) {
                    entry = injectAnswer.get(n);
                    if (entry[3] == null) {
                        continue;
                    }
                    Pattern p = Pattern.compile(entry[3], Pattern.DOTALL);
                    Matcher m = p.matcher(res.getText());
                    if (m.find()) {
                        break;
                    }
                    entry = null;
                }
                if (entry == null) {
                    entry = injectAnswer.get(res.getHit() - PLW0.length);
                }
                traceInfo(worker, "Matched write/inject-answer "+entry[0]+": injecting answer "+stringQuote(entry[2]));
                session.print(entry[2]);
                // Note: do not wait for echo, can be passwords which are not echoed
                res = session.expect(plw, worker);
            }

            // Standard YES and NO questions
            else {
                // First try sending a 'y' only, wait 1 sec for prompt
                session.print("y");
                session.expect(new String[] { "y" }, worker);
                try {
                    res = session.expect(plw, false, 1000, worker);
                } catch (Exception e) {
                    // Timeout -> send 'es\n' for a full 'yes' + enter
                    session.print("es\n");
                    session.expect(new String[] { "es" }, worker);
                    res = session.expect(plw, worker);
                }
            }
        }

        // Get reply text (note: after confirm-questions for new text)
        final String reply = res.getText();
        final String prevPrompt = lastPrompt;
        lastPrompt = expectGetMatch(res);
        //traceVerbose(worker, "RECEIVED: text="+stringQuote(reply)+" match="+stringQuote(lastPrompt));

        // Dirty fix for mtu command leaving sub-mode (e.g. xconnect)
        if (line.startsWith("mtu ") && !lastPrompt.equals(prevPrompt)) {
            throw new ApplyException(line, "failed, left sub-mode", true, true);
        }

        // Check prompt
        switch (res.getHit()) {
        case 0:
        case 1:
        case 2:
            // config mode
            break;
        case 3:
            // exec mode
            inConfig = false;
            traceInfo(worker, "SENDING ERROR: command '"+line+"' caused exit from config mode");
            throw new ApplyException(line, "exited from config mode", true, false);
        default:
            exitPrompting(worker);
            traceInfo(worker, "SENDING ERROR: command '"+line+"' prompted twice");
            throw new ApplyException(line, "Internal ERROR: prompted twice", true, true);
        }

        // Look for retries
        final int maxRetries = isCliRetry(reply);
        if (maxRetries > 0) {
            // Wait a while and retry
            if (retrying >= maxRetries) {
                // Already tried enough, give up
                throw new ApplyException(line, "["+retrying+" retries]: "+reply, true, true);
            }
            else {
                // Sleep (default 1000ms), reset timeout(s) and try same command again
                sleep(worker, configOutputRetryInterval, true);
                setReadTimeout(worker);
                if (applyRebootTimer > 0) {
                    resetReload(worker);
                }
                traceVerbose(worker, "Retry #" + (retrying+1));
                print_line_wait(worker, cmd, line, retrying+1, meta, num);
                return;
            }
        }

        // Look for errors
        if (waitForEcho == Echo.WAIT && isCliError(worker, cmd, reply, line, meta)) {
            throw new ApplyException(line+"\r\n", reply.trim()+modeStack.toString(), true, true);
        }

        // Retry succeeded, reset timeout
        if (retrying > 0) {
            traceInfo(worker, "Retry success after " + retrying + " retries");
            setReadTimeout(worker);
        }

        // Sleep three seconds for clear command to take effect (RT20042)
        if ("do clear crypto ikev2 sa fast".equals(line)
            || line.startsWith("do clear ip nat ")) {
            resetTimeout(worker, this.readTimeout + 3000, 0);
            sleep(worker, 3000, true); // Sleep 3 seconds
        }
    }


    /**
     *
     * @param
     * @throws Exception
     */
    private void noprint_line_wait(NedWorker worker, String trimmed)
        throws NedException, IOException, SSHSessionException, ApplyException {

        // Wait for echo
        session.expect(new String[] { Pattern.quote(trimmed) }, worker);

        // Second, wait for the prompt
        NedExpectResult res = session.expect(plw, worker);

        // Third, check if we exited config mode
        switch (res.getHit()) {
        case 0: // (cfg) - top mode
        case 1: // (config) - top mode
        case 2: // (.*) - sub-mode
            break;
        case 3: // exec mode
            inConfig = false;
            traceInfo(worker, "BULK SENDING ERROR: command '"+trimmed+"' caused exit from config mode");
            throw new ApplyException(trimmed, "exited from config mode", true, false);
        default:
            throw new ApplyException(trimmed, "Internal ERROR: device prompted", true, true);
        }

        // Verify no retry
        String reply = res.getText();
        if (isCliRetry(reply) > 0) {
            throw new ApplyException(trimmed, "Internal ERROR: retry-command", true, true);
        }

        // Check for device error
        if (isCliError(worker, NedCmd.PREPARE_CLI, reply, trimmed, null)) {
            throw new ApplyException(trimmed+"\r\n", reply.trim()+modeStack.toString(), true, true);
        }
    }


    /**
     * Check if command must be retried
     * @param
     * @return max number of retries if retry command, else 0
     */
    private int isCliRetry(String reply) {

        if (reply.trim().isEmpty()) {
            return 0;
        }

        // Ignore retry on these patterns:
        final String[] ignoreRetry = {
            "%(\\S+): (informational|error): \\S+ is in use on",
            "please remove .* from .* first",
            "is in use[.] remove from .* before deleting", // no flow monitor *
            "is in use[.] cannot be deleted", // no policy-map *
            "first remove .* from the above", // no crypto ipsec transform-set
            "\\S+ is in use and cannot be modify or delete",
            "failed to field add[:] object is in use",
            "failed to set .+ .+ is in use. remove from all interfaces"
        };
        for (int n = 0; n < ignoreRetry.length; n++) {
            if (findString(ignoreRetry[n], reply.toLowerCase()) >= 0) {
                return 0;
            }
        }

        // Short retry on these:
        final String[] isShort = {
            "flow \\S+ is in use. remove from all \\S+ before (editing|modification)"
        };
        for (int n = 0; n < isShort.length; n++) {
            if (findString(isShort[n], reply.toLowerCase()) >= 0) {
                return 3;
            }
        }

        // Retry on these patterns:
        final String[] isRetry = {
            "is in use",
            "is still in use and cannot be removed",
            "wait for it to complete",
            "wait for the current operation to complete",
            "wait for current config download to complete",
            "Config update in progress; please wait and retry",
            "is currently being deconfigured",
            "is currently deactivating",
            "is being deleted, please try later",
            "is being deleted.* Try it later",
            "being configured in another session.* try again later",
            "are down, try again later",
            "Certificate server is busy, initial .* unable to be processed, try again later",
            "In-use PW template cannot be removed", // no template type pseudowire
            " already in use by VRF", // vrf definition * / rd
            "You may retry shortly" // iox
        };
        for (int n = 0; n < isRetry.length; n++) {
            if (findString(isRetry[n], reply) >= 0) {
                return configOutputMaxRetries;
            }
        }

        // Do not retry
        return 0;
    }


    /**
     * Check if device reply is an error or if the output should be ignored
     * @param
     * @return
     */
    private boolean isCliError2(NedWorker worker, int cmd, String replyall, String reply, String line, String meta) {

        reply = reply.trim();
        if (reply.isEmpty() || reply.equals(line)) {
            return false;
        }

        traceVerbose(worker, "Checking device reply "+stringQuote(reply));

        if (meta != null
            && line.startsWith("no ")
            && reply.contains("Invalid input detected at")
            && meta.contains("suppress-delete-error-invalid")) {
            traceVerbose(worker, "suppressed delete invalid error on: " + line);
            return false;
        }

        // Special cases ugly patches
        if ("no shutdown".equals(line)
            && reply.contains("shutdown can't be applied on standby interface")) {
            // Happens if interface used as "backup interface"
            return false;
        }
        if (line.contains("no ip address ") && reply.contains("Invalid address")) {
            // Happens when IP addresses already deleted on interface
            return false;
        }
        if (line.contains("no ip address ") && reply.contains("Invalid address")) {
            // Happens when IP addresses already deleted on interface
            return false;
        }
        if (("no duplex".equals(line) || "no speed".equals(line) || "speed auto".equals(line))
            && !reply.contains("Auto-negotiation is enabled. Speed cannot be set")) {
            // Ignore these errors because harmless and happen:
            // E.g. when 'no media-type' is deleted before duplex or speed
            // E.g. when 'no speed' is sent after negotiation auto
            // E.g. when no speed & speed auto are both sent
            traceInfo(worker, "Ignoring error/warning");
            return false;
        }
        if ("no mpls control-word".equals(line)) {
            traceInfo(worker, "Ignoring '"+line+"' (cli-show-no)");
            return false;
        }
        if (line.contains("switchport") && reply.contains("Maximum number of interfaces reached")) {
            return true;
        }
        if ("no switchport".equals(line)) {
            // Can't do no switchport on some devices:
            traceInfo(worker, "Ignoring non-required command");
            return false;
        }
        if ("switchport".equals(line) && reply.contains("Incomplete command")) {
            // Some devices (e.g. 891) do not use switchport on single line.
            // Some devices do not support switchport on Port-channel if.
            traceInfo(worker, "Ignoring non-required command");
            return false;
        }
        if ((line.startsWith("no interface LISP") || line.startsWith("no interface CEM"))
            && reply.contains("Invalid input detected at")) {
            // Delete of router lisp deletes LISP interfaces (which in turn can't be deleted first)
            // Delete of controller E1 * / framing deletes the CEM interface (which in turn can't be deleted first)
            traceInfo(worker, "Ignoring delete of missing interface");
            return false;
        }
        if (line.contains("reporting smart-licensing-data")
            && reply.contains("Invalid input detected at")) {
            traceInfo(worker, "Ignoring non-required command");
            return false;
        }
        if (line.startsWith("ip redirects")
            && replyall.contains("ip redirect is not applicable for p2p link")) {
            traceInfo(worker, "Ignoring non-required command");
            return false;
        }
        if (line.startsWith("no interface ")
            && replyall.contains("Sub-interfaces are not allowed on switchports")) {
            traceInfo(worker, "Ignoring useless warning");
            return false;
        }
        if (line.startsWith("no cem ") &&
            ((reply.contains("Circuit ") && reply.contains(" not present"))
             || reply.contains("Please use no cem-group command under controller to remove"))) {
            traceInfo(worker, "Ignoring non-required command");
            return false;
        }

        if (reply.contains("Invalid input detected at")) {
            // Ignore Invalid input error on non-existing injected config
            for (int n = interfaceConfig.size()-1; n >= 0; n--) {
                String[] entry = interfaceConfig.get(n);
                if (findString(line, entry[1]) >= 0) {
                    trace(worker, "Ignoring non-supported injected interface config", "out");
                    return false;
                }
            }
            for (int n = injectConfig.size()-1; n >= 0; n--) {
                String[] entry = injectConfig.get(n);
                if (findString(line, entry[2]) >= 0) {
                    traceInfo(worker, "Ignoring non-supported injected config '"+entry[2]+"'");
                    return false;
                }
            }
        }

        // Static error triggers (overriding ignore)
        for (int n = 0; n < staticError.length; n++) {
            if (findString(staticError[n], reply) >= 0) {
                traceInfo(worker, "ERROR SENDING - matched static error '"+reply+"'");
                return true;
            }
        }

        // Ignore static warnings
        for (int n = 0; n < staticWarning.length; n++) {
            if (findString(staticWarning[n], reply.toLowerCase()) >= 0) {
                traceInfo(worker, "ignoring static warning: "+stringQuote(staticWarning[n]));
                warningsBuf += "> "+line+"\n"+reply+"\n";
                return false;
            }
        }

        // Ignore dynamic warnings
        for (int n = 0; n < dynamicWarning.size(); n++) {
            if (findString(dynamicWarning.get(n), reply) >= 0) {
                traceInfo(worker, "ignoring dynamic warning: '"+reply+"'");
                warningsBuf += "> "+line+"\n"+reply+"\n" ;
                return false;
            }
        }

        // Ignore all errors when rollbacking due to abort (i.e. a previous error)
        if (writeIgnoreAbortErrors && cmd == NedCmd.ABORT_CLI) {
            traceInfo(worker, "ignoring ABORT error: "+stringQuote(reply));
            return false;
        }

        // Fail on all else
        traceInfo(worker, "ERROR SENDING - reply '"+reply+"'");
        return true;
    }


    /**
     *
     * @param
     * @return
     */
    private boolean isCliError(NedWorker worker, int cmd, String reply, String line, String meta) {

        // Debug code:
        if (cmd == NedCmd.ABORT_CLI && failphase.contains("abort")) {
            traceInfo(worker, "DEBUG: Simulating failed abort");
            failphase = failphase.replace("abort", "");
            return true;
        }

        // Strip shutdown info message(s)
        reply = reply.replaceAll("\\*\\*\\*\r\n\\*\\*\\* --- SHUTDOWN in \\S+ ---\r\n\\*\\*\\*\r\n", "");
        String replyall = reply;

        // Trim and check if empty reply
        reply = reply.replaceAll("\\r", "").trim();
        if (reply.isEmpty() || reply.length() <= 1) {
            return false;
        }

        // Strip echo of the failing command 'line'
        if (reply.contains("Invalid input")) {
            reply = reply.replace(line, "");
        }

        // Check all warnings, may be multiple
        reply = "\n" + reply;
        String[] warnings = reply.split("\n% ");
        for (int i = 0; i < warnings.length; i++) {
            String warning = warnings[i].trim();
            if (warning.isEmpty() || warning.length() <= 1) {
                continue;
            }
            if (isCliError2(worker, cmd, replyall, warning, line, meta)) {
                return true;
            }
        }
        return false;
    }


    /*
     **************************************************************************
     * persist
     **************************************************************************
     */

    /**
     * Persist (save) config on device
     * @param
     * @throws Exception
     */
    @Override
    public void persist(NedWorker worker) throws Exception {
        final long start = tick(0);
        if (trace) {
            session.setTracer(worker);
        }
        logInfo(worker, "BEGIN PERSIST");

        // Save config
        if (!ignoreNextWrite && "on-persist".equals(this.writeMemoryMode)) {
            saveConfig(worker, NedCmd.PERSIST);
        }

        logInfo(worker, "DONE PERSIST "+tickToString(start));
        worker.persistResponse();
    }


    /**
     * Save configuration on device
     * @param
     * @throws NedException, IOException, SSHSessionException, ApplyException
     */
    private void saveConfig(NedWorker worker, int cmd)
        throws NedException, IOException, SSHSessionException, ApplyException {

        // Save running-config to startup-config
        print_line_wait_oper(worker, cmd, this.writeMemory, 0, writeTimeout);
    }


    /*
     **************************************************************************
     * commit
     **************************************************************************
     */

    /**
     * Commit config
     * @param
     * @throws Exception
     */
    @Override
    public void commit(NedWorker worker, int timeout) throws Exception {
        final long start = tick(0);
        if (trace) {
            session.setTracer(worker);
        }
        logInfo(worker, "BEGIN COMMIT");

        // Reconnect to device if remote end closed connection due to being idle
        if (session.serverSideClosed()) {
            reconnectDevice(worker);
        }

        // Save config
        if (!ignoreNextWrite && "on-commit".equals(this.writeMemoryMode)) {
            saveConfig(worker, NedCmd.COMMIT);
        }
        ignoreNextWrite = false;

        // Archive config
        configArchive.archive(worker);

        logInfo(worker, "DONE COMMIT "+tickToString(start));
        worker.commitResponse();
    }


    /*
     **************************************************************************
     * abort
     **************************************************************************
     */

    /**
     * apply failed, rollback config
     * @param
     * @throws Exception
     */
    @Override
    public void abort(NedWorker worker, String data) throws Exception {
        final long start = tick(0);
        if (trace) {
            session.setTracer(worker);
        }
        logInfo(worker, "BEGIN ABORT");

        if (relock.length() > 0) {
            String relockBuf = relock.toString();
            relock.setLength(0);
            traceInfo(worker, "locks: pre-injecting relock(s) "+stringQuote(relockBuf));
            data = relockBuf + data;
        }

        // Apply the abort
        doApplyConfig(worker, NedCmd.ABORT_CLI , data);

        logInfo(worker, "DONE ABORT "+tickToString(start));
        worker.abortResponse();
    }


    /*
     **************************************************************************
     * revert
     **************************************************************************
     */

    /**
     * Revert config
     * @param
     * @throws Exception
     */
    @Override
    public void revert(NedWorker worker, String data) throws Exception {
        final long start = tick(0);
        if (trace) {
            session.setTracer(worker);
        }
        logInfo(worker, "BEGIN REVERT");

        // Apply the revert
        doApplyConfig(worker, NedCmd.REVERT_CLI, data);

        // Save config
        if ("on-commit".equals(this.writeMemoryMode)) {
            saveConfig(worker, NedCmd.REVERT_CLI);
        }

        // Archive config
        configArchive.archive(worker);

        logInfo(worker, "DONE REVERT "+tickToString(start));
        worker.revertResponse();
    }


    /*
     **************************************************************************
     * command
     **************************************************************************
     */

    /**
     * Run command(s) on device.
     * From ncs_cli: devices device <dev> live-status exec any "command"
     * @param
     * @throws Exception
     */
    @Override
    public void command(NedWorker worker, String cmdName, ConfXMLParam[] p) throws Exception {
        if (trace) {
            session.setTracer(worker);
        }

        // Prepare command
        String cmd = nedCommand.prepare(worker, cmdName, p);

        // internal - show warnings
        String reply;
        if ("show warnings".equals(cmd)) {
            reply = "\nWarnings/output since last commit: \n"+ warningsBuf;
        }

        // internal - show ned-settings
        else if ("show ned-settings".equals(cmd)) {
            reply = "\n"+nedSettings.dumpAll();
        }

        // internal - show outformat raw
        else if ("show outformat raw".equals(cmd)) {
            reply = "\nNext dry-run will show raw (unmodified) format.\n";
            showRaw = true;
        }

        // internal - set iosmodel
        else if (cmd.startsWith("set iosmodel ")) {
            iosmodel = cmd.substring(13);
            reply = "\niosmodel set to '"+iosmodel+"'";
        }

        // internal - secrets resync
        else if ("secrets resync".equals(cmd)) {
            secrets.enableReSync();
            String config = getConfig(worker);
            modifyInput(worker, false, -1, config);
            reply = "\nRe-synced all cached secrets.\n";
        }

        // internal - sync-from-file <path/file>
        else if (cmd.startsWith("sync-from-file ")) {
            syncFile = cmd.trim().substring(15).trim();
            reply = "\nNext sync-from will use file = " + syncFile + "\n";
        }

        // internal - fail <phase>
        else if (cmd.startsWith("fail ")) {
            failphase = cmd.substring(5).trim();
            reply = "\nfailphase set to: '"+failphase+"'\n";
        }

        // internal - accept-eula
        else if (cmd.startsWith("accept-eula")) {
            reply = "\nUsage: live-status exec any accept-eula <seconds to wait>\n";
            int wait = Integer.parseInt(cmd.substring(12).trim());
            if (wait > 0) {
                traceInfo(worker, "Waiting for EULA agreement banner and prompt for "+wait+" seconds");
                try {
                    Pattern[] cmdPrompt = new Pattern[] {
                        Pattern.compile(".*ACCEPT[ ]+\\[y/n\\]")
                    };
                    NedExpectResult res = session.expect(cmdPrompt, true, wait * 1000, worker);
                    if (res.getHit() == 0) {
                        traceInfo(worker, "Sending 'y', accepting EULA agreement");
                        session.println("y");
                        // Wait for prompt?
                        reply = "\nAccepted EULA agreement\n";
                    }
                } catch (Exception e) {
                    reply = "\nTimeout with no EULA agreement prompt\n";
                }
            }
        }

        // Device command
        else {
            nedCommand.execute(worker, cmd);
            return;
        }

        // Internal command reply
        logInfo(worker, "COMMAND - internal: "+stringQuote(cmd));
        traceInfo(worker, reply);
        worker.commandResponse(new ConfXMLParam[] { new ConfXMLParamValue("ios-stats", "result", new ConfBuf(reply))});
    }


    /**
     * Exit prompting
     * @param
     * @throws Exception
     */
    protected void exitPrompting(NedWorker worker) throws IOException, SSHSessionException {

        Pattern[] cmdPrompt = new Pattern[] {
            // Prompt patterns:
            Pattern.compile(PRIVEXEC_PROMPT),
            Pattern.compile(CFG_PROMPT),
            Pattern.compile(ANY_PROMPT),
            // Question patterns:
            Pattern.compile(":\\s*$"),
            Pattern.compile("\\]\\s*$")
        };

        while (true) {
            traceVerbose(worker, "Sending CTRL-C");
            session.print("\u0003");
            traceVerbose(worker, "Waiting for non-question");
            NedExpectResult res = session.expect(cmdPrompt, true, readTimeout, worker);
            if (res.getHit() <= 2) {
                traceVerbose(worker, "Got prompt ("+res.getHit()+")");
                return;
            }
        }
    }


    /*
     **************************************************************************
     * keepAlive
     **************************************************************************
     */

    /**
     * This method is invoked periodically to keep an connection
     * alive. If false is returned the connection will be closed using the
     * close() method invocation.
     *
     * @param worker
     */
    public boolean keepAlive(NedWorker worker) {
        final long start = tick(0);
        if (trace) {
            session.setTracer(worker);
        }
        logInfo(worker, "BEGIN KEEP-ALIVE");
        boolean alive = true;
        try {
            if (session.serverSideClosed()) {
                reconnectDevice(worker);
            } else {
                traceVerbose(worker, "Sending newline");
                session.println("");
                traceVerbose(worker, "Waiting for prompt");
                session.expect(new String[] { CONFIG_PROMPT, PRIVEXEC_PROMPT}, worker);
            }
        } catch (Exception e) {
            alive = false;
            logError(worker, "KEEP_ALIVE ERROR: "+e.getMessage(), e);
        }
        logInfo(worker, "DONE KEEP-ALIVE = "+alive+" "+tickToString(start));
        return alive;
    }


    /**
     * Reconnect to device using connector
     * @throws Exception
     */
    private void reconnectDevice(NedWorker worker) throws NedException {
        traceInfo(worker, "Server side closed, reconnecting");
        try {
            connectorReconnectDevice(worker);
        } catch (Exception e) {
            throw new NedException("Failed to reconnect :: "+e.getMessage(), e);
        }
    }



    /*
     **************************************************************************
     * NedSecrets
     **************************************************************************
     */

    /**
     * Used by NedSecrets to check whether a secret is cleartext or encrypted.
     * Method must be implemented by all NED's which use NedSecrets.
     * @param secret - The secret
     * @return True if secret is cleartext, else false
     */
    @Override
    public boolean isClearText(String secret) {
        String trimmed = secret.trim();

        // encrypted
        if (secret.matches("[0-9a-f]{2}(:([0-9a-f]){2})+")) {
            return false;  // aa:11 .. :22:bb
        }
        if (trimmed.contains(" encrypted")) {
            return false;  // XXX encrypted
        }
        if (trimmed.startsWith("password ")) {
            return false;  // password XXX
        }
        if (trimmed.endsWith(" 7")) {
            return false;  // XXX 7
        }
        if (getMatch(trimmed, "^([1-9] \\S+)") != null) {
            return false;  // [1-9] XXX
        }

        // Default to cleartext
        return true;
    }


    /*
     **************************************************************************
     * Common utility methods
     **************************************************************************
     */

    /**
     * Report progress with Verbosity NORMAL
     */
    private long nedReportProgress(NedWorker worker, String msg, long lastTime) {
        return reportProgress(worker, Verbosity.NORMAL, msg, lastTime);
    }

    /**
     * Trace debug message (NOTE: same as traceVerbose)
     * @param
     *
     */
    public void traceDebug(NedWorker worker, String info) {
        if (devTraceLevel >= TRACE_DEBUG) {
            traceInfo(worker, info);
        }
    }


    /**
     * Trace debug2 message
     * @param
     *
     */
    public void traceDebug2(NedWorker worker, String info) {
        if (devTraceLevel >= TRACE_DEBUG2) {
            traceInfo(worker, info);
        }
    }


    /**
     * Trace debug3 message
     * @param
     *
     */
    public void traceDebug3(NedWorker worker, String info) {
        if (devTraceLevel >= TRACE_DEBUG3) {
            traceInfo(worker, info);
        }
    }


    /**
     * Trace expect buffers
     * @param
     */
    private void traceReceived(NedWorker worker, NedExpectResult res) {
        traceVerbose(worker, "RECEIVED: text="+stringQuote(res.getText())
                     +" match="+stringQuote(expectGetMatch(res)));
    }


    /**
     * Attach to Maapi
     * @param
     * @throws NedException
     */
    private void maapiAttach(NedWorker worker, int fromTh, int toTh) throws NedException {
        try {
            int usid = worker.getUsid();
            traceDebug2(worker, "Maapi.Attach: from="+fromTh+" to="+toTh+" usid="+usid);
            if (fromTh != -1) {
                maapi.attach(fromTh, 0, usid);
            }
            if (toTh != -1) {
                maapi.attach(toTh, 0, usid);
            }
        } catch (Exception e) {
            throw new NedException("Internal ERROR: maapiAttach(): "+e.getMessage(), e);
        }
    }


    /**
     * Detach from Maapi
     * @param
     */
    private void maapiDetach(NedWorker worker, int fromTh, int toTh) {
        try {
            traceDebug2(worker, "Maapi.Detach: from="+fromTh+" to="+toTh);
            if (fromTh != -1) {
                maapi.detach(fromTh);
            }
            if (toTh != -1) {
                maapi.detach(toTh);
            }
        } catch (Exception e) {
            logError(worker, "Internal ERROR: maapiDetach(): "+e.getMessage(), e);
        }
    }


    /**
     * Return true if device reply is an error
     * @param
     * @return
     */
    private boolean isExecError(String res) {
        return res.contains("Invalid input ");
    }


    /**
     *
     * @param
     * @throws Exception
     */
    private void print_line_wait_oper(NedWorker worker, int cmd, String line, int retrying)
        throws NedException, IOException, SSHSessionException, ApplyException {
        print_line_wait_oper0(worker, cmd, line, retrying, this.readTimeout);
    }

    private void print_line_wait_oper(NedWorker worker, int cmd, String line, int retrying, int timeout)
        throws NedException, IOException, SSHSessionException, ApplyException {
        print_line_wait_oper0(worker, cmd, line, retrying, timeout);
        setReadTimeout(worker);
    }

    private void print_line_wait_oper0(NedWorker worker, int cmd, String line, int retrying, int timeout)
        throws NedException, IOException, SSHSessionException, ApplyException {

        traceVerbose(worker, "Sending(oper): '"+line+"'");

        // Send line and wait for echo
        session.print(line+"\n");
        session.expect(new String[] { Pattern.quote(line) }, worker);

        // Reset timeout after echo in case expect() reset timeout or echo slow
        resetTimeout(worker, timeout, 0);

        // Wait for prompt
        boolean loop = true;
        NedExpectResult res = null;
        while (loop) {
            traceVerbose(worker, "Waiting for oper prompt");
            res = session.expect(new String[] {
                    "Overwrite the previous NVRAM configuration\\?\\[confirm\\]",
                    "Warning: Saving this config to nvram may corrupt any network",
                    "Destination filename \\[\\S+\\][\\?]?\\s*$",
                    PRIVEXEC_PROMPT}, worker);
            String failtxt = res.getText();
            switch (res.getHit()) {
            case 0:
                // Overwrite the previous NVRAM configuration
                traceVerbose(worker, "Sending 'y'");
                session.print("y");
                break;
            case 1:
                // Warning: Saving this config to nvram may corrupt any network
                // management or security files stored at the end of nvram.
                // Continue? [no]: no
                // % Configuration buffer full, can't add command: access-list 99
                // %Aborting Save. Compress the config,
                // Save it to flash or Free up space on device[OK]
                // Confirm question with "n", wait for prompt again then fail
                traceVerbose(worker, "Sending 'n'");
                session.print("n");
                session.expect(new String[] {".*#"}, worker);
                throw new ApplyException(line, failtxt, true, false);
            case 2:
                // Destination filename
                traceInfo(worker, "Sending newline (destination filename)");
                session.print("\r\n");
                break;
            default:
                loop = false;
                break;
            }
        }

        //
        // Check device reply
        //

        // Retries
        String reply = res.getText().trim();
        if (reply.contains("Device or resource busy")) {
            if (retrying >= configOutputMaxRetries) {
                throw new ApplyException(line, reply, true, false); // Give up retrying
            }
            // Sleep and retry
            sleep(worker, configOutputRetryInterval, true);
            print_line_wait_oper(worker, cmd, line, retrying + 1);
            return;
        }

        // Errors
        if (reply.toLowerCase().contains("error")
            || reply.toLowerCase().contains("failed")) {

            // Ignore dynamic warnings
            for (int n = 0; n < dynamicWarning.size(); n++) {
                if (findString(dynamicWarning.get(n), reply) >= 0) {
                    traceInfo(worker, "ignoring dynamic oper warning on: "+stringQuote(reply));
                    return;
                }
            }

            // Throw exception
            throw new ApplyException(line, reply, true, false);
        }
    }


    /**
     *
     * @param
     * @return
     * @throws Exception
     */
    protected String print_line_exec(NedWorker worker, String line) throws Exception {

        // Send command and wait for echo
        session.print(line + "\n");
        session.expect(new String[] { Pattern.quote(line) }, worker);

        // Return command output
        return session.expect(PRIVEXEC_PROMPT, worker);
    }


    /**
     * Same as print_line_exec except also checks simulated ned-settings
     * @param
     * @return
     * @throws Exception
     */
    protected String print_line_simulated(NedWorker worker, String line) throws Exception {
        // ned-setting cisco-ios developer simulate-show *
        String simulated = simulateShow(worker, line);
        if (simulated != null) {
            return simulated;
        }
        return print_line_exec(worker, line);
    }


    /**
     *
     * @param
     * @return
     * @throws Exception
     */
    protected String print_line_exec(NedWorker worker, String line, int timeout) throws Exception {

        // Send command and wait for echo
        session.print(line + "\n");
        session.expect(new String[] { Pattern.quote(line) }, worker);

        // Reset timeout after echo in case expect() reset timeout or echo slow
        this.lastTimeout = resetTimeout(worker, timeout, 0);

        // Return command output
        return session.expect(PRIVEXEC_PROMPT, worker);
    }


    /**
     * Check if path exists
     * @param
     * @return
     */
    protected boolean maapiExists(NedWorker worker, int th, String path) {
        // Trim to absolute path
        int up;
        while ((up = path.indexOf("/../")) > 0) {
            int slash = path.lastIndexOf('/', up-1);
            path = path.substring(0, slash) + path.substring(up + 3);
        }
        try {
            if (maapi.exists(th, path)) {
                traceVerbose(worker, "maapiExists("+path+") = true");
                return true;
            }
        } catch (Exception e) {
            logError(worker, "maapiExists("+path+") ERROR: ", e);
        }

        traceVerbose(worker, "maapiExists("+path+") = false");
        return false;
    }


    /**
     * Get value from CDB using Maapi
     * @param
     * @return
     */
    protected String maapiGetLeafString(NedWorker worker, int th, String path) {
        // Trim to absolute path
        int up;
        while ((up = path.indexOf("/../")) > 0) {
            int slash = path.lastIndexOf('/', up-1);
            path = path.substring(0, slash) + path.substring(up + 3);
        }
        // Get leaf
        try {
            if (maapi.exists(th, path)) {
                return ConfValue.getStringByValue(path, maapi.getElem(th, path));
            }
        } catch (Exception e) {
            traceInfo(worker, "maapiGetLeafString("+path+") Exception: "+e.getMessage());
        }
        return null;
    }


    /**
     *
     * @param
     * @return
     */
    private long maapiGetLeafLong(NedWorker worker, int th, String path, long defaultValue) {
        long val = defaultValue;
        String string = maapiGetLeafString(worker, th, path);
        if (string != null) {
            val = Long.parseLong(string);
        }
        return val;
    }


    /**
     * Bulk read an entire list using maapi.getObjects()
     * @param
     * @return ArrayList with String[] containing all entries and all leaves for the entire list
     *         Note: Unset leaves are indicated by the null String, e.g. [i] = null
     *         Note2: For 'type empty' the name of the leaf is returned (excluding prefix)
     *         Warning: The list may not contain embedded lists.
     * @throws NedException
     */
    protected ArrayList<String[]> maapiGetObjects(NedWorker worker, int th, String path, int numLeaves)
        throws NedException {
        final long start = tick(0);
        try {
            ArrayList<String[]> list = new ArrayList<>();

            // Verify list exists
            if (!maapi.exists(th, path)) {
                traceVerbose(worker, "'" + path + "' not found");
                return list;
            }

            // Read number of instances
            int num = maapi.getNumberOfInstances(th, path);
            if (num <= 0) {
                traceInfo(worker, "'" + path + "' is empty (" + num + ")");
                return list;
            }
            traceVerbose(worker, "'" + path + "' getNumberOfInstances() = " + num);

            // Bulk-read all rules
            MaapiCursor cr = maapi.newCursor(th, path);
            List<ConfObject[]> objList = maapi.getObjects(cr, numLeaves, num);

            // Add all the entries in an ArrayList
            for (int n = 0; n < objList.size(); n++) {
                ConfObject[] objs = objList.get(n);
                String[] entry = new String[numLeaves];
                // Add all the leaves in a String[] array
                for (int l = 0; l < numLeaves; l++) {
                    entry[l] = objs[l].toString();
                    if ("J_NOEXISTS".equals(entry[l])) {
                        entry[l] = null;
                    } else if (entry[l].startsWith(PREFIX)) {
                        entry[l] = entry[l].replaceFirst(PREFIX, "");
                    }
                    traceVerbose(worker, "LIST["+n+","+l+"] = "+entry[l]);
                }
                list.add(entry);
            }

            traceInfo(worker, "'" + path + "' read " + numLeaves + " leaves in "
                      +objList.size()+" entries " + String.format("[%d ms]", tick(start)));
            return list;

        } catch (Exception e) {
            throw new NedException("Internal ERROR in maapiGetObjects(): " + e.getMessage(), e);
        }
    }


    /**
     * Read an entire list entry using maapi.getObject()
     * @param
     * @return String[] containing all the leaves for this list entry
     *         Note: Unset leaves are indicated by the null String, e.g. [i] = null
     *         Note2: For 'type empty' the name of the leaf is returned (excluding prefix)
     *         Warning: The list entry may not contain embedded lists.
     * @throws NedException
     */
    protected String[] maapiGetObject(NedWorker worker, int th, String path, int numLeaves)
        throws NedException {
        final long start = tick(0);
        try {
            // Verify list exists
            if (!maapi.exists(th, path)) {
                traceVerbose(worker, "'" + path + "' not found");
                return new String[0];
            }

            // Read the list entry and add all the leaves in a String[] array
            ConfObject[] obj = maapi.getObject(th, path);
            String[] entry = new String[numLeaves];
            for (int l = 0; l < numLeaves; l++) {
                entry[l] = obj[l].toString();
                if ("J_NOEXISTS".equals(entry[l])) {
                    entry[l] = null;
                } else if (entry[l].startsWith(PREFIX)) {
                    entry[l] = entry[l].replaceFirst(PREFIX, "");
                }
                traceVerbose(worker, "ENTRY["+l+"] = "+entry[l]);
            }

            traceInfo(worker, "'" + path + "' read " + numLeaves + " leaves "
                      + String.format("[%d ms]", tick(start)));
            return entry;

        } catch (Exception e) {
            throw new NedException("Internal ERROR in maapiGetObjects(): " + e.getMessage(), e);
        }
    }


    /**
     * Get config from CDB
     * @param
     * @return
     */
    protected String maapiGetConfig(NedWorker worker, int th, String path, int trimLevel) {

        // Trim to absolute path
        int up;
        while ((up = path.indexOf("/../")) > 0) {
            int slash = path.lastIndexOf('/', up-1);
            path = path.substring(0, slash) + path.substring(up + 3);
        }

        // Load config from NSO CDB
        StringBuilder sb = new StringBuilder();
        try {
            if (!maapi.exists(th, path)) {
                return null;
            }

            MaapiInputStream in = maapi.saveConfig(th,
                                                   EnumSet.of(MaapiConfigFlag.MAAPI_CONFIG_C_IOS,
                                                              MaapiConfigFlag.CISCO_IOS_FORMAT),
                                                   path);
            if (in == null) {
                traceInfo(worker, "maapiGetConfig ERROR: failed to get "+ path);
                return null;
            }

            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = in.read(buffer, 0, buffer.length)) > 0) {
                sb.append(new String(buffer).substring(0, bytesRead));
                if (bytesRead < buffer.length) {
                    break;
                }
            }
        } catch (Exception e) {
            traceInfo(worker, "maapiGetConfig ERROR: read exception "+ e.getMessage());
            return null;
        }

        String[] lines = sb.toString().split("\n");
        if (lines.length < 5) {
            return null; // output does not contain 'devices device <device-id>\n config\n' + ' !\n!\n'
        }

        sb = new StringBuilder();
        for (int n = 2 + trimLevel; n < lines.length - 2 - trimLevel; n++) {
            String line = lines[n].substring(2);
            if (line.trim().startsWith("ios:") || line.trim().startsWith("no ios:")) {
                line = line.replaceFirst("ios:", "");
            }
            sb.append(line+"\n");
        }

        String data = sb.toString();
        traceVerbose(worker, "MAAPI_GET_AFTER=\n"+data);
        return data;
    }


    /**
     * tailf:cli-range-list-syntax leaf-list to int[] ArrayList
     * @param
     * @return
     */
    protected ArrayList<String> rangeListToArray(String leafListVal) {
        ArrayList<String> list = new ArrayList<>();
        if (leafListVal == null || leafListVal.isEmpty()) {
            return list;
        }
        final String[] ranges = leafListVal.split(",");
        for (int r = 0; r < ranges.length; r++) {
            Pattern p = Pattern.compile("(\\d+)(?:[-](\\d+))?");
            Matcher m = p.matcher(ranges[r]);
            if (!m.find()) {
                continue;
            }
            int start = Integer.parseInt(m.group(1));
            int end = m.group(2) != null ? Integer.parseInt(m.group(2)) : start;
            for (int v = start; v <= end; v++) {
                list.add(Integer.toString(v));
            }
        }
        return list;
    }


    /**
     *
     * @param
     * @return
     */
    private boolean hasPolice(String police) {
        return iospolice.contains(police);
    }


    /**
     *
     * @return
     */
    private boolean isDevice() {
        return !isNetsim();
    }


    /**
     *
     * @return
     */
    @Override
    public boolean isNetsim() {
        return iosmodel.contains("NETSIM");
    }


    /**
     * Check if line is top exit
     * @param
     * @return
     */
    private boolean isTopExit(String line) {
        line = line.replace("\r", "");
        if ("exit".equals(line)) {
            return true;
        }
        return "!".equals(line);
    }


    /**
     * Write file to disk
     * @param
     * @return
     */
    private boolean writeFile(String text, String file) {
        try (
             java.io.BufferedWriter writer = new java.io.BufferedWriter(new java.io.FileWriter(file))
             ) {
            writer.write(text);
        } catch (java.io.IOException e) {
            return false;
        }
        return true;
    }


    /**
     * Inject data
     * @param
     * @return
     * @throws NedException
     */
    private String injectData(NedWorker worker, String data, String[] entry, String dir)
        throws NedException {
        String insert;

        if (entry[3] == null) {
            throw new NedException("ned-settings: Missing 'where' leaf config");
        }

        Pattern p = Pattern.compile(entry[1]+"(?:[\r])?[\n]", Pattern.DOTALL);
        Matcher m = p.matcher(data);

        // Special (slow) case for after-last
        if ("after-last".equals(entry[3])) {
            int end = -1;
            String[] groups = null;
            while (m.find()) {
                end = m.end(0);
                groups = fillGroups(m);
            }
            if (end != -1) {
                try {
                    insert = fillInjectLine(worker, entry[2] + "\n", entry[3], groups, dir);
                } catch (Exception e) {
                    throw new NedException("malformed inject regexp '"+entry[1]+"' : "+e.getMessage());
                }
                data = data.substring(0, end) + insert + "\n" + data.substring(end);
            }
        }

        else {
            StringBuffer sb = new StringBuffer();
            while (m.find()) {
                String replacement = m.group(0);
                try {
                    insert = fillInjectLine(worker, entry[2] + "\n", entry[3], fillGroups(m), dir);
                } catch (Exception e) {
                    throw new NedException("malformed inject regexp '"+entry[1]+"' : "+e.getMessage());
                }
                if ("before-first".equals(entry[3])) {
                    m.appendReplacement(sb, Matcher.quoteReplacement(insert + replacement));
                    break;
                } else if ("before-each".equals(entry[3])) {
                    m.appendReplacement(sb, Matcher.quoteReplacement(insert + replacement));
                } else if ("after-each".equals(entry[3])) {
                    m.appendReplacement(sb, Matcher.quoteReplacement(replacement + insert));
                }
            }
            m.appendTail(sb);
            data = sb.toString();
        }

        return data;
    }


    /**
     *
     * @param
     * @return
     */
    private String fillInjectLine(NedWorker worker, String insert, String where, String[] groups, String dir) {
        int offset = 0;

        // Replace $i with group value from match.
        // Note: hard coded to only support up to $9
        for (int i = insert.indexOf('$'); i >= 0; i = insert.indexOf('$', i+offset)) {
            int num = (int)(insert.charAt(i+1) - '0');
            insert = insert.substring(0,i) + groups[num] + insert.substring(i+2);
            offset = offset + groups[num].length() - 2;
        }

        traceInfo(worker, "transformed "+dir+" injected "+stringQuote(insert)+" "+where+" "+stringQuote(groups[0]));

        return insert;
    }


    /**
     * Sleep for milliseconds
     * @param
     */
    private void sleep(NedWorker worker, long milliseconds, boolean log) {
        if (log) {
            traceVerbose(worker, "Sleeping " + milliseconds + " milliseconds");
        }
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            traceInfo(worker, "sleep interrupted");
            Thread.currentThread().interrupt();
        }
        if (log) {
            traceVerbose(worker, "Woke up from sleep");
        }
    }


    /**
     *
     * @param
     * @return
     */
    private String decryptPassword(NedWorker worker, String line) {
        Pattern p = Pattern.compile("( \\$[48]\\$[^\\s]*)"); // " $4$<key>" || " $8<key>"
        Matcher m = p.matcher(line);
        while (m.find()) {
            String password = line.substring(m.start() + 1, m.end());
            try {
                traceVerbose(worker, "decryptPassword: "+stringQuote(password));
                String decrypted = mCrypto.decrypt(password);
                traceVerbose(worker, "transformed => decrypted MAAPI password: "+password);
                line = line.substring(0, m.start()+1)
                    + decrypted
                    + line.substring(m.end(), line.length());
            } catch (Exception e) {
                // Ignore exceptions, since can't tell if $8 is NSO or IOS encrypted
                return line;
            }
            m = p.matcher(line);
        }
        return line;
    }


    /**
     *
     * @param
     * @return
     */
    private static String nedCmdName(int cmd) {
        if (cmd == NedCmd.ABORT_CLI) {
            return "abort ";
        }
        if (cmd == NedCmd.REVERT_CLI) {
            return "revert ";
        }
        return "";
    }


    /**
     *
     * @param
     * @return
     */
    private static String nedCmdFullName(int cmd) {
        if (cmd == NedCmd.ABORT_CLI) {
            return "ABORT";
        }
        if (cmd == NedCmd.REVERT_CLI) {
            return "REVERT";
        }
        return "APPLY-CONFIG";
    }


    /**
     * Set user session
     * @throws Exception
     */
    private void setUserSession(NedWorker worker) throws ConfException, IOException {
        try {
            maapi.getMyUserSession();
        } catch (Exception ignore) {
            maapi.setUserSession(worker.getUsid());
        }
    }


    /**
     *
     * @param
     * @return
     */
    private String stringInsertCtrlV(String line) {
        if (line.indexOf('?') < 0) {
            return line;
        }
        return line.replace("?", (char)(0x16)+"?");
    }


    /**
     *
     * @param
     * @return
     */
    private static String findLine(String buf, String search) {
        int i = buf.indexOf(search);
        if (i >= 0) {
            int nl = buf.indexOf('\n', i+1);
            if (nl >= 0) {
                return buf.substring(i,nl);
            } else {
                return buf.substring(i);
            }
        }
        return null;
    }


    /**
     *
     * @param
     * @return
     */
    private static String getString(String buf, int offset) {
        int nl = buf.indexOf('\n', offset);
        if (nl < 0) {
            return buf;
        }
        return buf.substring(offset, nl).trim();
    }


    /**
     *
     * @param
     * @return
     */
    private String stripLineAll(NedWorker worker, String res, String search) {
        StringBuilder buffer = new StringBuilder();
        String[] lines = res.split("\n");
        for (int i = 0; i < lines.length; i++) {
            if (lines[i].trim().startsWith(search)) {
                traceVerbose(worker, "transformed <= stripped '"+lines[i]+"'");
                continue;
            }
            buffer.append(lines[i]+"\n");
        }
        return buffer.toString();
    }


    /**
     * Read file from disk
     * @param
     * @return
     * @throws Exception
     */
    private String readFile(String file) throws IOException {
        BufferedReader reader = new BufferedReader(new java.io.FileReader(file));
        String line = null;
        StringBuilder sb = new StringBuilder();
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
                sb.append("\r\n");
            }
            return sb.toString();
        } finally {
            reader.close();
        }
    }


    /**
     * Like NedString.stringDequote except that it preserves single backslash
     * @param
     * @return
     */
    private static String textDequote(String aText) {
        if (aText.indexOf('"') != 0) {
            return aText;
        }
        aText = aText.substring(1,aText.length()-1);
        StringBuilder result = new StringBuilder();
        StringCharacterIterator iterator =
            new StringCharacterIterator(aText);
        char c1 = iterator.current();
        while (c1 != CharacterIterator.DONE) {
            if (c1 == '\\') {
                char c2 = iterator.next();
                if (c2 == CharacterIterator.DONE) {
                    result.append(c1);
                } else if (c2 == 'b') {
                    result.append('\b');
                } else if (c2 == 'n') {
                    result.append('\n');
                } else if (c2 == 'r') {
                    result.append('\r');
                } else if (c2 == 'v') {
                    result.append((char) 11); // \v
                } else if (c2 == 'f') {
                    result.append('\f');
                } else if (c2 == 't') {
                    result.append('\t');
                } else if (c2 == 'e') {
                    result.append((char) 27); // \e
                } else if (c2 == '\\') {
                    result.append('\\');
                } else {
                    result.append(c1);
                    result.append(c2);
                }
            } else {
                result.append(c1);
            }
            c1 = iterator.next();
        }
        return result.toString();
    }


    /**
     * Trim to single blank between words
     * @param
     * @return
     */
    protected String trimBlanks(String line) {
        StringBuilder sb = new StringBuilder();
        StringCharacterIterator it = new StringCharacterIterator(line);
        char ch = it.current();
        boolean trim = false;
        while (ch != CharacterIterator.DONE) {
            char nextCh = it.next();
            if (trim && ch == ' ' && nextCh == ' ') {
                continue;
            }
            if (ch == '\n') {
                trim = false;
            } else if (ch != ' ') {
                trim = true;
            }
            sb.append(ch);
            ch = nextCh;
        }
        return sb.toString();
    }

}
